import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import Page from 'components/Page';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import UserProgressTable from 'components/UserProgressTable';
import classNames from 'classnames';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardHeader,
  CardLink,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Table,
  Progress
} from 'reactstrap';
import { MdPersonPin} from 'react-icons/md';
import Avatar from 'components/Avatar';
import withBadge from 'hocs/withBadge';

const AvatarWithBadge = withBadge({
  position: 'bottom-right',
  color: 'success',
})(Avatar);

const usersData = [
  {
    name: 'Ilustración',
    population: '180000'
  },
  {
    name: 'Traducción',
    population: '18000'
  },
  {
    name: 'Composición musical',
    population: '1800'
  },
  {
    name: 'SEO',
    population: '180'
  },
  {
    name: 'Animación',
    population: '18'
  },
  {
    name: 'QA',
    population: '1'
  },
];

const headers=[
  'n°',
  'nombre',
  //'seguidores',
]


class CategoryRanking extends React.Component {
  render(){
  const {popular_categories} = this.props
  console.log(popular_categories)
    return (
      <Page className="custom-container">
          <Row className=" m-0">
              <Col md="5" sm="12" xs="12">
                  <Card>
                  <CardHeader>Ranking de categorías populares</CardHeader>
                  <CardBody>
                    <Table responsive hover>
                      <thead>
                        <tr className="text-capitalize align-middle text-center">
                          {headers.map((item, index) => <th key={index}>{item}</th>)}
                        </tr>
                      </thead>
                      <tbody>
                        {popular_categories.map((popular_category, index) => (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td className="align-middle text-center">{popular_category.name}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </CardBody>
                  </Card>
              </Col>
          </Row>      
        
      </Page>
    );
  }
};

export default CategoryRanking;
