import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import Page from 'components/Page';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import UserProgressTable from 'components/UserProgressTable';
import classNames from 'classnames';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardHeader,
  CardLink,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Table,
} from 'reactstrap';
import { MdPersonPin} from 'react-icons/md';
import { Link } from 'react-router-dom'
import user1Image from 'assets/img/users/100_1.jpg';
import user2Image from 'assets/img/users/100_2.jpg';
import user3Image from 'assets/img/users/100_3.jpg';
import user4Image from 'assets/img/users/100_4.jpg';
import user5Image from 'assets/img/users/100_5.jpg';
import user6Image from 'assets/img/users/100_6.jpg';


  const dataTable = [
    {
      month: 'Ene',
      earned: '180000',
    },
    {
      month: 'Feb',
      earned: '100000',
    },
    {
      month: 'Mar',
      earned: '200000',
    },
    {
      month: 'Abr',
      earned: '100',
    },
    {
      month: 'May',
      earned: '50000',
    },
    {
      month: 'Jun',
      earned: '145000',
    },
    {
      month: 'Jul',
      earned: '123456',
    },
    {
      month: 'Ago',
      earned: '555555',
    },
    {
      month: 'Sep',
      earned: '654321',
    },
    {
      month: 'Oct',
      earned: '753159',
    },
    {
      month: 'Nov',
      earned: '-',
    },
    {
      month: 'Dic',
      earned: '-',
    },
  ];

const headers=[
  'Mes',
  'Total ventas($)',
  'Opciones',
];
class EarningsUser extends React.Component {
  monthToString = (month) =>{
		let month_string= ""
		switch(month){
		  case 1: month_string="Ene";
		  break;
		  case 2: month_string="FEB";
		  break;
		  case 3: month_string="MAR";
		  break;
		  case 4: month_string="ABR";
		  break;
		  case 5: month_string="MAY";
      break;
      case 6: month_string="JUN";
      break;
      case 7: month_string="JUL";
      break;
      case 8: month_string="AGO";
      break;
      case 9: month_string="SEP";
      break;
      case 10: month_string="OCT";
      break;
      case 11: month_string="NOV";
      break;
      case 12: month_string="DIC";
		  break;
		}
		return month_string
	}
  render(){
    const{user,monthly_gains,onShowMonthGain} = this.props
    console.log(monthly_gains)
    return (
      <Page className="custom-container">
          <Row className=" m-0">
              <Col md="5" sm="12" xs="12">
                  <Card>
                  <CardHeader>Ganancias en el año</CardHeader>
                  <CardBody>
                    <Table responsive hover id='earning-table'>
                      <thead>
                        <tr className="text-capitalize align-middle text-center">
                          {headers.map((item, index) => <th key={index}>{item}</th>)}
                        </tr>
                      </thead>
                      <tbody>
                        {(monthly_gains) &&
                          monthly_gains.map((month_gain, index) => (
                          <tr key={index}>
                            <td className="align-middle text-center">{this.monthToString(month_gain.month)}</td>
                            <td className="align-middle text-center">{month_gain.monthly_total}</td>
                            <td className="align-middle text-center">
                              <Link to="/detail-earning-user">
                                <Button onClick={()=>{onShowMonthGain(month_gain)}} color="info">Ver</Button>
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </CardBody>
                  </Card>
              </Col>
          </Row>      
        
      </Page>
    )
  }
}

export default EarningsUser;
