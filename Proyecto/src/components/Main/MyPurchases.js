import React from 'react';
import bg11Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import img_publication1 from 'assets/img/custom-img/Emerald-Dream.png';
import img_publication2 from 'assets/img/custom-img/Queen.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'
import Page from 'components/Page';
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardHeader,
  Badge,
  CardText,
  CardTitle,
  Col,
  Row,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  Progress,
  Label
} from 'reactstrap';

class MyPurchases extends React.Component{

	statusToString = (status) =>{
		//estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada
		let status_string= ""
		switch(status){
		  case 1: status_string="solicitada";
		  break;
		  case 2: status_string="aceptada";
		  break;
		  case 3: status_string="rechazada";
		  break;
		  case 4: status_string="cancelada";
		  break;
		  case 5: status_string="finalizada";
		  break;
		  default:
		  	status_string="evaluada"
		}
		return status_string
	}

	render(){
		const {my_purchases, onShowOrder} = this.props
		console.log(my_purchases)
		return(
			<Page style={{textAlign: "center"}} className="custom-container">
					<Row className="row-custom">
				        <Col md={8} sm={6} xs={12} className="mb-3">
								{(my_purchases && my_purchases.length>0) ?
									my_purchases.map((my_purchase,index)=>(
										<div key={index}>
											<Card>
												<CardImg top src={"http://localhost:3000/uploads/posts/"+my_purchase.action_post.post.pictures[0]} />
												<CardBody>
													<CardHeader style={{padding: 0}}>
														<CardTitle>{my_purchase.action_post.post.title}</CardTitle>
														<div className="chip-status">
															<Badge id="popProgress" color="primary" title="Estado" style={{cursor: "pointer"}} className="mr-1">
																{this.statusToString(my_purchase.status)}
															</Badge>
														</div>
													</CardHeader>
													<br/>
													<CardText>
														<small>fecha de solicitud: <br/> {my_purchase.date}</small>
													</CardText>
													<Label for="exampleText"><b>Redes sociales:</b></Label>
													<CardText>
														email: <small>{my_purchase.action_post.post.user.email}</small>
													</CardText>
													{my_purchase.action_post.post.user.user_profile.facebook &&
														<CardText>
															facebook: <small>{my_purchase.action_post.post.user.user_profile.facebook}</small>
														</CardText>
													}
													{my_purchase.action_post.post.user.user_profile.instagram &&
														<CardText>
															instagram: <small>{my_purchase.action_post.post.user.user_profile.instagram}</small>
														</CardText>
													}
													{my_purchase.action_post.post.user.user_profile.twitter &&
														<CardText>
															twitter: <small>{my_purchase.action_post.post.user.user_profile.twitter}</small>
														</CardText>
													}
												</CardBody>
												<div style={{padding: "4%",marginTop: "-10px"}}>
													<Link to="detail-order">
														<Button className="btn btn-success button-succes-custom button-view-custom" onClick={()=>onShowOrder(my_purchase)}><FontAwesomeIcon icon={faEye}/></Button>
													</Link>
														<br/>
													
														<label style={{marginTop: "7px", fontSize: "small", marginBottom: 0}}> Ver detalles </label>
													
												</div>
											</Card>
											<br/>
										</div>
									))
									:
									<div style={{padding: 10, height: 42, background: "antiquewhite"}}>
										<label style={{float: "center",fontSize:14}}>No ha realizado alguna compra </label>
									</div>
								}
				        </Col>
				    </Row>
			 </Page>
		);
	}
}

export default MyPurchases;