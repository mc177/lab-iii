import React from 'react'
import Page from 'components/Page';
import {
	Card, 
	CardHeader, 
	CardBody, 
    CardTitle,
    CardText, 
	Row, 
	Col, 
	Label, 
	Form, 
	FormGroup, 
	FormText, 
	Input, 
	Button,
	InputGroup,
	InputGroupAddon, 
	Modal, 
	ModalBody, 
	ModalFooter,
	ModalHeader
} from 'reactstrap' 
import { Link } from 'react-router-dom'
import {dateFormatWithMonthName} from '../Utils/dateParse' 

class FinishOrden extends React.Component {
 
	constructor(props){ 
		super(props)
		this.state = {
			post: {
				category_id: "",
				price: {
					coin: 0, 
					value: ""
				},
				description: "",
				images: [],
				user: {
					name: "Jeferson",
					forename: "Alvarado",
					email: "jejoalca14@gmail.com",
					role: "Seller and Buyer",
					phone: "04245558208",
					adress: "Av. principal San Lorenzo",
					birthdate: "1996-07-30",
					sex: "M",
					description: "Me gusta ésta página",
					dropdownOpen: false					
				}
			},
			modal: false,
		}
	}

	clickModal = (e) =>{
		return this.setState({
			modal: !this.state.modal,
		}); 
	  }

	deleteImage(index_param){
		let post = Object.assign({}, this.state.post,{
			images: this.state.post.images.filter((image, index) => index != index_param)
		}) 
		this.setState({post})
	}
 
	render(){

		const {post} = this.state
		const{order,onFinishOrder, onHandleInputFile, onDeletOrderPicture} = this.props
		return(	
            <Page className="custom-container">
                <Row>
                    <Col xl={12} lg={12} md={12}>
                    <Card style={{marginBottom:15}}>
                        <CardHeader>Datos generales de la orden:</CardHeader>
                        <CardBody>
                            <CardText>
							<b>Producto ordenado:</b> {order.action_post.post.title}
                            </CardText>
                            <CardText>
                                 <b>Nombre del cliente:</b> {order.buyer.buyer_profile.name} {order.buyer.buyer_profile.lastname}
                            </CardText>
							<CardText>
								<b>fecha de pedido:</b> {dateFormatWithMonthName(order.date)}
							</CardText>
                        </CardBody>
                    </Card>
                    <Card> 
                        <CardHeader style={{textTransform: "none"}}>Producto(s)</CardHeader>
                        <CardBody>
                        <Form>
                            <FormGroup row>
                            <Label for="pictures" sm={2}>
                                Seleccione las imágenes (5 máx)
                            </Label>
                            <Col sm={10}>
                                <Input type="file" name="pictures" style={{color: "transparent"}} onChange={onHandleInputFile} />
                                {
                                    (order && order.pictures && order.pictures.length > 0)?
                                    <label>
                                        {"Se han seleccionado " + order.pictures.length + " imagenes"}
                                    </label>
                                    :
                                    ""
                                }
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                                <div style={{width: "100%", marginLeft: 19, background: "#dfdfdf", padding: 7, marginRight: 15, borderRadius: 4}}>
                                {
                                    (order && order.prev_pictures && order.prev_pictures.length > 0)?
                                        order.prev_pictures.map((image, index) => (
                                            <span key={index}>	
                                                <label className="x-close" onClick={() => onDeletOrderPicture(index)}> x </label>
                                                <img style={{maxWidth: "23%", borderRadius: 6, marginRight: 4}} src={image}/>
                                            </span>
                                        ))
                                    :
                                    <Label for="no_images">
                                        No se han seleccionado imagenes
                                    </Label>
                                }
                                </div>
                            </FormGroup>
                            <FormGroup check row>
                            <Row className="m-rl-0" style={{height:35, marginTop:15}}>
                                <Col className="mb-3" xs={6}>
									<Button onClick={()=> {this.clickModal()}} color="success">Finalizar</Button>
                                </Col>
                                <Col className="mb-3" xs={6}>
									<Link to="detail-order">
                                    	<Button color="danger">Cancelar</Button>
									</Link>
                                </Col>
                            </Row>
                            </FormGroup>
                        </Form>
                        </CardBody>
                    </Card>
                    </Col>
                </Row>
				<Modal
					isOpen={this.state.modal}>
					{!order.pictures.length>0 ? 
					<div>
						<ModalHeader>Seleccione una imagen</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="email">Debe seleccionar una imagen para finalizar el pedido</Label>{' '}
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="primary" onClick={()=> {this.clickModal()}}>
								Aceptar
							</Button>
							
						</ModalFooter>
						</div>
					:
					<div>
						<ModalHeader>Está seguro que desea finalizar la compra?</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="email">Esta acción no podrá ser revertida</Label>{' '}
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Link to="detail-order">
								<Button color="primary" onClick={()=> {this.clickModal();onFinishOrder(order,order.pictures)}}>
									Aceptar
								</Button>
							</Link>
							<Button color="danger" onClick={()=> {this.clickModal()}}>
								Cancelar
							</Button>
							
						</ModalFooter>
					</div>
					}
				</Modal>
            </Page>
		);
	}
}

export default FinishOrden; 