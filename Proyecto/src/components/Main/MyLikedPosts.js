import React from 'react';
import img_publication1 from 'assets/img/custom-img/Emerald-Dream.png';
import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user2Image from 'assets/img/users/100_2.jpg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import Page from 'components/Page';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardHeader,
  Badge,
  CardText,
  CardTitle,
  Col,
  Row,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  Progress,
  Modal,ModalBody,ModalFooter,ModalHeader,
  FormGroup,Input,Label,
} from 'reactstrap';
import { Link } from 'react-router-dom'

class MyLikedSells extends React.Component {

	constructor(props){
        super(props);
        this.state = {
            modal: false,
        };
	}
	
	clickModal = (e) =>{
        return this.setState({
            modal: !this.state.modal,
        }); 
	}
	
	actionExecuted = (actions,string) =>{
		let action_executed = null
		actions.map((action)=>{
			if(action.name == string)
				action_executed = action
		})
		return action_executed
	}
	render(){
		const {user,my_liked_posts,actions, onRemoveLikePost,onShowPost} = this.props
		return(
			<Page style={{textAlign: "center"}} className="custom-container">
					<Row className="row-custom">
				        <Col md={8} sm={6} xs={12} className="mb-3">
							{(my_liked_posts && my_liked_posts.length>0) ?
								my_liked_posts.map((my_liked_post, index) => (
									<div key={index}>
										{(my_liked_post && my_liked_post.post) &&
											<Card>
												<div className="user-header">
													<li className="nav-item" style={{listStyle: "none"}}>
														<a id="Popover2" className="nav-link">
															<img src={(my_liked_post.post.user && my_liked_post.post.user.user_profile && my_liked_post.post.user.user_profile.image)? process.env.REACT_APP_VERONICA_URL_WEB + 'uploads/users/' + my_liked_post.post.user.user_profile.image : process.env.REACT_APP_VERONICA_URL_WEB + 'assets/user_default.png' + my_liked_post.post.user.user_profile.image} className="rounded-circle can-click flipthis-highlight user-image-cardsells"/>
															<label style={{marginLeft: "2%", color: "white", marginTop:"2%", fontSize:14}}>
															{(my_liked_post.post.user.user_profile.name!="" && my_liked_post.post.user.user_profile.lastname!="") ? <>{my_liked_post.post.user.user_profile.name} {my_liked_post.post.user.user_profile.lastname}</> : my_liked_post.post.user.email}
															</label>
														</a>
													</li>
												</div>
												<CardImg top style={{minHeight:250}} src={"http://localhost:3000/uploads/posts/"+my_liked_post.post.pictures[0]} />
												<CardBody style={{marginBottom: "-30px"}}>
												<CardHeader style={{padding: 0, textTransform:"none", textAlign:"left"}}>
													<div className="chip-status">
														<Badge id="categoryIlustration" title="Categoría" color="info"  style={{cursor: "pointer"}} className="mr-1">
														{my_liked_post.post.category.name}
														</Badge>
														<UncontrolledPopover trigger="legacy" placement="bottom" target="categoryIlustration">
															<PopoverHeader style={{background: "rgb(0, 201, 255)", color: "white"}}>Categoría {my_liked_post.post.category.name}</PopoverHeader>
															<PopoverBody>
																{my_liked_post.post.category.description}
															</PopoverBody>
														</UncontrolledPopover>
													</div>
													<CardText>
														<b>{my_liked_post.post.title}</b>
													</CardText>
													<CardText>
														<small>{my_liked_post.post.description}</small>
													</CardText>
												</CardHeader>
												<br/>
												</CardBody>
												<div style={{padding: "2%", marginBottom: "5px"}}>
													<FontAwesomeIcon onClick={()=>onRemoveLikePost(my_liked_post.post,this.actionExecuted(actions,"Me gusta"))} title="Me gusta"  className="heart-icon" icon={faHeart}/>
													<FontAwesomeIcon title="Compartir" className="share-icon" icon={faShareAlt} onClick={()=> this.clickModal()}/>
													<Link to="/detail-publication">
														<Button title="Ver detalles" className="btn btn-success button-succes-custom see-details-liked-sells" onClick={()=>{onShowPost(my_liked_post.post)}}><FontAwesomeIcon icon={faEye}/></Button>
													</Link>
												</div>
											</Card>
										}
										<br/>
									</div>
								))
							:
								<div style={{padding: 10, height: 42, background: "antiquewhite"}}>
									<label style={{float: "center",fontSize:14}}>No se le ha dado Me gusta a un post </label>
								</div>
							}
				        </Col>
				    </Row>
					<Modal
						isOpen={this.state.modal}>
						<ModalHeader>Compartir publicación</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="email">Correo</Label>{' '}
								<Input
									type="email"
									name="email"
									placeholder="ejemplo@gmail.com"
								/>
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="primary" onClick={()=> this.clickModal()}>
							Enviar
							</Button>{' '}
							<Button
							color="secondary" onClick={()=> this.clickModal()}>
							Cancelar
							</Button>
						</ModalFooter>
					</Modal>
			 </Page>
		);
	}
}

export default MyLikedSells;