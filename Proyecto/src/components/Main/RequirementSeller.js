import share from 'assets/img/share.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Badge,
  Input,
  Label,
  FormGroup,
} from 'reactstrap';
import Avatar from 'components/Avatar';

class RequirementSeller extends React.Component {

    statusToString = (status) =>{
		//estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada
		let status_string= ""
		switch(status){
		  case 1: status_string="solicitada";
		  break;
		  case 2: status_string="aceptada";
		  break;
		  case 3: status_string="rechazada";
		  break;
		  case 4: status_string="cancelada";
		  break;
		  case 5: status_string="finalizada";
		  break;
		}
		return status_string
	}
    render(){
        const {order,onChangeOrderStatus} = this.props
        return (
            <Page className="custom-container">
                <Row>
                    <Col md={5}>
                        <Card style={{marginBottom:15}}>
                            <CardHeader>Datos generales de la orden:</CardHeader>
                            <CardBody>
                                <CardText>
                                    <b>Producto ordenado:</b> {order.action_post.post.title}
                                </CardText>
                                    <CardText>
                                        {(order.buyer.buyer_profile.name && order.buyer.buyer_profile.name!="" && order.buyer.buyer_profile.lastname && order.buyer.buyer_profile.lastname!="") ? 
                                        <><b>Nombre del cliente: </b>{order.buyer.buyer_profile.name} {order.buyer.buyer_profile.lastname}</>
                                        : 
                                        <><b>Correo del cliente: </b>{order.buyer.email}</>}
                                    </CardText>

                                <CardText>
                                    <b>fecha de pedido:</b> {order.date}
                                </CardText>
                                <CardText>
                                    <b>Estado:</b> {this.statusToString(order.status)}
                                </CardText>
                            </CardBody>
                        </Card>

                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Detalle del requerimiento</Label>
                                    <Input type="textarea" value={order.requirements} name="requirement" style={{height:100, background:'white'}} disabled/>
                                </Row>
                                <Row className="m-rl-0" style={{height:35, marginTop:15}}>
                                   
                                        <Col className="mb-3" xs={6}>
                                            <Link to="detail-order">
                                                <Button color="success" onClick={()=>{onChangeOrderStatus(order,2)}}>Aceptar</Button>
                                            </Link>
                                        </Col>
                                        <Col className="mb-3" xs={6}>
                                            <Link to="detail-order">
                                                <Button color="danger" onClick={()=>{onChangeOrderStatus(order,3)}}>Rechazar</Button>
                                            </Link>
                                        </Col>
                                    
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row> 
            </Page>
        );
    }
}

export default RequirementSeller;
