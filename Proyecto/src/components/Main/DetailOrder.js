import React from 'react'
import Page from 'components/Page';
import {
	Card, 
	CardHeader, 
	CardBody, 
    CardTitle,
    CardText, 
	Row, 
	Col, 
	Label, 
	Form, 
	FormGroup, 
	FormText, 
	Input, 
	Button,
	InputGroup,
	InputGroupAddon 
} from 'reactstrap' 
import { Link } from 'react-router-dom'
import {dateFormatWithMonthName} from '../Utils/dateParse'

 
class DetailOrder extends React.Component {

	constructor(props){ 
		super(props)
	}
    statusToString = (status) =>{
		//estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada
		let status_string= ""
		switch(status){
		  case 1: status_string="solicitada";
		  break;
		  case 2: status_string="aceptada";
		  break;
		  case 3: status_string="rechazada";
		  break;
		  case 4: status_string="cancelada";
		  break;
		  case 5: status_string="finalizada";
		  break;
          default:
            status_string="evaluada"
            break;
		}
		return status_string
    }
    
	render(){
        const {order,user,onDownloadImage} = this.props
        console.log()
		return(	
            <Page className="custom-container">
                <Row>
                    <Col xl={12} lg={12} md={12}>
                        <Card style={{marginBottom:15}}>
                            <CardHeader>Datos generales de la orden:</CardHeader>
                            <CardBody>
                                <CardText>
                                    <b>Producto ordenado:</b> {order.action_post.post.title}
                                </CardText>
                                {user.id!=order.buyer.buyer_id ?
                                    <CardText>
                                        {(order.buyer.buyer_profile.name && order.buyer.buyer_profile.name!="" && order.buyer.buyer_profile.lastname && order.buyer.buyer_profile.lastname!="") ? 
                                        <><b>Nombre del cliente: </b>{order.buyer.buyer_profile.name} {order.buyer.buyer_profile.lastname}</>
                                        : 
                                        <><b>Correo del cliente: </b>{order.buyer.email}</>}
                                    </CardText>
                                :
                                    <CardText>
                                        {(order.action_post.post.user.user_profile.name && order.action_post.post.user.user_profile.lastname)? 
                                        <><b>Nombre del vendedor: </b>{order.action_post.post.user.user_profile.name} {order.action_post.post.user.user_profile.lastname}</>
                                        : 
                                        <> <b>Correo del vendedor:</b> {order.action_post.post.user.email}</> }
                                    </CardText>
                                }
                                <CardText>
                                    <b>fecha de pedido: </b> {dateFormatWithMonthName(order.date)}
                                </CardText>
                                <CardText>
                                    <b>Estado:</b> {this.statusToString(order.status)}
                                </CardText>
                                {order.status == 4 &&
                                    <CardText>
                                        <b>Motivo de cancelacion:</b> {order.cancel_description}
                                    </CardText>
                                }
                            </CardBody>
                        </Card>
                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Condiciones del vendedor:</Label>
                                    <Input value={order.action_post.post.conditions} type="textarea" name="text" style={{height:100,color:"black",background:'white'}} disabled/>
                                </Row>
                            </CardBody>
                        </Card>
                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Detalle del requerimiento</Label>
                                    <Input type="textarea" value={order.requirements} name="requirements" style={{height:100, background:'white'}} disabled/>
                                </Row>
                                
                                    {user.id!=order.buyer.buyer_id ?
                                        (order.status==1 || order.status==2) &&
                                        <Row className="m-rl-0" style={{marginTop:15}}>
                                            <Col className="mb-3">
                                                <Link to="finish-order">
                                                    <Button color="success">Entregar Pedido</Button>
                                                </Link>
                                            </Col>
                                            <Col className="mb-3">
                                                <Link to="cancel-order">
                                                <Button color="danger">Cancelar Orden</Button>
                                                </Link>
                                            </Col>                                         
                                        </Row>
                                    :               
                                        <Row className="m-rl-0" style={{marginTop:15}}>
                                            {(order.status==1 || order.status==2) ?
                                            <Col className="mb-12" xs={12}>
                                                <Link to="cancel-order">
                                                    <Button color="danger">Cancelar Orden</Button>
                                                </Link>
                                            </Col>
                                            :
                                            (order.status==5)?
                                            <Link to="newUserQualification">
                                                <Button color="success">Evaluar Compra</Button>
                                            </Link>
                                            :
                                            (order.status==6) &&
                                            <div>
                                                <a href={"http://localhost:3000/purchase/download_image/"+order.pictures[0]} color="info">Descargar Pedido</a>                             
                                            </div>
                                            }
                                        </Row>
                                    }
                                    
                                
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Page>
		);
	}
}

export default DetailOrder; 