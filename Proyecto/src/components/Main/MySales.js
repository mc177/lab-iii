import React from 'react';
import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'
import Page from 'components/Page';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardHeader,
  Badge,
  CardText,
  CardTitle,
  Col,
  Row,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  Progress
} from 'reactstrap';

class MySales extends React.Component{
	render(){
		return(
			<Page style={{textAlign: "center"}} className="custom-container">
					<Row className="row-custom">
				        <Col md={8} sm={6} xs={12} className="mb-3">
					          <Card>
					            <CardImg top src={bg11Image} />
					            <CardBody>
					              <CardHeader style={{padding: 0}}>
					              	<CardTitle>Ilustración 3D de Rostro</CardTitle>
					              	<div className="chip-status">
						              	<Badge id="popProgress" color="primary" title="Estado" style={{cursor: "pointer"}} className="mr-1">
								          En progreso
								        </Badge>
						              	<UncontrolledPopover trigger="legacy" placement="bottom" target="popProgress">
					                        <PopoverHeader style={{background: "#6a82fb"}}>En progreso</PopoverHeader>
								          	<PopoverBody>
								          		<Col  xs={12} className="mb-3">
										            <Card>
											            <CardBody style={{textAlign:"center", paddinBottom: "4px"}}>
												            <Progress
											                  animated={true}
											                  color="blue"
											                  value={75}
											                  className="mb-3"
											                />
											                75% realizado
											            </CardBody>
									                </Card>
										        </Col>
										        La venta de la ilustración se encuentra en progreso, esto significa que solo queda entregar el producto 
										        y concretar la venta con el comprador.
								            </PopoverBody>
							            </UncontrolledPopover>
						             	<Badge id="categoryIlustration" title="Categoría" color="info"  style={{cursor: "pointer"}} className="mr-1">
								          Ilustración
								        </Badge>
						              	<UncontrolledPopover trigger="legacy" placement="bottom" target="categoryIlustration">
					                        <PopoverHeader style={{background: "rgb(0, 201, 255)", color: "white"}}>Categoría Ilustración</PopoverHeader>
								          	<PopoverBody>
										        Esta categoría se basa principalmente en ilustraciones realizados con 
										        técnicas especiales de dibujo.
								            </PopoverBody>
							            </UncontrolledPopover>
							         </div>
					              </CardHeader>
					              <br/>
					              <CardText>
					                Se realiza una ilustaración 3D de rostro femenino, con fondo degradado relacionado con una 
					                galáxia. Se usan las mejores técnicas de iluminación y una paleta de colores variada
					              </CardText>
					            </CardBody>
					            <div style={{padding: "4%",marginTop: "-10px"}}>
					            	<Button className="btn btn-success button-succes-custom button-view-custom"><FontAwesomeIcon icon={faEye}/></Button>
					            		<br/>
					            	<label style={{marginTop: "7px", fontSize: "small", marginBottom: 0}}> Ver detalles </label>
					            </div>
					          </Card>
					          <br/>
					          <Card>
					            <CardImg top src={bg3Image} />
					            <CardBody>
					              <CardHeader style={{padding: 0}}>
					              	<CardTitle>Ilustración 3D de Vehiculo</CardTitle>
					              	<div className="chip-status">
						              	<Badge id="PopoverLegacy" color="success" title="Estado"  style={{cursor: "pointer"}} className="mr-1">
								          Finzalizada
								        </Badge>
						              	<UncontrolledPopover trigger="legacy" placement="bottom" target="PopoverLegacy">
					                        <PopoverHeader style={{background: "#45b649"}}>Finzalizada</PopoverHeader>
								          	<PopoverBody>
								          		<Col  xs={12} className="mb-3">
										            <Card>
											            <CardBody style={{textAlign:"center", paddinBottom: "4px"}}>
												            <Progress
											                  animated={true}
											                  color="success"
											                  value={100}
											                  className="mb-3"
											                />
											                100% realizado
											            </CardBody>
									                </Card>
										        </Col>
										        La venta de la ilustración se encuentra Finzalizada, esto significa que la venta se ha 
										        realizado con éxito.
								            </PopoverBody>
							             </UncontrolledPopover>
							             <Badge id="categoryIlustration2" title="Categoría" color="info"  style={{cursor: "pointer"}} className="mr-1">
								          Ilustración
								        </Badge>
						              	<UncontrolledPopover trigger="legacy" placement="bottom" target="categoryIlustration2">
					                        <PopoverHeader style={{background: "rgb(0, 201, 255)", color: "white"}}>Categoría Ilustración</PopoverHeader>
								          	<PopoverBody>
										        Esta categoría se basa principalmente en ilustraciones realizados con 
										        técnicas especiales de dibujo.
								            </PopoverBody>
							            </UncontrolledPopover>
						             </div>
					              </CardHeader>
					              <br/>
					              <CardText>
					                Se realiza una ilustaración 3D de un vehiculo de los 80's. Se usa técnicas realista y 
					                subrealistas para lograr acabados magníficos.
					              </CardText>
					            </CardBody>
					            <div style={{padding: "4%",marginTop: "-10px"}}>
					            	<Button className="btn btn-success button-succes-custom button-view-custom"><FontAwesomeIcon icon={faEye}/></Button>
					            		<br/>
					            	<label style={{marginTop: "7px", fontSize: "small", marginBottom: 0}}> Ver detalles </label>
					            </div>
					          </Card>
				        </Col>
				    </Row>
			 </Page>
		);
	}
}

export default MySales;