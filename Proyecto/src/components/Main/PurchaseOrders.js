import Page from 'components/Page';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import UserProgressTable from 'components/UserProgressTable';
import classNames from 'classnames';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardHeader,
  CardLink,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Table,
  Input,
} from 'reactstrap';
import { MdPersonPin} from 'react-icons/md';
import { Link, Redirect } from 'react-router-dom'
import {dateFormatWithoutMonthName} from '../Utils/dateParse'

const headers=[
  'n°',
  'fecha',
  'comprador',
  'estado',
  'opciones'
]


class PurchaseOrders extends React.Component {

  statusToString = (status) =>{
    //estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada
    let status_string= ""
    switch(status){
      case 1: status_string="solicitada";
      break;
      case 2: status_string="aceptada";
      break;
      case 3: status_string="rechazada";
      break;
      case 4: status_string="cancelada";
      break;
      case 5: status_string="finalizada";
      break;
      default:
        status_string="evaluada"
    }
    return status_string
  }

  render(){
    const {my_sales,onShowOrder} = this.props
    console.log(my_sales)
    return (
      <Page className="custom-container">
          <Row className=" m-0">   
            <Col md="5" sm="12" xs="12">
                <Card>
                  <CardBody>
    
                    <Row className="m-0">
                      <Col style={{paddingLeft:0, paddingRight:0}}>
                      <Input className="mb-2" type="select">
                        <option>Todas</option>
                        <option>Solicitadas</option>
                        <option>Aceptadas</option>
                        <option>Canceladas</option>
                        <option>Finalizadas</option>
                      </Input>
                      </Col>
                    </Row>
                    <Table responsive hover>
                      <thead>
                        <tr className="text-capitalize align-middle text-center">
                          {headers.map((item, index) => <th key={index}>{item}</th>)}
                        </tr>
                      </thead>
                      {(my_sales && my_sales.length>0) &&
                      <tbody>
                          {my_sales.map((sale, index) => (                           
                                <tr key={index} onClick={this.onRedirectDetailOrder}>
                                {console.log(sale.buyer.buyer_profile.name)}
                                    <td>{index + 1}</td>
                                    <td className="align-middle text-center">{dateFormatWithoutMonthName(sale.date)}</td>
                                    <td className="align-middle text-center">{sale.buyer.buyer_profile.name} {sale.buyer.buyer_profile.lastname}</td>
                                    <td className="align-middle text-center">{this.statusToString(sale.status)}</td>
                                    <td className="align-middle text-center">
                                    {sale.status == 1 ?
                                      <Link to="requirement-seller"> 
                                        <Button color="info" onClick={()=>{onShowOrder(sale)}}>Ver</Button>
                                      </Link>
                                    :
                                      <Link to="detail-order">
                                          <Button color="info" onClick={()=>{onShowOrder(sale)}}>Ver</Button>
                                        </Link>
                                    }
                                      
                                    </td>
                                </tr>
                          ))}
                      </tbody>
                      }
                    </Table>
                  </CardBody>
                </Card>
              </Col>
          </Row>         
      </Page>
    );
  }
}

export default PurchaseOrders;
