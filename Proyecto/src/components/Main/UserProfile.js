import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
	FormText,
	Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';


class UserProfile extends React.Component {

	constructor(props){
		super(props)
		this.toggle = this.toggle.bind(this);
		this.state = {
			user: {
				name: "Jeferson",
				forename: "Alvarado",
				email: "jejoalca14@gmail.com",
				role: "Seller and Buyer",
				phone: "04245558208",
				adress: "Av. principal San Lorenzo",
				birthdate: "1996-07-30",
				sex: "M",
				description: "Me gusta ésta página",
			},
			modal: false,
			modal_validation: false,
			modal_validation_fields: false,
			toggleMenu: {
				changePassword: false,
				updateProfile: true
			}
		}
	}

	clickModal = (e) =>{
		return this.setState({
				modal: !this.state.modal,
		}); 
}

clickModalValidation = (e) =>{
	return this.setState({
			modal_validation: !this.state.modal_validation,
	}); 
}

clickModalValidationFields = (e) =>{
	return this.setState({
			modal_validation_fields: !this.state.modal_validation_fields,
	}); 
}

	toggle() {
	    this.setState(prevState => ({
	      dropdownOpen: !prevState.dropdownOpen
	    }));
	 }

	onHandleChangePage(event){
		let toggleMenu = {}
		let user = Object.assign({}, this.state.user,{
				newPassword: "",
				passwordConfirmation: ""
			})
		if(event.target.dataset.value === "Cambiar contraseña"){
			toggleMenu = Object.assign({}, this.state.toggleMenu, {
				updateProfile: false,
				changePassword: true
			})
		}else{
			toggleMenu = Object.assign({}, this.state.toggleMenu, {
				updateProfile: true,
				changePassword: false
			})
		}
		this.setState({toggleMenu, user})
	}

	onHandleInputChange(event){
		let user = Object.assign({}, this.state.user, {
			[event.target.name]: event.target.value
		})

		this.setState({user})
	}

	render(){
		const {user, onHandleProfileImageChagne, countries,change_password,onHandleUserProfileInputChange,onChangePassword,resetChancePassword, onHandleUserInputChange,onCancelEditProfile,onSaveUserProfile,onHandleChangePasswordInputChange} = this.props
		return(
			<div className="custom-container-without-space">
				<div className="d-flex justify-content-center align-items-center flex-column card-body" style={{background: "#e6ebec"}}>
					<label htmlFor="imageFile" style={{marginBottom: "0"}}>
						<img src={(user && user.user_profile && user.user_profile.image)? process.env.REACT_APP_VERONICA_URL_WEB + 'uploads/users/' + user.user_profile.image : process.env.REACT_APP_VERONICA_URL_WEB + 'assets/user_default.png' + user.user_profile.image} className="rounded-circle mb-2" style={{width: "125px", height: "125px"}} />
					</label>
					<div>
						<input type="file" name="imageFile" id="imageFile" className="change-image" data-multiple-caption="{count} files selected" onChange={onHandleProfileImageChagne}/>
						<label htmlFor="imageFile">
							<span className="label-change-image">Cambiar la foto de perfil</span>
						</label>
					</div>
					<div className="card-title" style={{fontSize: "larger"}}>{user.user_profile_saved_values.name + " " + user.user_profile_saved_values.lastname}</div>
					<div className="card-subtitle" style={{fontSize: "small", fontWeight: "bold"}}>{user.email}</div>
					<p className="card-text"><small>{user.role ? user.role.name : ""}</small></p>
				</div>
				<Card>
		            <CardHeader style={{textAlign: "center", fontWeight: "bold"}}>
		             <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} className="dropcustom">
				        <DropdownToggle className="toggle-custom toggle-hover" caret>
				          {(this.state.toggleMenu.updateProfile)? "Actualizar Datos de perfil" : "Cambiar contraseña"}
				        </DropdownToggle>
				        <DropdownMenu className="menu-custom">
				          <DropdownItem data-value={(this.state.toggleMenu.updateProfile)? "Cambiar contraseña" : "Actualizar Datos de perfil"} className={(this.state.toggleMenu.updateProfile)? "dropdown-item-custom" : "dropdown-item-custom-changed"} onClick={(e) => {this.onHandleChangePage(e);resetChancePassword()}}>{(this.state.toggleMenu.updateProfile)? "Cambiar contraseña" : "Actualizar Datos de perfil"}</DropdownItem>
				        </DropdownMenu>
				      </Dropdown>
		           	</CardHeader>
		            <CardBody>
		            {
		             (this.state.toggleMenu.updateProfile)?
		             <div>
			              <Form>
			                <FormGroup>
			                  <Label htmlFor="name">Nombre</Label>
			                  <Input
			                  	type="text"
			                  	name="name"
			                  	value={user.user_profile_saved_values.name}
			                  	onChange={(e)=> onHandleUserProfileInputChange(e)}
			                  	placeholder="Introduce un nombre"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="lastname">Apellido </Label>
			                  <Input
			                    type="text"
			                    name="lastname"
			                    value={user.user_profile_saved_values.lastname}
			                  	onChange={(e)=> onHandleUserProfileInputChange(e)}
			                    placeholder="Apellido"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="phone">Teléfono</Label>
			                  <Input
			                    type="number"
			                    name="phone"
			                    value={user.user_profile_saved_values.phone}
			                  	onChange={(e)=> onHandleUserProfileInputChange(e)}
			                    placeholder="Ingrese número de teléfono"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="birthdate">Fecha de nacimiento</Label>
			                  <Input
			                    type="date"
			                    name="birthday"
			                    format="dd-mm-yyyy"
			                    value={user.user_profile_saved_values.birthday}
			                  	onChange={(e)=> onHandleUserProfileInputChange(e)}
			                    placeholder="Fecha de nacimiento"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="sex">Sexo</Label>
			                  <Input value={user.user_profile_saved_values.sex} onChange={(e) => onHandleUserProfileInputChange(e)} type="select" name="sex">
			                    <option value="-1">Selecciona una opción</option>
			                    <option value="1">Masculino</option>
			                    <option value="2">Femenino</option>
			                    <option value="3">Prefiero no decirlo</option>
			                  </Input>
			                </FormGroup>
											<FormGroup>
												<Label>País</Label>
												<Input type="select" name="country_id" value={user.user_profile_saved_values.country_id} onChange={(e) => onHandleUserProfileInputChange(e)}>
																	<option value="">Seleccione una país</option>
																	{countries.map((country,index)=>(
																		<option key={index} value={country.id}>{country.name}</option>
																	))}
												</Input>
											</FormGroup>
			              </Form>
			              <div>
			              	<Button color="secondary" style={{float: "right"}} onClick={()=>onCancelEditProfile()}>Cancelar</Button>
			              </div>
			              <div>
			              	<Button className="btn btn-success button-succes-custom" style={{float: "right", marginRight: "21px"}} onClick={()=> {onSaveUserProfile(user);this.clickModal()}}>Guardar</Button>
			              </div>
		             	</div>
		              :
		              <div>
		              	<Form>
			              <FormGroup>
			                  <Label htmlFor="current_password">Contraseña actual</Label>
			                  <Input
			                  	type="password"
			                  	name="current_password"
			                  	value={change_password.current_password}
			                  	onChange={(e)=> onHandleChangePasswordInputChange(e)}
			                  	placeholder="Introduce tu contraseña"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="new_password">Nueva contraseña </Label>
			                  <Input
			                    type="password"
			                    name="new_password"
			                    value={change_password.new_password}
			                  	onChange={(e)=> onHandleChangePasswordInputChange(e)}
			                    placeholder="Ingresa la nueva contraseña"
			                  />
			                </FormGroup>
			                <FormGroup>
			                  <Label htmlFor="new_password_confirm">Confirmación de contraseña</Label>
			                  <Input
			                    type="password"
			                    name="new_password_confirm"
			                    value={change_password.new_password_confirm}
			                  	onChange={(e)=> onHandleChangePasswordInputChange(e)}
			                    placeholder="Ingresa tu confirmación de contraseña"
			                  />
			                </FormGroup>
		                </Form>
		                <div>
			              <Button color="secondary" style={{float: "right"}} onClick={()=>console.log("cancelar cambio de password")}>Cancelar</Button>
			            </div>
									{(change_password.current_password!="" && change_password.new_password!="" && change_password.new_password_confirm!="" && change_password.new_password==change_password.new_password_confirm) ?
										<div>
											<Button className="btn btn-success button-succes-custom" style={{float: "right", marginRight: "21px"}} onClick={()=>{this.clickModalValidation()}}>Guardar</Button>
										</div>
									:
										<div>
											<Button className="btn btn-success button-succes-custom" style={{float: "right", marginRight: "21px"}} onClick={()=>{this.clickModalValidationFields()}}>Guardar</Button>
										</div>
									}
			           </div>
		            }
		            </CardBody>
          		</Card>
							<Modal
									isOpen={this.state.modal}>
									<ModalHeader>Perfil guardado con éxito!</ModalHeader>
									<ModalBody>
											<FormGroup>
													<Label for="email">Los datos de su perfil han sido actualizados con éxito</Label>{' '}
											</FormGroup>
									</ModalBody>
									<ModalFooter>
										<Button color="primary" onClick={()=> {this.clickModal();}}>
												OK
										</Button>
									</ModalFooter>
							</Modal>


							<Modal
									isOpen={this.state.modal_validation_fields}>
									{(change_password.current_password=="" || change_password.new_password=="" || change_password.new_password_confirm=="") ?
										<>
										<ModalHeader>Campos faltantes</ModalHeader>
											<ModalBody>
													<FormGroup>
															<Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
													</FormGroup>
											</ModalBody>
											<ModalFooter>
															<Button color="primary" onClick={()=> {this.clickModalValidationFields()}}>
																	Aceptar
															</Button>                  
											</ModalFooter>
										</>
									:
										<>
											<ModalHeader>Los campos no coinciden</ModalHeader>
											<ModalBody>
													<FormGroup>
															<Label for="email">La nueva contraseña debe coincidir con la confirmación</Label>{' '}
													</FormGroup>
											</ModalBody>
											<ModalFooter>
															<Button color="primary" onClick={()=> {this.clickModalValidationFields()}}>
																	Aceptar
															</Button>                     
											</ModalFooter>
										</>				
									}
							</Modal>


							<Modal
									isOpen={this.state.modal_validation}>
											<ModalHeader>La contraseña será cambiada</ModalHeader>
									<ModalBody>
											<FormGroup>
													<Label for="email">¿Está seguro que desea realizar esta acción?</Label>{' '}
											</FormGroup>
									</ModalBody>
									<ModalFooter>
													<Button color="primary" onClick={()=> {this.clickModalValidation();onChangePassword(change_password)}}>
															Aceptar
													</Button> 
													<Button color="danger" onClick={()=> {this.clickModalValidation();resetChancePassword()}}>
															Cancelar
													</Button>                      
									</ModalFooter>
							</Modal>

			</div>
		);
	}
}

export default UserProfile;