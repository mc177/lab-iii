import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg18Image from 'assets/img/bg/background_1920-18.jpg';
import bg1Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import yellowStar from 'assets/img/star_checked.png'
import like from 'assets/img/like.png'
import border_like from 'assets/img/border-like.png'
import share from 'assets/img/share.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Badge,
  Input,
  Label,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import Avatar from 'components/Avatar';

class RequirementSeller extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modal: false,
            modal_validation: false
        };
	}

    clickModal = (e) =>{
        return this.setState({
            modal: !this.state.modal,
        }); 
    }

    clickModalValidation = (e) =>{
        return this.setState({
            modal_validation: !this.state.modal_validation,
        }); 
    }
    
    render(){
        const {order,my_purchases,onCancelOrder,onHandleCancelOrderDescription} = this.props
        return (
            <Page className="custom-container">
                <Row>
                    <Col md={5}>
                        <Card style={{marginBottom:15}}>
                            <CardHeader>Cancelar orden:</CardHeader>
                            <CardBody>
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Razones de la cancelación</Label>
                                    <Input type="textarea" value={order.cancel_description} onChange={(e) => onHandleCancelOrderDescription(e)} name="cancel_description" style={{height:100, background:'white'}}/>
                                </Row>
                                <Row className="m-rl-0" style={{height:35, marginTop:15}}>
                                    {(order.cancel_description && order.cancel_description!="") ?
                                        <Col className="mb-3 justify-content-center" >
                                            <Button color="success" onClick={()=> {this.clickModal()}}>Aceptar</Button>
                                        </Col>
                                    :
                                        <Col className="mb-3 justify-content-center" >
                                            <Button color="success" onClick={()=> {this.clickModalValidation()}}>Aceptar</Button>
                                        </Col>
                                    }
                                    <Link to="detail-order">
                                        <Col className="mb-3 justify-content-center" >
                                            <Button color="danger">Regresar</Button>
                                        </Col>
                                    </Link>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Modal
                    isOpen={this.state.modal}>
                        <ModalHeader>Está seguro que desea cancelar la orden?</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="email">esta opción no podrá ser revertida</Label>{' '}
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Link to="/detail-order">
                            <Button color="primary" onClick={()=> {this.clickModal();onCancelOrder(order)}}>
                                Si
                            </Button>
                        </Link>
                        <Button color="primary" onClick={()=> {this.clickModal()}}>
                            No
                        </Button>
                        
                    </ModalFooter>
                </Modal> 
                <Modal
                    isOpen={this.state.modal_validation}>
                        <ModalHeader>Campo faltante</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="email">Debe establecer un motivo de cancelación</Label>{' '}
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                            <Button color="primary" onClick={()=> {this.clickModalValidation()}}>
                                Aceptar
                            </Button>                      
                    </ModalFooter>
                </Modal>  
            </Page>
        );
    }
}

export default RequirementSeller;
