import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import Page from 'components/Page';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import UserProgressTable from 'components/UserProgressTable';
import classNames from 'classnames';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardHeader,
  CardLink,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Table,
  Progress
} from 'reactstrap';
import { MdPersonPin} from 'react-icons/md';

import user1Image from 'assets/img/users/100_1.jpg';
import Avatar from 'components/Avatar';
import withBadge from 'hocs/withBadge';

const AvatarWithBadge = withBadge({
  position: 'bottom-right',
  color: 'success',
})(Avatar);

const headers=[
  'n°',
  <MdPersonPin size={25} />,
  'nombre/correo',
  'Total',
]
class EarningsAdmin extends React.Component {
  render(){
    const{user_gains} = this.props
    console.log(user_gains)
    return (
      <Page className="custom-container">
          <Row className=" m-0">
              <Col md="5" sm="12" xs="12">
                  <Card>
                  <CardHeader>Ranking de usuarios</CardHeader>
                  <CardBody>
                    <Table responsive hover>
                      <thead>
                        <tr className="text-capitalize align-middle text-center">
                          {headers.map((item, index) => <th key={index}>{item}</th>)}
                        </tr>
                      </thead>
                      <tbody>
                        {user_gains.map((user_gain, index) => (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td className="align-middle text-center">
                              <img className="rounded-circle can-click flipthis-highlight user-image-cardsells" src={user1Image} />
                            </td>
                            <td className="align-middle text-center">{(user_gain.name && user_gain.name!="")? user_gain.name:user_gain.email}</td>
                            <td className="align-middle text-center">{user_gain.total_gain}</td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </CardBody>
                  </Card>
              </Col>
          </Row>      
        
      </Page>
    );
  }
};

export default EarningsAdmin;
