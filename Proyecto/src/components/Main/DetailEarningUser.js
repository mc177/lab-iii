import React from 'react';
import { getColor } from 'utils/colors';
import { randomNum } from 'utils/demos';
import { Row, Col, Card, CardHeader, CardBody, Badge,CardTitle, } from 'reactstrap';
import { Line, Pie, Doughnut, Bar, Radar, Polar } from 'react-chartjs-2';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faPaperPlane, faArrowLeft} from '@fortawesome/free-solid-svg-icons'

import Page from 'components/Page';
import {Link} from 'react-router-dom'
const genPieData = (weeks) => {
  return {
    datasets: [
      {
        data: [weeks[0].weekly_total, weeks[1].weekly_total, weeks[2].weekly_total, weeks[3].weekly_total],
        backgroundColor: [
          getColor('primary'),
          getColor('secondary'),
          getColor('success'),
          getColor('info'),
        ],
        label: 'Dataset 1',
      },
    ],
  };
};

class DetailEarningUser extends React.Component {
  monthToString = (month) =>{
		let month_string= ""
		switch(month){
		  case 1: month_string="Ene";
		  break;
		  case 2: month_string="FEB";
		  break;
		  case 3: month_string="MAR";
		  break;
		  case 4: month_string="ABR";
		  break;
		  case 5: month_string="MAY";
      break;
      case 6: month_string="JUN";
      break;
      case 7: month_string="JUL";
      break;
      case 8: month_string="AGO";
      break;
      case 9: month_string="SEP";
      break;
      case 10: month_string="OCT";
      break;
      case 11: month_string="NOV";
      break;
      case 12: month_string="DIC";
		  break;
		}
		return month_string
	}
  render(){
    const{user,monthly_gains,month_gain} = this.props
    return (
      <Page className="custom-container">
        <Row>
          <Col xl={6} lg={12} md={12}>
            <Card>
              <CardHeader>
                <Link to="/earnings" className="left-arrow"> <FontAwesomeIcon icon={faArrowLeft}/> </Link>
                <CardTitle style={{marginTop:5}}>Ganancias semanales: {this.monthToString(month_gain.month)}</CardTitle>
              </CardHeader>
              <CardBody>
                  <Row className="m-0">
                      <Col xs={6} style={{paddingLeft:0,paddingRight:0,marginBottom:0}}>
                          <label>
                              <Badge color="primary" pill className="mr-1">
                              Semana 1: 
                              </Badge>
                              {month_gain.weeks[0].weekly_total}
                          </label>
                          <label>
                              <Badge color="secondary" pill className="mr-1">
                              Semana 3: 
                              </Badge>
                              {month_gain.weeks[2].weekly_total}
                          </label>
                      </Col>
                      <Col xs={6} style={{paddingLeft:0,paddingRight:0,marginBottom:0}}>
                          <label>
                              <Badge color="success" pill className="mr-1">
                              Semana 2: 
                              </Badge>
                              {month_gain.weeks[1].weekly_total}
                          </label >
                          <label>
                              <Badge color="info" pill className="mr-1">
                              Semana 4: 
                              </Badge>
                              {month_gain.weeks[3].weekly_total}
                          </label>
                      </Col>
                  </Row>

                <Doughnut data={genPieData(month_gain.weeks)} />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Page>
    );
  }
};

export default DetailEarningUser;
