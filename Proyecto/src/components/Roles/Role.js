import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Role extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {rol,index, onEditRol, onDeleteRol, statusAsString} = this.props

		return(
			<>
			<tr key={rol.id}>
				<td> {index} </td>		
				<td> {rol.name} </td>
				<td> {rol.description} </td>
				<td> {statusAsString(rol.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditRol(rol.id)}> 
						Modificar
					</Button> 
				</td>
				<td>
					{ rol.status==1 &&
						<Button color="secondary" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button>
					} 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={rol} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteRol} />
			</>
		);
	}
}

export default Role;