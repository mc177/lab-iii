import React from 'react'
import { Table } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';

import Role from './Role'
import RolEdit from './RoleEdit'
import RolNew from './RoleNew'

class RolList extends React.Component {

	constructor(props){
		super(props)

		this.hasSomeRoleEditing = this.hasSomeRoleEditing.bind(this)		
	}
	hasSomeRoleEditing(roles){
		let _is_some = false 
		if(roles.length > 0){
			roles.map((role) => {
				if(role.isEditing){
					_is_some=true
				}
			})
		}
		return _is_some
	}
	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}
	
	render(){
		const {roles, 
			onHandleNameInputChange, 
			onHandleDescriptionInputChange, 
			onHandleStatusInputChange, 
			onAddNewRol,
			onCancelNewRol, 
			onDeleteRol, 
			onEditRol, 
			onCancelEditRol,
			onSaveNewRol,
			onUpdateRol,
		} = this.props
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Rols 
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>Nro</th>
			            <th>Nombre</th>
			            <th>Descripción</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomeRoleEditing(roles))?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(roles && roles.length > 0)? 
			        		roles.map((rol,index) => (
			        			(rol.isEditing)?
									<RolEdit key={index} role={rol} index={index+1}
										onHandleNameInputChange={(e)=>onHandleNameInputChange(e,rol.id)}
										 onHandleDescriptionInputChange = {(e)=>onHandleDescriptionInputChange(e,rol.id)}
										 onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,rol.id)} 
										 onCancelEditRole={(text)=>onCancelEditRol(text,rol.id)}
										 onUpdateRol = {(r)=>onUpdateRol(r)}									 
									/>
			        			:
			        			(rol.isNew)?
			        				<RolNew key={rol.id} rol={rol} index={index+1} 
										onHandleNameInputChange={(e)=>onHandleNameInputChange(e,rol.id)}
										onHandleDescriptionInputChange = {(e)=>onHandleDescriptionInputChange(e,rol.id)}
										onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,rol.id)}
										onCancelAddNewRol = {()=>onCancelNewRol(rol.id)}
										onSaveNewRol = {()=>onSaveNewRol(rol)}
									/>
			        			:
									<Role index={index+1} key={rol.id} rol={rol} statusAsString={this.statusAsString}
									onDeleteRol ={()=>onDeleteRol(rol.id)}
									onEditRol = {()=>onEditRol(rol.id)}	
									/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={7}> No existen paises registrados </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={7}>
					        <Button color="primary" onClick={() => onAddNewRol()} >
					        	Agregar Rol
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default RolList;