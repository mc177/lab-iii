import React from 'react'
import {Button, Input} from 'reactstrap'

class CountryNew extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {
			country, 
			index,
			countries, 
			onHandleNameInputChange, 
			onHandleCountryInputChange,
			onCancelNewCountry,
			onHandleStatusInputChange,
			onSaveNewCountry,
		} = this.props

		return(
			<tr key='*'>
				<td>NUEVO</td>
				<td> 
					<Input
						//data-id={country.id}
						name="name"
						defaultValue={country.name}
						value={country.name}
						onChange={(e) => onHandleNameInputChange(e,country.id)}
					/>
				</td>		
				<td>  
          <Input data-id={country.id} defaultValue={country.status} onChange={(e) => onHandleStatusInputChange(e,country.id)} type="select" name="status">
            <option value={-1}>Selecciona una opción</option>
            <option value={1}>Activo</option>
            <option value={2}>Inactivo</option>
          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onSaveNewCountry(country)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelNewCountry(country.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default CountryNew;