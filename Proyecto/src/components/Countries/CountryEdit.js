import React from 'react'
import {Button, Input} from 'reactstrap'

class CountryEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {index, country, onUpdateCountry, onCancelEditCountry, onHandleCountryInputChange, onHandleNameInputChange,onHandleStatusInputChange} = this.props
		return(
			<tr key={country.id}>
				<td> {index+1}</td>
				<td> 
					<Input 
						data-id={country.id}
						name="name"
						defaultValue={country.name}
						value={country.name}
						onChange={(e) => onHandleNameInputChange(e,country.id)}
					/>
				</td>		
				<td>  
          <Input data-id={country.id} defaultValue={country.status} onChange={(e) => onHandleStatusInputChange(e,country.id)} type="select" name="status">
            <option value={-1}>Selecciona una opción</option>
            <option value={1}>Activo</option>
            <option value={2}>Inactivo</option>
          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdateCountry(country)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditCountry(country.originalName,country.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default CountryEdit;