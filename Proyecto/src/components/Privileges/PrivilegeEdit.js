import React from 'react'
import {Button, Input} from 'reactstrap'

class PrivilegeEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {privilege, onUpdatePrivilege, onCancelEditPrivilege, onHandlePrivilegeInputChange} = this.props

		return(
			<tr key={privilege.id}>
				<td> {privilege.id} </td>
				<td> 
					<Input 
						data-id={privilege.id}
						name="name"
						defaultValue={privilege.name}
						onChange={(e) => onHandlePrivilegeInputChange(e,privilege.id)}
					/>
				</td>
				<td> 
					<Input 
						data-id={privilege.id}
						name="description"
						defaultValue={privilege.description}
						onChange={(e) => onHandlePrivilegeInputChange(e,privilege.id)}
					/>
				</td>		
				<td>  
		          <Input data-id={privilege.id} defaultValue={privilege.status} onChange={(e) => onHandlePrivilegeInputChange(e,privilege.id)} type="select" name="status">
		            <option value={-1}>Selecciona una opción</option>
		            <option value={1}>Activo</option>
		            <option value={2}>Inactivo</option>
		          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdatePrivilege(privilege)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditPrivilege(privilege.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default PrivilegeEdit;