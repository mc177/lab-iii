import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Privilege extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {privilege, onEditPrivilege, onDeletePrivilege, statusAsString} = this.props

		return(
			<>
			<tr key={privilege.id}>
				<td> {privilege.id} </td>		
				<td> {privilege.name} </td>
				<td> {privilege.description} </td>
				<td> {statusAsString(privilege.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditPrivilege(privilege.id)}> 
						Modificar
					</Button> 
				</td>
				<td> 
					<Button color="secondary" onClick={() => this.openHandleModal()}> 
						Eliminar
					</Button> 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={privilege} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeletePrivilege} />
			</>
		);
	}
}

export default Privilege;