import React from 'react'
import { Table } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';

import Privilege from './Privilege'
import PrivilegeEdit from './PrivilegeEdit'
import PrivilegeNew from './PrivilegeNew'

class PrivilegeList extends React.Component {

	constructor(props){
		super(props)
		this.hasSomePrivilegeEditing = this.hasSomePrivilegeEditing.bind(this)

	}

	hasSomePrivilegeEditing(privileges){
		let _is_some = false 
		privileges.map((privilege) => {
			if(privilege.isEditing){
				_is_some=true
			}
		})
		return _is_some
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	render(){
		const {
			privileges,
			onCancelEditPrivilege,
			onDeletePrivilege,
			onEditPrivilege,
			onUpdatePrivilege,
			onSaveNewPrivilege,
			onHandlePrivilegeInputChange,
			onAddNewPrivilege
		} = this.props
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Privilegios 
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>ID</th>
			            <th>Nombre</th>
			            <th>Descripción</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomePrivilegeEditing(privileges))?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(privileges && privileges.length > 0)? 
			        		privileges.map((privilege) => (
			        			(privilege.isEditing)?
			        				<PrivilegeEdit key={privilege.id} privilege={privilege} onUpdatePrivilege={onUpdatePrivilege} onHandlePrivilegeInputChange={onHandlePrivilegeInputChange} onCancelEditPrivilege={onCancelEditPrivilege}/>
			        			:
			        			(privilege.isNew)?
			        				<PrivilegeNew key={privilege.id} privilege={privilege} onDeletePrivilege={onDeletePrivilege} onSaveNewPrivilege={onSaveNewPrivilege} onHandlePrivilegeInputChange={onHandlePrivilegeInputChange} />
			        			:
			        				<Privilege key={privilege.id} privilege={privilege} statusAsString={this.statusAsString} onEditPrivilege={onEditPrivilege} onDeletePrivilege={onDeletePrivilege}/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={7}> No existen paises registrados </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={7}>
					        <Button color="primary" onClick={() => onAddNewPrivilege()} >
					        	Agregar Privilegio
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default PrivilegeList;