import logo200Image from 'assets/img/logo/logo_200.png';
import sidebarBgImage from 'assets/img/sidebar/sidebar-4.jpg';
import SourceLink from 'components/SourceLink';
import React from 'react';
import { FaGithub } from 'react-icons/fa';
import {
  MdAccountCircle,
  MdArrowDropDownCircle,
  MdBorderAll,
  MdBrush,
  MdChromeReaderMode,
  MdDashboard,
  MdExtension,
  MdGroupWork,
  MdInsertChart,
  MdKeyboardArrowDown,
  MdNotificationsActive,
  MdPages,
  MdRadioButtonChecked,
  MdSend,
  MdStar,
  MdTextFields,
  MdViewCarousel,
  MdViewDay,
  MdViewList,
  MdWeb,
  MdWidgets,
} from 'react-icons/md';
import { NavLink, Link } from 'react-router-dom';
import {
  // UncontrolledTooltip,
  Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Button,
} from 'reactstrap';
import bn from 'utils/bemnames';
import {connect} from 'react-redux'
import {
  resetUserData
} from '../../actions/mainAppAction'
import { bindActionCreators } from 'redux';

const sidebarBackground = {
  backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const navComponents = [
  { to: '/buttons', name: 'buttons', exact: false, Icon: MdRadioButtonChecked },
  {
    to: '/button-groups',
    name: 'button groups',
    exact: false,
    Icon: MdGroupWork,
  },
  { to: '/forms', name: 'forms', exact: false, Icon: MdChromeReaderMode },
  { to: '/input-groups', name: 'input groups', exact: false, Icon: MdViewList },
  {
    to: '/dropdowns',
    name: 'dropdowns',
    exact: false,
    Icon: MdArrowDropDownCircle,
  },
  { to: '/badges', name: 'badges', exact: false, Icon: MdStar },
  { to: '/alerts', name: 'alerts', exact: false, Icon: MdNotificationsActive },
  { to: '/progress', name: 'progress', exact: false, Icon: MdBrush },
  { to: '/modals', name: 'modals', exact: false, Icon: MdViewDay },
];

const navRankings = [
  { to: '/category-ranking', name: 'Categorías populares', exact: false, Icon: MdBorderAll },
  { to: '/earnings-admin', name: 'Usuarios con más ventas', exact: false, Icon: MdBorderAll },
];

const navItems = [
 // { to: '/', name: 'Inicio', exact: true, Icon: MdDashboard },
  { to: '/countries', name: 'Paises', exact: false, Icon: MdWeb },
  { to: '/users', name: 'Usuarios', exact: false, Icon: MdInsertChart },
  { to: '/categories', name: 'Categorías', exact: false, Icon: MdWidgets },
  /*{ to: '/privileges', name: 'Privilegios', exact: false, Icon: MdWidgets },
  { to: '/roles', name: 'Roles', exact: false, Icon: MdWidgets },*/
];

const navReports = [
  { to: '/earnings', name: 'Mis ganancias', exact: false, Icon: MdWeb },
];

const navLogout = [
  { to: '/logout', name: 'cerrar sesión', exact: true, Icon: MdDashboard },
];

const bem = bn.create('sidebar');

class SidebarAdmin extends React.Component {
  state = {
    isOpenReports: false,
    isOpenRankings:false,
    modal_validation: false

  };

  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  clickModalValidation = (e) =>{
    return this.setState({
        modal_validation: !this.state.modal_validation,
    }); 
}

  render() {
    const {resetUserData, user} = this.props
    return (
      <aside id="aside-custom" className={bem.b()} data-image={sidebarBgImage}>
        <div className={bem.e('background')} style={sidebarBackground} />
        <div className={bem.e('content')}>
          <Navbar className="justify-content-center">
            <label style={{marginBottom: "0"}}>
              <img src={(user && user.user_profile && user.user_profile.image)? process.env.REACT_APP_VERONICA_URL_WEB + 'uploads/users/' + user.user_profile.image : process.env.REACT_APP_VERONICA_URL_WEB + 'assets/user_default.png' + user.user_profile.image} className="rounded-circle mb-2 sidebar-img" />
            </label>
            <div className="card-title" style={{fontSize: "larger", marginBottom:0}}>{user.user_profile.name} {user.user_profile.lastname}</div>
          </Navbar>
          <Nav vertical className="sidebar-options-container">
            {navItems.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  className="text-uppercase"
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Rankings')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdSend className={bem.e('nav-item-icon')} />
                  <span className="">Rankings</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenRankings
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>
            <Collapse isOpen={this.state.isOpenRankings}>
              {navRankings.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            <NavItem className={bem.e('nav-item')}>
              <div
                className="text-uppercase"
                onClick={()=> {this.clickModalValidation()}}
                style={{padding: '0.5rem 1rem', fontWeight:400}}
              >
                <MdDashboard className={bem.e('nav-item-icon')} />
                <span className="">{'cerrar sesión'}</span>
              </div>
            </NavItem>

            <Modal
                isOpen={this.state.modal_validation}>
                    <ModalHeader>Cerrar Sesión</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label for="email">¿Está seguro que desea cerrar sesión?</Label>{' '}
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Link to="/">
                        <Button color="primary" onClick={()=> resetUserData()}>
                            Aceptar
                        </Button>
                    </Link>
                        <Button color="danger" onClick={()=> {this.clickModalValidation()}}>
                            Cancelar
                        </Button>                      
                </ModalFooter>
            </Modal>
          </Nav>
        </div>
      </aside>
    );
  }
}

const mapStateToProps = state => {
  return {
    globalData: state.main.globalData,
    user: state.main.userData,

  }
}


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    resetUserData
  }, dispatch)
);

export default connect(mapStateToProps,mapDispatchToProps)(SidebarAdmin)
