import logo200Image from 'assets/img/logo/logo_200.png';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, Form, FormGroup, Input, Label,Modal, ModalBody, ModalFooter,ModalHeader} from 'reactstrap';
import { bindActionCreators } from 'redux';
import {Redirect} from 'react-router-dom';

class AuthForm extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      user: {
        email: "",
        password: "",
        confirmPassword: "",
        remember: false
      },
      modal: false,
    }
  }


  clickModal = (e) =>{
    return this.setState({
        modal: !this.state.modal,
    }); 
  }

  userLoggedRedirect = data =>{
    if(data.userLogged){
      if(data.roleUser==1){
        this.props.history.push("/countries")
      }
      else
        this.props.history.push("/home")
    }
  };

  renderButtonText(authState) {
    const { buttonText } = this.props;

    if (!buttonText && authState=="STATE_LOGIN") {
      return 'Iniciar sesión';
    }

    if (!buttonText && authState=="STATE_SIGNUP") {
      return 'Registrarse';
    }

    if (!buttonText && authState=="RECOVER_PASSWORD") {
      return 'Obtener contraseña';
    }

    return buttonText;
  }

  validateFields = (authState) =>{
    const {user} = this.props
    let empty_field = true
    if(authState=="STATE_LOGIN"){
      if(user.email.length!=0 && user.password.length!=0)
       empty_field = false
    }
    else if(authState=="STATE_SIGNUP"){
      if(user.email.length!=0 && user.password.length!=0 && user.passwordConfirm!=0 && user.country_id!="")
        empty_field = false
    }
    else if(authState=="RECOVER_PASSWORD"){
      if(user.email.length!=0){
        empty_field = false
      }
    }
    return empty_field
  }

  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      confirmPasswordLabel,
      confirmPasswordInputProps,
      onLogoClick,

      authState,
      user,
      fetch,
      countries,
      globalData,
      onHandleAuthStateChange,
      onHandleUserInputChange,
      fetchCreateUser,
      onFetchAuthUser,
      onHandleUserProfileInputChange,
      onResetFetch,
      onResetUserData,
    } = this.props;
    return (
      <div style={{width:"100%",height:"100%",display: 'flex',justifyContent: 'center',alignItems: 'center'}}>
        <div style={{padding:10,width:340}}>
          <Form>
            {(globalData.isLogged) && ( 
              (globalData.roleUser=="1") ?
              <Redirect to="/countries"/>
              :
              <Redirect to="/home"/>
            )
            }
            {showLogo && (
              <div className="text-center pb-4">
                <img
                  src={logo200Image}
                  className="rounded"
                  style={{ width: 60, height: 60, cursor: 'pointer' }}
                  alt="logo"
                  onClick={onLogoClick}
                />
              </div>
            )}
            {authState=="STATE_SIGNUP" || authState=="STATE_LOGIN" ? (
              <div>
                <FormGroup>
                  <Label for={usernameLabel}>{usernameLabel}</Label>
                  <Input 
                    {...usernameInputProps} 
                    name="email"
                    value={((user.email) ? user.email : "")} 
                    onChange={(e) => onHandleUserInputChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for={passwordLabel}>{passwordLabel}</Label>
                  <Input 
                    {...passwordInputProps} 
                    name="password"
                    value={((user.password) ? user.password : "")} 
                    onChange={(e) => onHandleUserInputChange(e)}
                  />
                </FormGroup>
              
                {authState=="STATE_SIGNUP" ? (
                  <div>
                    <FormGroup>
                      <Label for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
                      <Input 
                        {...confirmPasswordInputProps} 
                        name="passwordConfirm"
                        value={((user.passwordConfirm) ? user.passwordConfirm : "")} 
                        onChange={(e) => onHandleUserInputChange(e)}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label>País</Label>
                      <Input type="select" name="country_id" value={user.country_id} onChange={(e) => onHandleUserInputChange(e)}>
                                <option value="">Seleccione una país</option>
                                {countries.map((country,index)=>(
                                  <option key={index} value={country.id}>{country.name}</option>
                                ))}
                      </Input>
                    </FormGroup>
                  </div>
                ) : (
                  <div className="pt-1">
                    <h6>
                      <a onClick={()=> {onHandleAuthStateChange("RECOVER_PASSWORD");onResetUserData()}}>
                        ¿Ha olvidado su contraseña?
                      </a>
                    </h6>
                  </div>
                )}
                
              </div>
              
            ) : (
              <div>
                <h6>Se le enviará un mensaje a su correo con una contraseña provisional de acceso, por favor ingrese su correo</h6>
                <FormGroup>
                  <Label for={usernameLabel}>{usernameLabel}</Label>
                  <Input {...usernameInputProps}
                  name="email"
                  defaultValue={((user.email) ? user.email : "")} 
                  onChange={(e) => onHandleUserInputChange(e)} />
                </FormGroup>
              </div>
            )}
            <hr />
            {authState=="STATE_SIGNUP"}
            {this.validateFields(authState)?
            <Button
              size="lg"
              className="bg-gradient-theme-left border-0"
              block
              onClick={() => this.clickModal()}>
              {this.renderButtonText(authState)}
            </Button>
            :
            <Button
              size="lg"
              className="bg-gradient-theme-left border-0"
              block
              onClick={((authState=="STATE_LOGIN") ? 
                () => {onFetchAuthUser(user)}
                : ((authState=="STATE_SIGNUP") ?
                  () => {fetchCreateUser(user);onHandleAuthStateChange("STATE_LOGIN");onResetUserData()} 
                  : () => {console.log("esto es recover password")}))}>
              {this.renderButtonText(authState)}
            </Button>
            }

              <div className="text-center pt-1">
                <h6>o</h6>
                <h6>
                  {(authState=="STATE_SIGNUP" || authState=="RECOVER_PASSWORD") ? (
                    <a onClick={()=> {onHandleAuthStateChange("STATE_LOGIN");onResetUserData()}}>
                      Iniciar sesión
                    </a>
                  ) : (
                    <a onClick={()=> {onHandleAuthStateChange("STATE_SIGNUP");onResetUserData()}}>
                      Registrate
                    </a>
                  )}
                </h6>
              </div>
              <Modal
                  isOpen={this.state.modal}>
                  <ModalHeader>Debe llenar los campos solicitados</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {this.clickModal()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
              <Modal
                  isOpen={fetch.result_succeeded == false}>
                  <ModalHeader>Fallo de autenticación</ModalHeader>
                  <ModalBody>
                      <FormGroup>
                          <Label for="email">{fetch.message}</Label>{' '}
                      </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="primary" onClick={()=> {onResetFetch()}}>
                          Aceptar
                      </Button>
                      
                  </ModalFooter>
              </Modal>
          </Form>
        </div>
      </div>
    );
  }
}

export const STATE_LOGIN = 'STATE_LOGIN';
export const STATE_SIGNUP = 'STATE_SIGNUP';
export const RECOVER_PASSWORD = 'RECOVER_PASSWORD';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP,RECOVER_PASSWORD]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'STATE_LOGIN',
  showLogo: true,
  usernameLabel: 'Correo',
  usernameInputProps: {
    type: 'email',
    placeholder: 'your@email.com',
  },
  passwordLabel: 'Contraseña',
  passwordInputProps: {
    type: 'password',
    placeholder: 'contraseña',
  },
  confirmPasswordLabel: 'Confirmar contraseña',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirmación de contraseña',
  },
  onLogoClick: () => {},
};

export default AuthForm
