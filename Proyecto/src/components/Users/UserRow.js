import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class UserRow extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {
			user, 
			onShowUser, 
			onEditUser, 
			statusAsString,
			rolIdAsString,
			onChangeView,
			onDeleteUser
		} = this.props

		return(
			<>
				<tr key={user.id}>
					<td> {user.id} </td>		
					<td> {user.email} </td>
					<td> { (user && user.role_id)? rolIdAsString(user.role_id) : "No posee"} </td>
					<td> {statusAsString(user.status)} </td>
					<td> 
						<Button color="info" onClick={() => {onShowUser(user); onChangeView("show")}}> 
							Ver
						</Button> 
					</td>
					<td> 
						<Button color="primary" onClick={() => {onEditUser(user); onChangeView("edit")}}> 
							Modificar
						</Button> 
					</td>
					<td> 
						<Button color="secondary" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button> 
					</td>
				</tr>
				<ModalConfirmDeleteItem item={user} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteUser} onChangeView={onChangeView}/>
			</>
		);
	}
}

export default UserRow;