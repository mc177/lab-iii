import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';


class UserEditable extends React.Component {

	constructor(props){
		super(props)
	}
	rolIdAsString(rol_id, roles){
		let rol_string = ""
		roles.map((rol)=>{
			if(rol.id ==rol_id){
				rol_string=rol.name
			}	
		}) 
		
		return rol_string
	}	

	render(){
		const {
					user,
					roles,
					countries, 
					onCancelEditClick, 
					onUpdateUserClick, 
					onUpdateUser,
					onHandleUserRoleChange,
					onHandleUserInputChange,
					onHandleUserCountryChange,
				} = this.props
		
console.log(user)
		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12}>
	          <Card>
	            <CardHeader>Editar información de usuario</CardHeader>
	            <CardBody>
	              <Form>
	                <FormGroup row>
	                  <Label for="email" sm={2}>
	                    Email
	                  </Label>
	                  <Col sm={10}>
	                    <Input
	                      type="email"
	                      name="email"
	                      defaultValue={user.email}
	                      onChange={(e) => onHandleUserInputChange(e)}
	                      placeholder="Ingrese el email"
	                    />
	                  </Col>
	                </FormGroup>
	                <FormGroup row>
	                  <Label for="exampleSelect" sm={2}>
	                    Rol
	                  </Label>
	                  <Col sm={10}>
	                    <Input type="select" name="id" value={(user && user.role.id)? user.role.id : -1 } onChange={(e) => onHandleUserRoleChange(e)}>
									{
										(roles && roles.length > 0)? 
										roles.map((rol) => (
											<option value={rol.id}> {rol.name} </option>
										)):
										<option value={-1}> No existen roles </option>
									}
	                  	</Input>
	                  </Col>
	                </FormGroup>
					<FormGroup row>
	                  <Label for="exampleSelect" sm={2}>
	                    Rol
	                  </Label>
	                  <Col sm={10}>
	                    <Input type="select" name="id" value={(user && user.user_profile.country.id)? user.user_profile.country.id: -1 } onChange={(e) => onHandleUserCountryChange(e)}>
									{
										(countries && countries.length > 0)? 
										countries.map((country) => (
											<option value={country.id}> {country.name} </option>
										)):
										<option value={-1}> No existen Paises </option>
									}
	                  	</Input>
	                  </Col>
	                </FormGroup>
					
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button style={{float: "right"}} onClick={()=> {onCancelEditClick();}}>Cancelar</Button>
	                    <Button color="success" style={{marginRight: "20px",float: "right"}} onClick={() => {onUpdateUserClick(user); onUpdateUser(user)}}>Guardar</Button>
	                  </Col>
	                </FormGroup>
	              </Form>
	            </CardBody>
	          </Card>
	        </Col>
			</div>
		);
	}
}

export default UserEditable;