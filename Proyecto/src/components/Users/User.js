import React from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'


class User extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}


	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {user, onEditUser, onDeleteUser, rolIdAsString, onGoBackToList,onChangeView} = this.props

		return(
			<div className="custom-container-without-space">
	        <Col xl={12} lg={12} md={12}>
	          <Card>
	            <CardHeader>Información Usuario</CardHeader>
	            <CardBody>
	              <Form>
	                <FormGroup row>
	                  <Label for="email" sm={5}>
	                    Email: {user.email}
	                  </Label>
	                </FormGroup>
	                <FormGroup row>
	                  <Label for="exampleSelect" sm={3}>
	                    Rol: {user.role.name}
	                  </Label>
	                </FormGroup>
	                <FormGroup check row>
	                  <Col sm={{ size: 10, offset: 2 }}>
	                    <Button color="info" style={{float: "right"}} onClick={()=> onGoBackToList()}>Volver</Button>
	                    <Button style={{float: "right", marginRight: 10, marginLeft: -10}} onClick={()=> this.openHandleModal()}>Eliminar</Button>
	                    <Button color="success" style={{float: "right", marginRight: 20, marginLeft: -20}} onClick={() => {onEditUser(user);onChangeView("edit")}}>Modificar</Button>
	                  </Col>
	                </FormGroup>
	              </Form>
	            </CardBody>
	          </Card>
	        </Col>
	        <ModalConfirmDeleteItem item={user} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteUser} onChangeView={onChangeView}/>
			</div>
		);
	}
}

export default User;