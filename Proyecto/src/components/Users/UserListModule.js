import React from 'react'
import UserList from './UserList'
import UserView from './UserView'

class UserListModule extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			view: "list",
			roles: [
				{
					id: 1,
					name: "Administrador",
					description: "Usuario encargado del manejo y control de la apliación y de la data", 
					status: 1
				},
				{
					id: 2,
					name: "Usuario",
					description: "Usuario Venderdor o comprador de la aplicación",
					status: 1
				},
				{
					id: 3,
					name: "Otro",
					description: "Otro rol aun no definido", 
					status: 2
				}
			],
			countries: [
				{
					id: 1,
					name: "Venezuela", 
					status: 1
				},
				{
					id: 2,
					name: "Colombia", 
					status: 2
				},
				{
					id: 3,
					name: "Argentina", 
					status: 1
				}
			],
			user: {},
			users: [
				{
					id: 1,
					username: "JeferJAC96",
					email: "jejoalca14@gmail.com",
					password: 12345,
					confirmPassword: 12345,
					status: 1,
					role_id: 1,
					user_profile: {
						id: 1,
						name: "Jeferson",
						lastname: "Alvarado",
						birthdate: "1996-07-30",
						sex: "M",
						phone: "04245558208",
						country: {
							id: 1,
							name: "Venezuela", 
							status: 1
						},
						facebook: "Jeferson96",
						twitter: "@Jefer96",
						instagram: "JefersonJAC96"
					}
				},
				{
					id: 2,
					username: "Josue",
					email: "josue@gmail.com",
					password: 12345,
					confirmPassword: 12345,
					status: 1,
					role_id: 2,
					user_profile: {
						id: 1,
						name: "Josue",
						lastname: "Alvarado",
						birthdate: "1996-07-30",
						sex: "M",
						phone: "04245558208",
						country: {
							id: 1,
							name: "Venezuela", 
							status: 1
						},
						facebook: "josue96",
						twitter: "@josue96",
						instagram: "josue96"
					}
				},
				{
					id: 3,
					username: "luis",
					email: "luis@gmail.com",
					password: 12345,
					confirmPassword: 12345,
					status: 1,
					role_id: 3,
					user_profile: {
						id: 1,
						name: "Luis",
						lastname: "Alvarado",
						birthdate: "1996-07-30",
						sex: "M",
						phone: "04245558208",
						country: {
							id: 1,
							name: "Venezuela", 
							status: 1
						},
						facebook: "luis",
						twitter: "@luis",
						instagram: "JefersonJAC96"
					}
				}
			]
		}

		this.statusAsString=this.statusAsString.bind(this)
		this.onViewUser=this.onViewUser.bind(this)
		this.onEditClick=this.onEditClick.bind(this)
		this.onCancelEditClick=this.onCancelEditClick.bind(this)
		this.onUpdateUserClick=this.onUpdateUserClick.bind(this)
		this.onAddNewUserClick=this.onAddNewUserClick.bind(this)
		this.onSaveClick=this.onSaveClick.bind(this)
		this.onCancelAddNewUserClick=this.onCancelAddNewUserClick.bind(this)
		this.onHandleInputChange=this.onHandleInputChange.bind(this)
		this.rolIdAsString=this.rolIdAsString.bind(this)
		this.onGoBackToList=this.onGoBackToList.bind(this)
		this.onChangeView = this.onChangeView.bind(this)
	}

	onHandleInputChange(event){
		let user = Object.assign({}, this.state.user, {
			[event.target.name]: event.target.value
		})

		this.setState({user})
	}

	onGoBackToList(){
		this.setState({view:"list"})
	}

	rolIdAsString(rol_id){
		let rol_string = ""
		switch(parseInt(rol_id,10)){
			case 1: 
				rol_string = "Administrador"; break;
			case 2: 
				rol_string = "Usuario"; break;
			default:
				rol_string = "Otro"; break;
		}
		return rol_string
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	onViewUser(reason){
		switch(reason.toString()){
			case "show":
				this.setState({view: "show"}); break;
			case "edit":
				this.setState({view: "edit"}); break; 
			case "new":
				this.setState({view: "new"}); break;
		}
	}

	onEditClick(id){
		this.setState({view: "edit"})
	}

	onCancelEditClick(id){
		this.setState({view: "list"})
	}

	onChangeView(type){
		//type is"edit","show" or "list"
		this.setState({view: type})
	}

	onUpdateUserClick(user){
		this.setState({view: "show"})

	}

	onAddNewUserClick(){
		this.setState({view: "new"})		
	}

	onSaveClick(user){
		this.setState({view: "list"})
	}

	onCancelAddNewUserClick(){
		this.setState({user:{}, view: "list"})
	}

	render(){
		const {view} = this.state
		const {
			users,
			countries,
			roles,
			user,
			onShowUser,
			onEditUser,
			onRestoreUser,
			onDeleteUser,
			onHandleUserInputChange,
			onUpdateUser,
			onHandleUserRoleChange,
			onHandleUserCountryChange,
			onSaveNewUser
		} = this.props
		return(
			<div> 
			{
				(view && view == "list")?
					<UserList
						countries={countries}
						roles={roles}
						users={users}
						rolIdAsString={this.rolIdAsString}
						statusAsString={this.statusAsString}
						onShowUser={onShowUser} 
						onEditUser={onEditUser}
						onOpenDeleteUserModal={this.onOpenDelete} 
						onAddNewUserClick={this.onAddNewUserClick}
						onChangeView={this.onChangeView}
						onRestoreUser={onRestoreUser}
						onDeleteUser = {onDeleteUser} 
					/>
				:
					<UserView 
						view={view}
						countries={countries}
						roles={roles}
						onHandleUserRoleChange={onHandleUserRoleChange}
						onHandleUserCountryChange={onHandleUserCountryChange} 
						user={user} 
						onGoBackToList={this.onGoBackToList}
						rolIdAsString={this.rolIdAsString}
						onHandleUserInputChange={onHandleUserInputChange}
						onEditUser={onEditUser}
						onCancelEditClick={this.onCancelEditClick}
						onSaveClick={this.onSaveClick}
						onOpenDeleteUserModal={this.onOpenDelete}
						onDeleteUser={onDeleteUser}
						onCancelAddNewUserClick={this.onCancelAddNewUserClick}
						onUpdateUserClick={this.onUpdateUserClick}
						onChangeView={this.onChangeView}
						onUpdateUser={onUpdateUser}
						onSaveNewUser={onSaveNewUser}
					/>
			}
			</div>
		);
	}
}

export default UserListModule;