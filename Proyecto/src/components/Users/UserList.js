import React from 'react'
import { Table } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';

import UserRow from './UserRow'

class UserList extends React.Component {

	constructor(props){
		super(props)
	}

	render(){
		const {
			users,
			user,
			countries, 
			onShowUser, 
			onEditUser, 
			onOpenDeleteUserModal, 
			onAddNewUserClick, 
			statusAsString,
			rolIdAsString,
			onChangeView,
			onRestoreUser,
			onDeleteUser
		} = this.props
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Usuarios 
					</CardHeader> 
					<CardBody>
						<Table responsive>
					        <thead>
					          <tr>
					            <th>ID</th>
					            <th>Email</th>
					            <th>Rol</th>
					            <th>Estado</th>
					            <th>Ver</th>
					            <th>Modificar</th>
					            <th>Eliminar</th>
					          </tr>
					        </thead>
					        <tbody>
					        {
					        	(users && users.length > 0)? 
					        		users.map((user) => (
										<UserRow
											countries={countries} 
					        				key={user.id} 
					        				user={user} 
					        				onShowUser={onShowUser} 
					        				onEditUser={onEditUser} 
					        				statusAsString={statusAsString} 
					        				onOpenDeleteUserModal={onOpenDeleteUserModal} 
											rolIdAsString={rolIdAsString}
											onChangeView={onChangeView}
											onDeleteUser={onDeleteUser}  
					        			/>
					        		))
					        	:
					        	<tr> 
					        		<td colSpan={5}> No existen usuarios registrados </td>
					        	</tr>
					        }
					        <tr>
					        	<td colSpan={5}>
							        <Button color="primary" onClick={() => {onAddNewUserClick(); onRestoreUser()}} >
							        	Agregar Usuario
							        </Button>
						      	</td>
					      	</tr>
				        </tbody>
				      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default UserList;