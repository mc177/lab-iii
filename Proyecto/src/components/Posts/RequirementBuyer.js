import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg18Image from 'assets/img/bg/background_1920-18.jpg';
import bg1Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import yellowStar from 'assets/img/star_checked.png'
import like from 'assets/img/like.png'
import border_like from 'assets/img/border-like.png'
import share from 'assets/img/share.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  Badge,
  Input,
  Label,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import Avatar from 'components/Avatar';

class RequirementBuyer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modal: false,
        };
	}

    clickModal = (e) =>{
        return this.setState({
            modal: !this.state.modal,
        }); 
    }

    actionExecuted = (actions,string) =>{
		let action_executed = null
		actions.map((action)=>{
			if(action.name == string)
				action_executed = action
		})
		return action_executed
    }
    
    render(){
        const {
            fetch_post,
            post_to_purchase,
            post,
            onHandlePostToPurchaseInputChange,
            onCancelPostToPurchase,
            onResetFetchPost,
            onSendRequirements,
            actions
        } = this.props
        return (
            <Page className="custom-container">
                <Row>
                    <Col md={5}>
                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Condiciones del vendedor:</Label>
                                    <FormGroup style={{width:'100%'}}>
                                        <Input value={post.conditions} type="textarea" name="text" style={{height:100,color:"black"}} disable/>
                                    </FormGroup>
                                </Row>
                            </CardBody>
                        </Card>

                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Detalle sus requerimientos (obligatorio)</Label>
                                    <Input value={post_to_purchase.requirements} type="textarea" name="requirements" style={{height:100}} onChange={(e) => onHandlePostToPurchaseInputChange(e)}/>
                                </Row>
                                <Row className="m-rl-0" style={{height:35, marginTop:15}}>
                                    <Col className="mb-3" xs={6}>
                                        <Button onClick={()=>{onSendRequirements(post,post_to_purchase.requirements,this.actionExecuted(actions,"Comprar"));this.clickModal()}} color="success">Solicitar</Button>
                                    </Col>
                                    <Col className="mb-3" xs={6}>
                                        <Link to="/detail-publication">
                                            <Button onClick={()=>{onCancelPostToPurchase()}} color="danger">Cancelar</Button>
                                        </Link>
                                        
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row> 
                <Modal
                    isOpen={this.state.modal}>
                    {fetch_post.result_succeeded ?
                        <ModalHeader>Requerimientos enviados con éxito!</ModalHeader>
                    :
                        <ModalHeader>No se pudieron enviar los requerimientos!</ModalHeader>
                    }
                    {fetch_post.result_succeeded ?
                        <ModalBody>
                            <FormGroup>
                                <Label for="email">Sus requerimientos han sido enviados al vendedor, espere su respuesta!</Label>{' '}
                            </FormGroup>
                        </ModalBody>
                    :
                        <ModalBody>
                            <FormGroup>
                                <Label for="email">No se ha podido realizar su solicitud de compra</Label>{' '}
                            </FormGroup>
                        </ModalBody>
                    }
                    <ModalFooter>
                        <Link to="/detail-publication">
                        {fetch_post.result_succeeded ?
                            <Button color="primary" onClick={()=> {this.clickModal();onResetFetchPost()}}>
                                OK
                            </Button>
                            :
                            <Button color="primary" onClick={()=> this.clickModal()}>
                                OK
                            </Button>
                        }
                        </Link>
                    </ModalFooter>
                </Modal> 
            </Page>
        );
    }
}

export default RequirementBuyer;
