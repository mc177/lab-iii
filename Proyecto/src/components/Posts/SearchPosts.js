import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import img_publication1 from 'assets/img/custom-img/Emerald-Dream.png';
import img_publication2 from 'assets/img/custom-img/Queen.png';
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardHeader,
  Badge,
  CardText,
  CardTitle,
  Col,
  Row,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  Progress,
  Input,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledButtonDropdown,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Label
} from 'reactstrap';

class SearchPosts extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
	}


/*	getIdCategoryByName(name){
		const {categories} = this.state

		let category_returned_id = null;

		categories.map((category) => {
			if(category.name.toString().toLowerCase() === name.toString().toLowerCase()){
				category_returned_id = category.id
			}
		})
		return category_returned_id
	}

	onSearchPostByCategory(name){
		const {posts} = this.state
		let id = this.getIdCategoryByName(name)

		let posts_filter = [];

		posts.map((post) => {
			if(post.category && parseInt(post.category.id, 10) === parseInt(id, 10)){
				posts_filter.push(post)
			}
		})

		let filter = Object.assign({}, this.state.filter, {
			category_name: name,
			isChange: false
		})

		this.setState({
			posts_filter,
			filter
		})
	}

	onKeySearchPress(event, name){
		var code = (event.keyCode ? event.keyCode : event.which);
		if(code == 13) { 
		    this.onSearchPostByCategory(name)
		}
	}

	followCategory(id){
		let categories = this.state.categories.map((category) => {
			if(parseInt(category.id, 10) === parseInt(id, 10)){
				return Object.assign({}, category, {
					is_followed: true
				})
			}
			return category
		})

		this.setState({categories})
	}

	disFollowCategory(id){
		let categories = this.state.categories.map((category) => {
			if(parseInt(category.id, 10) === parseInt(id, 10)){
				return Object.assign({}, category, {
					is_followed: false
				})
			}
			return category
		})

		this.setState({categories})
	}

	thisCategoryIsBeingFollowed(id){
		let result = false

		this.state.categories.map((category) => {
			if(parseInt(category.id, 10) === parseInt(id, 10) && category.is_followed){
				result = true	
			}
		})

		return result;
	}
*/

	isFollowed = (followed_categories,category_id)=> {
		let is_followed = false
		if(followed_categories && followed_categories.length>0)
			followed_categories.map((followed_category)=>{
			if(followed_category.id==category_id){
				is_followed = true
			}   
			})
        return is_followed
	}
	
	clickModal = (e) =>{
        return this.setState({
            modal: !this.state.modal,
        }); 
    }

    isLiked = (post,my_liked_posts)=> {
		let is_liked = false
		if(my_liked_posts.length>0)
			my_liked_posts.map((liked_post)=>{
			if(liked_post.post.id==post.id){
				is_liked = true
			}
				
			})
        return is_liked
	}

	actionExecuted = (actions,string) =>{
		let action_executed = null
		actions.map((action)=>{
			if(action.name == string)
				action_executed = action
		})
		return action_executed
	}
	
	render(){
		const {filter, posts_filter, categories} = this.state
		const {categories_,actions, followed_categories, onFollowCategory, onUnFollowCategory,
			onSearchPostByCategory,filter_category,onShowPost,my_liked_posts,onLikePost,onRemoveLikePost
		} = this.props
		return(
			<div className="custom-container-without-space">
				<div className="div-search"> 				
					<UncontrolledButtonDropdown className="m-1" style={{width:"100%"}}>
						<DropdownToggle caret>Categorías</DropdownToggle>
						<DropdownMenu>
							{categories_.map((category, index) => (
								<DropdownItem 
									onClick={() => onSearchPostByCategory(category)} 
									key={index} value={category.id}
								>
									{category.name}
								</DropdownItem>
							))}
						</DropdownMenu>
					</UncontrolledButtonDropdown>
				</div>
				<div>
				{
					( filter_category && filter_category.posts && filter_category.posts.length > 0)?  
						<>
							<div style={{padding: 10, height: 42, background: "antiquewhite"}}>
								<label style={{float: "left", textTransform: "uppercase"}}>{filter_category.category.name} </label>
								{(!this.isFollowed(followed_categories,filter_category.category.id))?
										<Badge color="info" style={{cursor: "pointer", float: "right", marginRight: 12, height: 22, fontSize: "initial"}} onClick={() => onFollowCategory(filter_category.category)}> Seguir </Badge> 
									:
										<Badge color="success" style={{cursor: "pointer", float: "right", marginRight: 12, height: 22, fontSize: "initial"}} onClick={() => onUnFollowCategory(filter_category.category)}> Seguido </Badge> 
								}
							</div>
							<div className="result-search">
							{
							filter_category.posts.map((post,index) => (
							  <Row className="row-custom" key={index}>
					        	<Col md={8} sm={6} xs={12} className="mb-3">
								  <Card>
							        <div className="user-header">
							          	<li className="nav-item" style={{listStyle: "none"}}>
							          		<a id="Popover2" className="nav-link">
							          			<img src={(post.user && post.user.user_profile && post.user.user_profile.image)? process.env.REACT_APP_VERONICA_URL_WEB + 'uploads/users/' + post.user.user_profile.image : process.env.REACT_APP_VERONICA_URL_WEB + 'assets/user_default.png' + post.user.user_profile.image} className="rounded-circle can-click flipthis-highlight user-image-cardsells"/>
							          			<label style={{marginLeft: "2%", color: "white", marginTop:"2%"}}>
												  {(post.user.user_profile.name!="" && post.user.user_profile.lastname!="") ? <>{post.user.user_profile.name} {post.user.user_profile.lastname}</> : post.user.email}
							          			</label>
							          		</a>
							          	</li>
							        </div>
						            <CardImg top src={"http://localhost:3000/uploads/posts/"+post.pictures[0]} />
						            <CardBody style={{marginBottom: "-30px"}}>
						              <CardHeader style={{padding: 0, textTransform: "none"}}>
						              		<div className="chip-status">
								              	<Badge id="categoryIlustration" title="Categoría" color="info"  style={{cursor: "pointer"}} className="mr-1">
										          {post.category.name}
										        </Badge>
								              	<UncontrolledPopover trigger="legacy" placement="bottom" target="categoryIlustration">
							                        <PopoverHeader style={{background: "rgb(0, 201, 255)", color: "white"}}>Categoría Ilustración</PopoverHeader>
										          	<PopoverBody>
												        {post.category.description}
										            </PopoverBody>
									             </UncontrolledPopover>
							             </div>
										<CardText>
											<b>{post.title}</b>
										</CardText>
										 <CardText>
											<small>{post.description}</small>
										</CardText>
						              </CardHeader>
						              <br/>
						            </CardBody>
						            <div style={{padding: "2%", marginBottom: "5px"}}>
										{this.isLiked(post,my_liked_posts) ?
											<FontAwesomeIcon onClick={()=>onRemoveLikePost(post,this.actionExecuted(actions,"Me gusta"))} title="Me gusta"  className="heart-icon" icon={faHeart}/>
										:
											<FontAwesomeIcon onClick={()=>onLikePost(post,this.actionExecuted(actions,"Me gusta"))} title="Me gusta"  className="heart-icon" icon={farHeart}/>
										}
										<FontAwesomeIcon title="Compartir" className="share-icon" icon={faShareAlt} style={{color:"black"}} onClick={this.clickModal}/>
						            	<Row className="m-rl-0" style={{height:35,marginTop:15,marginBottom: 13}}>
											<Link to="/detail-publication">
												<Button onClick={()=>{onShowPost(post)}} title="Ver detalles" className="btn btn-success button-succes-custom btn-show"><FontAwesomeIcon icon={faEye}/></Button>
											</Link>
										</Row>
						            </div>
						          </Card>
						        </Col>
						      </Row>
								))
							}
							</div>
						</>
					:
						<div style={{padding: 10, height: 61, background: "antiquewhite"}}>
							<label>No se encontraron coincidencias para la categoría </label>
						</div>	
				}
				<Modal
                    isOpen={this.state.modal}>
                    <ModalHeader>Compartir publicación</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="email">Correo</Label>{' '}
                            <Input
                                type="email"
                                name="email"
                                placeholder="ejemplo@gmail.com"
                            />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" >
                        Enviar
                        </Button>{' '}
                        <Button
                        color="secondary" onClick={()=> this.clickModal()}>
                        Cancelar
                        </Button>
                    </ModalFooter>
                </Modal>  
				</div>
			</div>
		);
	}

}

export default SearchPosts;