import React from 'react'
import {
	Card, 
	CardHeader, 
	CardBody, 
	CardTitle, 
	Row, 
	Col, 
	Label, 
	Form, 
	FormGroup, 
	FormText, 
	Input, 
	Button,
	InputGroup,
	InputGroupAddon,
	Modal, ModalBody, ModalFooter,ModalHeader 
} from 'reactstrap' 
import { Link } from 'react-router-dom'
 
class NewPost extends React.Component {
 
	constructor(props){ 
		super(props)
		this.state = {
			modal: false,
			savePostModal: false
		}
	}

	clickModal = (e) =>{
		return this.setState({
				modal: !this.state.modal,
		}); 
	}
	
	clickSavePostModal = (e) =>{
		return this.setState({
				savePostModal: !this.state.savePostModal,
		}); 
	}

	restorePostValues(){
		let post = {
			title:"",
			category_id: "",
			price: {
				coin: 0, 
				value: ""
			},
			conditions:"",
			description: "",
			pictures: [],
		}
		this.setState({post})
	}
 
	render(){

		const{categories,onPostInputChange,onSaveNewPost, onDeletePostPicture, onCancelPostPost, restorePostValues, post} = this.props
		console.log(post)
		return(	
			<Card className="new-post-card custom-container">
				<CardBody> 
					<Row>
						<Col xl={12} lg={12} md={12}>
									<Card> 
										<CardHeader style={{textTransform: "none"}}>Publica tu producto</CardHeader>
										<CardBody>
											<Form>
											<FormGroup row>
													<Label for="exampleText" sm={2}>
														Título
													</Label>
													<Col sm={10}>
														<Input 
															name="title"
															onChange={onPostInputChange}
															value={post.title} 
														/>
													</Col>
												</FormGroup>
												<FormGroup row>
													<Label for="category" sm={2}>
														Categoría
													</Label>
													<Col sm={10}>
														<Input type="select" name="category_id" value={(post && post.category_id)?  post.category_id : "-1"} onChange={onPostInputChange}>
															<option value="-1">Seleccione una categoría</option>
															{categories.map((category,index)=>(
																<option key={index} value={category.id}>{category.name}</option>
															))}
															
														</Input>
													</Col>
												</FormGroup>
												<FormGroup row>
													<Label for="price" sm={2}>
														Precio
													</Label>
													<Col sm={10}>
													<InputGroup>
													<Input 
														 type="number"
															defaultValue={post.price}
															name="price"
															min={0}
															placeholder="Ingrese el precio" 
															onChange={onPostInputChange}
														 />
												</InputGroup>
													</Col>
												</FormGroup>
												<Row className="m-0" >
														<Label for="exampleText">Condiciones:</Label>
														<FormGroup style={{width:'100%'}}>
																<Row className="m-rl-0" style={{marginTop:10}}>
																		<Input 
																			type="textarea" 
																			name="conditions"
																			onChange={onPostInputChange}
																			value={post.conditions}
																		/>
																</Row>
														</FormGroup>
												</Row>
												<FormGroup row>
													<Label for="exampleText" sm={2}>
														Descripción
													</Label>
													<Col sm={10}>
														<Input 
															type="textarea" 
															name="description"
															onChange={onPostInputChange} 
															value={post.description}
														/>
													</Col>
												</FormGroup>
												<FormGroup row>
													<Label for="pictures" sm={2}>
														Seleccion la imagen promocional
													</Label>
													<Col sm={10}>
														<Input type="file" name="pictures" style={{color: "transparent"}} onChange={onPostInputChange}  />
														{
															(post && post.pictures && post.pictures.length > 0)?
															<label>
																{"Se han seleccionado " + post.pictures.length + " imagenes"}
															</label>
															:
															""
														}
													</Col>
												</FormGroup>
												<FormGroup row>
													<div style={{width: "100%", marginLeft: 19, background: "#dfdfdf", padding: 7, marginRight: 15, borderRadius: 4}}>
														{
															(post && post.prev_pictures && post.prev_pictures.length > 0)?
																post.prev_pictures.map((image, index) => (
																	<span key={index}>	
																		<label className="x-close" onClick={() => onDeletePostPicture(index)}> x </label>
																		<img style={{maxWidth: "23%", borderRadius: 6, marginRight: 4}} src={image}/>
																	</span>
																))
															:
															<Label for="no_pictures">
																No se han seleccionado imagenes
															</Label>
														}
													</div>
												</FormGroup>
												<FormGroup check row>
													<Col sm={{ size: 10, offset: 2 }}>
														<div>
													<Button color="secondary" style={{float: "right"}} onClick={()=> onCancelPostPost()}>Cancelar</Button>
												</div>
												<div>
													{(post.title && post.price && post.description && post.conditions && post.category_id!=-1 && post.pictures && post.pictures.length>0) ?
														<Button className="btn btn-success button-succes-custom" onClick={()=>{this.clickSavePostModal();}} style={{float: "right", marginRight: "21px"}}>Publicar</Button>
													:
														<Button className="btn btn-success button-succes-custom" onClick={()=>{this.clickModal()}} style={{float: "right", marginRight: "21px"}}>Publicar</Button>
													}
													
												</div>
													</Col>
												</FormGroup>
											</Form>
										</CardBody>
										<Modal
											isOpen={this.state.modal}>
											<ModalHeader>Debe llenar los campos solicitados</ModalHeader>
											<ModalBody>
													<FormGroup>
															<Label for="email">Uno o más campos no han sido rellenados</Label>{' '}
													</FormGroup>
											</ModalBody>
											<ModalFooter>
													<Button color="primary" onClick={()=> {this.clickModal()}}>
															Aceptar
													</Button>
													
											</ModalFooter>
									</Modal>
									<Modal
											isOpen={this.state.savePostModal}>
											<ModalHeader>Está seguro que desea publicar este post?</ModalHeader>
											<ModalBody>
													<FormGroup>
															<Label for="email">Se publicará la información suministrada como nuevo post</Label>{' '}
													</FormGroup>
											</ModalBody>
											<ModalFooter>
													<Button color="primary" onClick={()=> {this.clickSavePostModal();onSaveNewPost(post);}}>
															Aceptar
													</Button>
													<Button color="danger" onClick={()=> {this.clickSavePostModal();}}>
															Cancelar
													</Button>
													
											</ModalFooter>
									</Modal>
									</Card>
								</Col>
							</Row>
				</CardBody>
			</Card>
			
		);
	}
}

export default NewPost; 