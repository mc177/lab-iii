import img_publication1 from 'assets/img/custom-img/Emerald-Dream.png';
import img_publication2 from 'assets/img/custom-img/Queen.png';
import user1Image from 'assets/img/users/100_1.jpg';
import yellowStar from 'assets/img/star_checked.png'
import like from 'assets/img/like.png'
import border_like from 'assets/img/border-like.png'
import share from 'assets/img/share.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  Col,
  ListGroup,
  Row,
  Badge,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Input,
  Label,
  Alert,
  CardHeader
} from 'reactstrap';
import Typography from 'components/Typography';
import Avatar from 'components/Avatar';

class DetailPost extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            modal: false,
        };
	}
    
    clickModal = (e) =>{
        return this.setState({
            modal: !this.state.modal,
        }); 
    }

    isLiked = (post,my_liked_posts)=> {
		let is_liked = false
		if(my_liked_posts.length>0)
			my_liked_posts.map((liked_post)=>{
			if(liked_post.post.id==post.id){
				is_liked = true
			}
				
			})
        return is_liked
	}

    actionExecuted = (actions,string) =>{
		let action_executed = null
		actions.map((action)=>{
			if(action.name == string)
				action_executed = action
		})
		return action_executed
    }
    
    render(){
        const {user,post,actions,my_liked_posts,onLikePost,onRemoveLikePost,onShowPublicUserProfile} = this.props
        return (
            <Page className="custom-container">
                <Row>
                    <Col md={5}>
                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <Link to='publicUserProfile' style={{width:'100%'}}>    
                                    <div className="user-header" style={{zIndex: 10,top: 0}} onClick={()=>onShowPublicUserProfile(post.user)}>
                                        <li className="nav-item" style={{listStyle: "none"}}>
                                            <div id="Popover2" className="nav-link">
                                                <img src={(post.user && post.user.user_profile && post.user.user_profile.image)? process.env.REACT_APP_VERONICA_URL_WEB + 'uploads/users/' + post.user.user_profile.image : process.env.REACT_APP_VERONICA_URL_WEB + 'assets/user_default.png' + post.user.user_profile.image} className="rounded-circle can-click flipthis-highlight user-image-cardsells"/>
                                                <label style={{marginLeft: "2%", color: "white", marginTop:"2%"}}>
                                                {(post.user.user_profile.name!="" && post.user.user_profile.lastname!="") ? <>{post.user.user_profile.name} {post.user.user_profile.lastname}</> : post.user.email}
                                                </label>
                                            </div>
                                        </li>
                                    </div>
                                </Link>
                                <Card className="img-standard-card">
                                <div className="position-relative">
                                    <CardImg style={{minHeight:250}} src={"http://localhost:3000/uploads/posts/"+post.pictures[0]} />
                                </div>
                                </Card>
                                <Row style={{width:'100%', marginTop:10}}>
                                    <Col className="col-1 " style={{paddingLeft:0,paddingRight:0, marginLeft:22, maxWidth: 100, marginBottom:2}}>
                                        <Row className="justify-content-center" style={{height:24,width:"100%",marginBottom:3, marginRight:0,marginLeft:0}}>
                                            {this.isLiked(post,my_liked_posts) ?
                                                <FontAwesomeIcon onClick={()=>onRemoveLikePost(post,this.actionExecuted(actions,"Me gusta"))} title="Me gusta" style={{marginLeft:0}}  className="heart-icon" icon={faHeart}/>
                                            :
                                                <FontAwesomeIcon onClick={()=>onLikePost(post,this.actionExecuted(actions,"Me gusta"))} title="Remover Me gusta" style={{marginLeft:0}} className="heart-icon" icon={farHeart}/>
                                            }
                                            </Row>
                                        <Row className="m-0 justify-content-center" style={{width:"100%"}}>
                                            <Badge color="secondary" pill className="mr-1">
                                                100
                                            </Badge>
                                        </Row>
                                    </Col>
                                    <FontAwesomeIcon title="Compartir" className="share-icon" icon={faShareAlt} style={{color:"black"}} onClick={this.clickModal}/>
                                    <Badge color="primary" pill className="mr-1 tool" style={{marginTop:5, marginBottom:9,height: 20}}>
                                        {post.category.name}
                                    </Badge>
                                </Row>
                                <Row className="m-0 row-standard-card-text" >
                                    <CardText>
                                        <b>{post.title}</b>
                                    </CardText>
                                </Row>
                                <Row className="m-0 row-standard-card-text" >
                                    <CardText>
                                        <small>{post.description}</small>
                                    </CardText>
                                </Row>
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText" style={{fontSize:15}}><b>Redes sociales:</b></Label>
                                </Row>
                                <Row className="m-0 row-standard-card-text" >
                                    <CardText>
                                        email: <small>{post.user.email}</small>
                                    </CardText>
                                </Row>
                                {post.user.user_profile.facebook &&
                                    <Row className="m-0 row-standard-card-text" >
                                        <CardText>
                                            facebook: <small>{post.user.user_profile.facebook}</small>
                                        </CardText>
                                    </Row>
                                }
                                {post.user.user_profile.instagram &&
                                    <Row className="m-0 row-standard-card-text" >
                                        <CardText>
                                            instagram: <small>{post.user.user_profile.instagram}</small>
                                        </CardText>
                                    </Row>
                                }
                                {post.user.user_profile.twitter &&
                                    <Row className="m-0 row-standard-card-text" >
                                        <CardText>
                                            twitter: <small>{post.user.user_profile.twitter}</small>
                                        </CardText>
                                    </Row>
                                }
                                <br/>
                                <Row className="m-0 row-standard-card-text" >
                                    <Label for="exampleText">Condiciones del vendedor:</Label>
                                    <FormGroup style={{width:'100%'}}>
                                        <Input value={post.conditions} type="textarea" name="text" style={{height:100,color:"black"}} disable/>
                                    </FormGroup>
                                </Row>
                                {user.id != post.user.id &&                       
                                    <Row className="m-0" style={{height:35,marginTop:15}}>
                                        <Link to="/requirement-buyer">
                                            <Button color="success" className="buy-button">Comprar</Button>
                                        </Link>
                                        
                                    </Row>
                                }
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Modal
                    isOpen={this.state.modal}>
                    <ModalHeader>Compartir publicación</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="email">Correo</Label>{' '}
                            <Input
                                type="email"
                                name="email"
                                placeholder="ejemplo@gmail.com"
                            />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=> this.clickModal()}>
                        Enviar
                        </Button>{' '}
                        <Button
                        color="secondary" onClick={()=> this.clickModal()}>
                        Cancelar
                        </Button>
                    </ModalFooter>
                </Modal>   
            </Page>
        );
    }
}

export default DetailPost;
