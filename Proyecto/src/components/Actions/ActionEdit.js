import React from 'react'
import {Button, Input} from 'reactstrap'

class ActionEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {action, onUpdateActionClick, onCancelEditActionClick, onHandleActionInputChange} = this.props

		return(
			<tr key={action.id}>
				<td> {action.id} </td>
				<td> 
					<Input 
						data-id={action.id}
						name="name"
						defaultValue={action.name}
						onChange={(e) => onHandleActionInputChange(e)}
					/>
				</td>		
				<td>  
          <Input data-id={action.id} defaultValue={action.status} onChange={(e) => onHandleActionInputChange(e)} type="select" name="status">
            <option value={-1}>Selecciona una opción</option>
            <option value={1}>Activo</option>
            <option value={2}>Inactivo</option>
          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdateActionClick(action)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditActionClick(action.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default ActionEdit;