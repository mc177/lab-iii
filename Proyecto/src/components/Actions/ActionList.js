import React from 'react'
import { Table } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';

import Action from './Action'
import ActionEdit from './ActionEdit'
import ActionNew from './ActionNew'

class ActionList extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			countries: [
				{
					id: 1,
					name: "Comprar", 
					status: 1
				},
				{
					id: 2,
					name: "Me gusta", 
					status: 2
				},
				{
					id: 3,
					name: "Seguir", 
					status: 1
				}
			]
		}

		this.onEditAction = this.onEditAction.bind(this)
		this.onCancelEditAction = this.onCancelEditAction.bind(this)
		this.onUpdateAction = this.onUpdateAction.bind(this)
		this.onActionInputChange = this.onActionInputChange.bind(this)
		this.onDeleteAction = this.onDeleteAction.bind(this)
		this.hasSomeActionEditing = this.hasSomeActionEditing.bind(this)
		this.onAddNewAction = this.onAddNewAction.bind(this)
		this.onCancelAddNewAction = this.onCancelAddNewAction.bind(this)
		this.onSaveNewAction = this.onSaveNewAction.bind(this)
	}

	hasSomeActionEditing(){
		let _is_some = false 
		this.state.countries.map((action) => {
			if(action.isEditing){
				_is_some=true
			}
		})
		return _is_some
	}

	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	onEditAction(id){
		let countries = this.state.countries;
		countries = countries.map((action) => {
			if(parseInt(action.id, 10) == parseInt(id, 10)){
				return Object.assign({}, action,{
					original: action,
					isEditing: true
				})
			}
			return action
		})

		this.setState({countries})
	}

	onCancelEditAction(id){
		let countries = this.state.countries.map((action) => {
			if(parseInt(action.id, 10) == parseInt(id, 10)){
				return Object.assign({}, action.original,{
					isEditing: false
				})
			}
			return action
		})

		this.setState({countries})
	}

	onUpdateAction(action){
		let countries = this.state.countries.map((action_map) => {
			if(parseInt(action_map.id, 10) == parseInt(action.id, 10)){
				return Object.assign({}, action_map,{
					isEditing: false
				})
			}
			return action_map	
		})

		this.setState({countries})
	}

	onActionInputChange(event){
		let countries = this.state.countries.map((action) => {
			if(parseInt(action.id, 10) == parseInt(event.target.dataset.id, 10)){
				return Object.assign({}, action,{
					[event.target.name]: event.target.value
				})
			}
			return action
		})

		this.setState({countries})
	}

	onDeleteAction(id){
		let countries = this.state.countries.filter((action) => action.id != id)
		this.setState({countries})
	}

	onAddNewAction(){
		let newAction = {
			id: this.state.countries.length + 1,
			name: "",
			status: 1, 
			isNew: true
		}

		let countries = this.state.countries
		countries.push(newAction)
		this.setState({countries})
	}

	onCancelAddNewAction(){
		let countries = this.state.countries
		countries.pop()
		this.setState({countries})
	}

	onSaveNewAction(id){
		let countries = this.state.countries.map((action_map) => {
			if(parseInt(action_map.id, 10) == parseInt(id, 10)){
				return Object.assign({}, action_map,{
					isNew: false
				})
			}
			return action_map	
		})

		this.setState({countries})
	}

	render(){
		const {countries} = this.state
		console.log(countries)
		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Acciones 
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>ID</th>
			            <th>Nombre</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomeActionEditing())?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(countries && countries.length > 0)? 
			        		countries.map((action) => (
			        			(action.isEditing)?
			        				<ActionEdit key={action.id} action={action} onUpdateActionClick={this.onUpdateAction} onHandleActionInputChange={this.onActionInputChange} onCancelEditActionClick={this.onCancelEditAction}/>
			        			:
			        			(action.isNew)?
			        				<ActionNew key={action.id} action={action} onCancelAddNewActionClick={this.onCancelAddNewAction} onAddActionClick={this.onSaveNewAction} onHandleActionInputChange={this.onActionInputChange} />
			        			:
			        				<Action key={action.id} action={action} statusAsString={this.statusAsString} onEditActionClick={this.onEditAction} onDeleteActionClick={this.onDeleteAction}/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={5}> No existen paises registrados </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={5}>
					        <Button color="primary" onClick={() => this.onAddNewAction()} >
					        	Agregar Acción
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default ActionList;