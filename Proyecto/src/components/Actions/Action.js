import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Action extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {action, onEditActionClick, onDeleteActionClick, statusAsString} = this.props

		return(
			<>
			<tr key={action.id}>
				<td> {action.id} </td>		
				<td> {action.name} </td>
				<td> {statusAsString(action.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditActionClick(action.id)}> 
						Modificar
					</Button> 
				</td>
				<td> 
					<Button color="secondary" onClick={() => this.openHandleModal()}> 
						Eliminar
					</Button> 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={action} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={onDeleteActionClick} />
			</>
		);
	}
}

export default Action;