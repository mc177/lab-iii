import React from 'react';
import PropTypes from 'utils/propTypes';
import homeImage from 'assets/img/home.png';
import MySales from 'assets/img/sales.png';
import Search from 'assets/img/search.png';
import Settings from 'assets/img/settings.png';
import Add from 'assets/img/add.png';
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import {
  Badge,
  Button,
  Row, 
  Col, 
  ColHeader, 
  Card, 
  CardTitle, 
  CardSubtitle, 
  CardText, 
  CardBody,
  NavLink as BtnNavLink
} from 'reactstrap';

import Avatar from './Avatar';

const BottomMenu = ({
  avatar,
  avatarSize,
  title,
  subtitle,
  text,
  className,
  onAddNewPost,
  ...restProps
}) => {
  const classes = classNames('bottom-menu-style', className);

  return (
    <Row id="bottom-menu" className={classes}>
        <Col className="mb-3 bottom-menu-col">
              <BtnNavLink
                tag={NavLink}
                to={'/'}
                activeClassName="active"
                exact={true}
              >
                <Button color="primary" outline className="bottom-menu-button">
                  <img src={homeImage} size={avatarSize} className="mb-2 bottom-menu-icon" />
                </Button>
            </BtnNavLink>
        </Col>
        <Col  className="mb-3 bottom-menu-col">
            <BtnNavLink
              tag={NavLink}
              to={'/searchPosts'}
              activeClassName="active"
              exact={true}
            >
              <Button color="primary" outline className="bottom-menu-button">
                <img src={Search} size={avatarSize} className="mb-2 bottom-menu-icon" />
              </Button>
            </BtnNavLink>
        </Col>
        <Col className="mb-3 bottom-menu-col">
            <BtnNavLink
                tag={NavLink}
                to={'/newPost'}
                activeClassName="active"
                exact={true}
            >
              <Button color="primary" outline className="bottom-menu-button">
                <img src={Add} size={avatarSize} className="mb-2 bottom-menu-icon" onClick={() => onAddNewPost()}/>
              </Button>
            </BtnNavLink>
        </Col>
        <Col className="mb-3 bottom-menu-col">
            <BtnNavLink
                  tag={NavLink}
                  to={'/profile'}
                  activeClassName="active"
                  exact={true}
                >
                <Button color="primary" outline className="bottom-menu-button">   
                  <img src={MySales} size={avatarSize} className="mb-2 bottom-menu-icon" />
                </Button>
            </BtnNavLink>
        </Col>
        <Col className="mb-3 bottom-menu-col">
            <BtnNavLink
                  tag={NavLink}
                  to={'/my-liked-posts'}
                  activeClassName="active"
                  exact={true}
                >
              <Button color="primary" outline className="bottom-menu-button">
                <FontAwesomeIcon title="Me gusta"  className="heart-icon mb-2 bottom-menu-icon" style={{color:'black',marginLeft:0}} icon={faHeart}/>
              </Button>
            </BtnNavLink>
        </Col>
        
    </Row>
    /*<Card inverse className={classes} {...restProps}>
      <CardBody className="d-flex justify-content-center align-items-center flex-column">
        <CardTitle className={'text-black'}>Hola</CardTitle>
        <CardSubtitle>Epale</CardSubtitle>
        <CardText>
          <small>otro texto más</small>
        </CardText>
      </CardBody>
    </Card>*/
  );
};

BottomMenu.propTypes = {
  avatar: PropTypes.string,
  avatarSize: PropTypes.number,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  text: PropTypes.string,
  className: PropTypes.string,
};

BottomMenu.defaultProps = {
  avatarSize: 80,
};

export default BottomMenu;
