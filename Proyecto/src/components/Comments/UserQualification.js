import React from 'react'
import Rating from 'react-rating'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faPaperPlane, faArrowLeft} from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'
import {Link} from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardHeader,
  Badge,
  CardText,
  CardTitle,
  Col,
  Row,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  Progress,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';

class UserQualification extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			qualification: "",
			description: "Este usuario si sabe hacer las cosas",
			user: {
				name: "Jeferson",
				forename: "Alvarado",
				email: "jejoalca14@gmail.com",
				role: "Seller and Buyer",
				phone: "04245558208",
				adress: "Av. principal San Lorenzo",
				birthdate: "1996-07-30",
				sex: "M"
			}
		}
	}

	render(){
		const{ user, qualification, description } = this.state
		const {comment, order,onHandleCommentInput,onHandleScore, onPublishComment} = this.props
		return(
			<Card style={{textAlign: "center", height: 66}} className="card-header-top">
				<CardHeader className="title-custom card-header">
					<Link to="/detail-order" className="left-arrow"> <FontAwesomeIcon icon={faArrowLeft}/> </Link>
					<CardTitle> Califica a {(order && order.action_post.post.user.user_profile.name)? order.action_post.post.user.user_profile.name : ""}</CardTitle>
				</CardHeader>
				<CardBody className="card-body-responsive">
					<Row className="row-custom">
						<Form>
							<FormGroup style={{marginBottom: 52}}>
					            <h5 style={{marginBottom: 32}}>Selecciona una calificación de acuerdo con su experiencia: </h5>
					            <Rating
								  initialRating={comment.purchase.score}
								  onChange={(e) =>onHandleScore(e)}
					              fullSymbol={<FontAwesomeIcon style={{color: "#ffbc34", fontSize: "xx-large"}} icon={faStar}/>}
					              emptySymbol={<FontAwesomeIcon style={{color: "#ffbc34", fontSize: "xx-large"}} icon={farStar}/>}
					            /> 
					        </FormGroup>
					        <FormGroup  style={{marginBottom: 32}}>
			                  <Label style={{marginBottom: 20}} htmlFor="userDescription">Deja tu breve descripción del usuario:</Label>
			                  <Input 
			                  	type="textarea" 
			                  	name="message" 
			                  	defaultValue={comment.message}
			                  	onChange={(e) => onHandleCommentInput(e)}
			                  />
			                </FormGroup>
			             	<div style={{padding: "2%", marginBottom: "5px"}}>
				            	<Button title="Ver detalles" className="btn btn-success button-succes-custom see-details-liked-sells button-send" onClick={() => onPublishComment(order, comment)} style={{width: 56, height: 56, marginLeft: "50px"}}><FontAwesomeIcon icon={faPaperPlane}/></Button>
				            </div>
		             	</Form>
				    </Row>
				</CardBody>
			</Card>
		);
	}
}


export default UserQualification;