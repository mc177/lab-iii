import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg18Image from 'assets/img/bg/background_1920-18.jpg';
import bg1Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import user2Image from 'assets/img/users/100_2.jpg';
import user3Image from 'assets/img/users/100_3.jpg';
import yellowStar from 'assets/img/star_checked.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
  CardHeader,
  Media
} from 'reactstrap';
import Avatar from 'components/Avatar';
import {getFullDateNoHours, getHoursParse} from '../Utils/dateParse'
import './style.css'

class Comments extends React.Component {
  render(){
    const {comments} = this.props
    return (
      <Page className="custom-container" style={{paddingBottom:20}}>
          <Col lg="4" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                <div className="d-flex justify-content-between align-items-center">
                  <span>Comentarios Recibidos</span>
                </div>
              </CardHeader>
              <CardBody>
              {
              (comments && comments.length>0)?
                comments.map((comment,index) => (
                  <div>
                    <Media className="m-2">
                      <Media left className="mr-2">
                        <img  className="rounded-circle can-click flipthis-highlight user-image-cardsells" src={(comment.purchase.buyer && comment.purchase.buyer.buyer_profile && comment.purchase.buyer.buyer_profile.image)? process.env.REACT_APP_VERONICA_URL_WEB + 'uploads/users/' + comment.purchase.buyer.buyer_profile.image : process.env.REACT_APP_VERONICA_URL_WEB + 'assets/user_default.png' + comment.purchase.buyer.buyer_profile.image} />
                      </Media>
                      <Media body>
                        <Media heading tag="h6" className="m-0">
                          {comment.purchase.buyer.buyer_profile.name} {comment.purchase.buyer.buyer_profile.lastname}
                        </Media>
                        <p className="text-muted m-0 mcustom">
                          <small>{getFullDateNoHours(comment.publish_date)}</small>
                        </p>
                        <p className="text-muted m-0">
                          <small>{getHoursParse(comment.publish_date)}</small>
                        </p>
                      </Media>
                    </Media>
                    <Media>
                      <p className="text-muted">{comment.message}</p>
                    </Media>
                  </div>
                ))
              :
              <p> El usuario no ha recibido Comentarios </p>
              }
              </CardBody>
            </Card>
          </Col>     
        
      </Page>
    );
  }
};

export default Comments;
