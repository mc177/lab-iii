import React from 'react'
import {Button, Input} from 'reactstrap'

class CategoryEdit extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {category, onUpdateCategory, onCancelEditCategory,onHandleStatusInputChange, onHandleNameInputChange,onHandleDescriptionInputChange} = this.props

		return(
			<tr key={category.id}>
				<td> {category.id} </td>
				<td> 
					<Input 
						data-id={category.id}
						name="name"
						defaultValue={category.name}
						onChange={(e) => onHandleNameInputChange(e,category.id)}
					/>
				</td>
				<td> 
					<Input 
						data-id={category.id}
						name="description"
						defaultValue={category.description}
						onChange={(e) => onHandleDescriptionInputChange(e,category.id)}
					/>
				</td>		
				<td>  
		          <Input data-id={category.id} defaultValue={category.status} onChange={(e) => onHandleStatusInputChange(e)} type="select" name="status">
		            <option value={-1}>Selecciona una opción</option>
		            <option value={1}>Activo</option>
		            <option value={2}>Inactivo</option>
		          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onUpdateCategory(category)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelEditCategory(category.originalName)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default CategoryEdit;