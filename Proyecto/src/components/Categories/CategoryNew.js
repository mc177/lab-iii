import React from 'react'
import {Button, Input} from 'reactstrap'

class CategoryNew extends React.Component {
	
	constructor(props){
		super(props);
	}

	render(){
		const {category, 
				onSaveNewCategory, 
				onHandleNameInputChange,
				onHandleStatusInputChange,
				onHandleDescriptionInputChange,
				onCancelNewCategory
		} = this.props

		return(
			<tr key={category.id}>
				<td> {category.id} </td>
				<td> 
					<Input 
						data-id={category.id}
						name="name"
						defaultValue={category.name}
						onChange={(e) => onHandleNameInputChange(e)}
					/>
				</td>		
				<td> 
					<Input 
						data-id={category.id}
						name="description"
						defaultValue={category.description}
						onChange={(e) => onHandleDescriptionInputChange(e)}
					/>
				</td>
				<td>  
		          <Input data-id={category.id} defaultValue={category.status} onChange={(e) => onHandleStatusInputChange(e)} type="select" name="status">
		            <option value={-1}>Selecciona una opción</option>
		            <option value={1}>Activo</option>
		            <option value={2}>Inactivo</option>
		          </Input>
				</td>
				<td> 
					<Button color="success" onClick={() => onSaveNewCategory(category)}> 
						Guardar
					</Button> 
				</td>
				<td> 
					<Button onClick={() => onCancelNewCategory(category.id)}> 
						Cancelar
					</Button> 
				</td>
			</tr>
		);
	}
}

export default CategoryNew;