import React from 'react'
import {Button} from 'reactstrap'
import ModalConfirmDeleteItem from '../Utils/ModalConfirmDeleteItem'

class Category extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			modal: false
		}
		this.openHandleModal = this.openHandleModal.bind(this)
	}

	openHandleModal(){
		this.setState({modal: !this.state.modal})
	}

	render(){
		const {category, onEditCategory, onDeleteCategory, statusAsString} = this.props

		return(
			<>
			<tr key={category.id}>
				<td> {category.id} </td>		
				<td> {category.name} </td>
				<td> {category.description} </td>
				<td> {statusAsString(category.status)} </td>
				<td> 
					<Button color="primary" onClick={() => onEditCategory(category.id)}> 
						Modificar
					</Button> 
				</td>
				<td>
					{ category.status==1 &&
						<Button color="secondary" onClick={() => this.openHandleModal()}> 
							Eliminar
						</Button>
					} 
				</td>
			</tr>
			<ModalConfirmDeleteItem item={category} modal={this.state.modal} onCloseModal={this.openHandleModal} onSubmit={()=> {onDeleteCategory(category.id);this.openHandleModal()}} />
			</>
		);
	}
}

export default Category;