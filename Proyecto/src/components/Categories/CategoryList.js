import React from 'react'
import { Table } from 'reactstrap';
import {
  Row,
  Col,
  Card,
  Button,
  CardHeader,
  CardBody,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Dropdown,
  FormText
} from 'reactstrap';

import Category from './Category'
import CategoryEdit from './CategoryEdit'
import CategoryNew from './CategoryNew'

class CategoryList extends React.Component {

	constructor(props){
		super(props)		
		this.hasSomeCategoryEditing = this.hasSomeCategoryEditing.bind(this)
	}


	statusAsString(status){
		let statusAsString = "";

		switch(parseInt(status, 10)){
			case 1: 
				statusAsString = "Activo"; break;
			default:
				statusAsString = "Inactivo"; break;
		}

		return statusAsString;
	}

	hasSomeCategoryEditing(categories){
		let _is_some = false 
		if(categories && categories.length>0)
			categories.map((category) => {
				if(category.isEditing){
					_is_some=true
				}
			})
		return _is_some
	}

	render(){
		const {
			categories,
			onHandleNameInputChange,
			onHandleDescriptionInputChange,
			onAddNewCategory,
			onDeleteCategory,
			onEditCategory,
			onCancelEditCategory,
			onHandleStatusInputChange,
			onUpdateCategory,
			onSaveNewCategory
		} = this.props

		return(
			<div align="center" className="custom-container-without-space">
				<Card>
					<CardHeader> 
						Listado de Categorias 
					</CardHeader> 
					<CardBody>
						<Table responsive>
			        <thead>
			          <tr>
			            <th>ID</th>
			            <th>Nombre</th>
			            <th>Descripción</th>
			            <th>Estado</th>
			            {
			            	(this.hasSomeCategoryEditing(categories))?
			            		<th colSpan={2}>Operaciones</th>
			            	:
			            	<>
					            <th>Modificar</th>
					            <th>Eliminar</th>
			            	</>
			            }
			          </tr>
			        </thead>
			        <tbody>
			        {
			        	(categories && categories.length > 0)? 
			        		categories.map((category) => (
			        			(category.isEditing)?
									<CategoryEdit 
										key={category.id} category={category}
										onUpdateCategory={(c)=>onUpdateCategory(c)}  
										onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,category.id)} 
										onCancelEditCategory={(text)=>onCancelEditCategory(text,category.id)}
										onEditCategory = {()=>onEditCategory(category.id)}
										onHandleNameInputChange={(e)=>onHandleNameInputChange(e,category.id)}
										onHandleDescriptionInputChange={(e)=>onHandleDescriptionInputChange(e,category.id)} />
			        			:
			        			(category.isNew)?
									<CategoryNew key={category.id} category={category} 
												onHandleNameInputChange={(e)=>onHandleNameInputChange(e,category.id)}
												onHandleStatusInputChange = {(e)=>onHandleStatusInputChange(e,category.id)} 
												onCancelNewCategory = {()=>onDeleteCategory(category.id)}
												onSaveNewCategory = {(c)=>onSaveNewCategory(c)}
												onHandleDescriptionInputChange={(e)=>onHandleDescriptionInputChange(e,category.id)} 
												/>
			        			:
									<Category key={category.id} category={category} 
											statusAsString={this.statusAsString} 
											onEditCategory={()=>onEditCategory()} 
											onDeleteCategory = {()=>onDeleteCategory(category.id)}
											onEditCategory = {()=>onEditCategory(category.id)}/>        		
									))
			        	:
			        	<tr> 
			        		<td colSpan={7}> No existen categorias registradas </td>
			        	</tr>
			        }
			        <tr>
			        	<td colSpan={7}>
					        <Button color="primary" onClick={() => onAddNewCategory()} >
					        	Agregar Categoría
					        </Button>
				      	</td>
				      </tr>
			        </tbody>
			      </Table>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default CategoryList;