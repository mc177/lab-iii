import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg18Image from 'assets/img/bg/background_1920-18.jpg';
import bg1Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import yellowStar from 'assets/img/star_checked.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardSubtitle,
} from 'reactstrap';
import Avatar from 'components/Avatar';

const MyLikes = () => {
  return (
    <Page className="custom-container">
        <Row className="publication-row m-0">
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
          <Col style={{paddingRight:0, paddingLeft:0}} className="col-4" sm="4" md="4" lg="4" xl="4">
            <Card>
              <div className="position-relative">
                <CardImg src={user1Image} />
              </div>
            </Card>
          </Col>
        </Row>      
      
    </Page>
  );
};

export default MyLikes;
