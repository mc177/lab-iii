import img_publication1 from 'assets/img/custom-img/Emerald-Dream.png';
import img_publication2 from 'assets/img/custom-img/Queen.png';
import user1Image from 'assets/img/users/100_1.jpg';
import yellowStar from 'assets/img/star_checked.png'
import like from 'assets/img/like.png'
import border_like from 'assets/img/border-like.png'
import share from 'assets/img/share.png'
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart, faShareAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import React from 'react';
import { Line } from 'react-chartjs-2';
import classNames from 'classnames';
import { Link } from 'react-router-dom'
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  Col,
  ListGroup,
  Row,
  Badge,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Input,
  Label
} from 'reactstrap';
import Avatar from 'components/Avatar';

class MyPublications extends React.Component {

    state = {
        modal: false,
    };

    clickModal = (e) =>{
        return this.setState({
            modal: !this.state.modal,
        }); 
    }
    render(){
        return (
            <Page className="custom-container">
                <Row>
                    <Col md={5}>
                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <div className="user-header" style={{zIndex: 10,top: 0}}>
						          	<li className="nav-item" style={{listStyle: "none"}}>
						          		<a id="Popover2" className="nav-link">
						          			<img src="/static/media/user_example.7414fd41.jpg" className="rounded-circle can-click flipthis-highlight user-image-cardsells"/>
						          			<label style={{marginLeft: "2%", color: "white", marginTop:"2%"}}>
						          				Jeferson Alvarado
						          			</label>
						          		</a>
						          	</li>
						        </div>
                                <Card className="img-standard-card">
                                <div className="position-relative">
                                    <CardImg style={{minHeight:250}} src={img_publication1} />
                                </div>
                                </Card>
                                <Row style={{width:'100%', marginTop:10}}>
                                    <FontAwesomeIcon title="Me gusta"  className="heart-icon" icon={faHeart}/>
                                    <FontAwesomeIcon title="Compartir" className="share-icon" icon={faShareAlt} style={{color:"black"}} onClick={this.clickModal}/>
                                    <Badge color="primary" pill className="mr-1 tool" style={{marginTop:5, marginBottom:9}}>
                                        Electrónicos
                                    </Badge>
                                </Row>
                                <Row className="m-0 row-standard-card-text" >
                                    <CardText>
                                        <small>Lorem ipsum dolor sit amet consectetur adipiscing elit, aenean urna non suscipit a rhoncus ut rutrum, platea dis class convallis ligula.</small>
                                    </CardText>
                                </Row>
                                <Row className="m-rl-0" style={{height:35,marginTop:15,marginBottom: 13}}>
                                    <Link to="/detail-publication">
                                        <Button title="Ver detalles" className="btn btn-success button-succes-custom btn-show"><FontAwesomeIcon icon={faEye}/></Button>
                                    </Link>
                                </Row>
                            </CardBody>
                        </Card>

                        <Card inverse style={{marginBottom:20}}>
                            <CardBody className="d-flex justify-content-center align-items-center standard-card-body flex-column">
                                <div className="user-header" style={{zIndex: 10,top: 0}}>
						          	<li className="nav-item" style={{listStyle: "none"}}>
						          		<a id="Popover2" className="nav-link">
						          			<img src="/static/media/user_example.7414fd41.jpg" className="rounded-circle can-click flipthis-highlight user-image-cardsells"/>
						          			<label style={{marginLeft: "2%", color: "white", marginTop:"2%"}}>
						          				Jeferson Alvarado
						          			</label>
						          		</a>
						          	</li>
						        </div>
                                <Card className="img-standard-card">
                                <div className="position-relative">
                                    <CardImg style={{minHeight:250}} src={img_publication2} />
                                </div>
                                </Card>
                                <Row style={{width:'100%', marginTop:10}}>
                                    <FontAwesomeIcon title="Me gusta"  className="heart-icon" icon={farHeart}/>
                                    <FontAwesomeIcon title="Compartir" className="share-icon" icon={faShareAlt} style={{color:"black"}} onClick={this.clickModal}/>
                                    <Badge color="primary" pill className="mr-1 tool" style={{marginTop:5, marginBottom:9}}>
                                        Electrónicos
                                    </Badge>
                                </Row>
                                <Row className="m-0 row-standard-card-text" >
                                    <CardText>
                                        <small>Lorem ipsum dolor sit amet consectetur adipiscing elit, aenean urna non suscipit a rhoncus ut rutrum, platea dis class convallis ligula.</small>
                                    </CardText>
                                </Row>
                                <Row className="m-rl-0" style={{height:35,marginTop:15,marginBottom: 13}}>
                                    <Link to="/detail-publication">
                                        <Button title="Ver detalles" className="btn btn-success button-succes-custom btn-show"><FontAwesomeIcon icon={faEye}/></Button>
                                    </Link>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Modal
                    isOpen={this.state.modal}>
                    <ModalHeader>Compartir publicación</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="email">Correo</Label>{' '}
                            <Input
                                type="email"
                                name="email"
                                placeholder="ejemplo@gmail.com"
                            />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" >
                        Enviar
                        </Button>{' '}
                        <Button
                        color="secondary" onClick={()=> this.clickModal()}>
                        Cancelar
                        </Button>
                    </ModalFooter>
                </Modal>   
            </Page>
        );
    }
}

export default MyPublications
