import AuthForm, { STATE_LOGIN, STATE_SIGNUP } from 'components/AuthForm';
import React from 'react';
import { Card, Col, Row } from 'reactstrap';

class AuthPage extends React.Component {
  handleAuthState = authState => {
    if (authState === STATE_LOGIN) {
      this.props.history.push('/login');
    } else {
      if (authState === STATE_SIGNUP) {
      this.props.history.push('/signup');
      }
      else{
        this.props.history.push('/recover-password');
      }
    }
  };

  handleLogoClick = () => {
    this.props.history.push('/login');
  };

  render() {
    return (
      <Row
        style={{
          height: '100vh',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Col md={6} lg={4}>
          <Card body>
            <AuthForm
              authState={this.props.authState}
              onChangeAuthState={this.handleAuthState}
              onLogoClick={this.handleLogoClick}
              history={this.props.history}
            />
          </Card>
        </Col>
      </Row>
    );
  }
}

export default AuthPage;
