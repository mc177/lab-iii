export const HANDLE_CATEGORY_NAME_INPUT_CHANGE = 'HANDLE_CATEGORY_NAME_INPUT_CHANGE'
export const HANDLE_CATEGORY_DESCRIPTION_INPUT_CHANGE = 'HANDLE_CATEGORY_DESCRIPTION_INPUT_CHANGE'
export const ADD_NEW_CATEGORY = 'ADD_NEW_CATEGORY'
export const DELETE_CATEGORY = 'DELETE_CATEGORY'
export const EDIT_CATEGORY = 'EDITING_CATEGORY'
export const CANCEL_EDIT_CATEGORY = 'CANCEL_EDIT_CATEGORY'
export const HANDLE_CATEGORY_STATUS_INPUT_CHANGE = 'HANDLE_CATEGORY_STATUS_INPUT_CHANGE'
export const FETCH_REQUEST_CATEGORIES_SUCCEEDED = 'FETCH_REQUEST_CATEGORIES_SUCCEEDED'
export const SAVE_NEW_CATEGORY = 'SAVE_NEW_CATEGORY'
export const UPDATE_CATEGORY = 'UPDATE_CATEGORY'
export const FILTER_CATEGORY = 'FILTER_CATEGORY'
export const FETCH_POPULAR_CATEGORIES_SUCCEEDED= 'FETCH_POPULAR_CATEGORIES_SUCCEEDED'

export function handleNameInputChange(event,id){
	return {
		type: HANDLE_CATEGORY_NAME_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}

export function fetchPopularCategoriesSucceeded(categories){
	return{
		type: FETCH_POPULAR_CATEGORIES_SUCCEEDED,
		categories
	}
}

export function handleDescriptionInputChange(event,id){
	return{
		type: HANDLE_CATEGORY_DESCRIPTION_INPUT_CHANGE,
		text: event.target.value,
		id:id
	}
}

export function updateCategory(category){
	return{
		type: UPDATE_CATEGORY,
		category: category
		
	}
}

export function saveNewCategory(category,id){
	return{
		type: SAVE_NEW_CATEGORY,
		category: category,
		new_category_id:id
	}
}

export function handleStatusInputChange(event,id){
	return {
		type: HANDLE_CATEGORY_STATUS_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}

export function addNewCategory(){
	return {
		type: ADD_NEW_CATEGORY,
	}
}

export function deleteCategory(id){
	return {
		type: DELETE_CATEGORY,
		id: id
	}
}

export function editCategory(id){
	return {
		type: EDIT_CATEGORY,
		id: id
	}
}

export function cancelEditCategory(text,id){
	return{
		type: CANCEL_EDIT_CATEGORY,
		text: text,
		id:id
	}
}

export function fetchRequestCategoriesSucceeded(categories){
	return{ 
		type: FETCH_REQUEST_CATEGORIES_SUCCEEDED,
		categories: categories
	}
}

export function filterCategory(category,posts){
	return{
		type: FILTER_CATEGORY,
		posts,
		category
	}
}

export function fetchCategories(){

	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"categories",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
				dispatch(fetchRequestCategoriesSucceeded(json.categories))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar los paises')
		})
		
	}
}

export function fetchCreateCategory(category){
	let newCategory = {
		name : category.name,
		description: category.description,
		status: category.status      
	}
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"categories",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(newCategory)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(saveNewCategory(json.category,category.id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchDeleteCategory(id){
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"categories/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("Categoria eliminado exitosamenteeeeee")
				dispatch(deleteCategory(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		})
		
	}
}

export function fetchUpdateCategory(category){
	let category_to_update = {
			name : category.name,
			description : category.description,
			status: category.status      
	}
	return function (dispatch,getState) {	
        const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"categories/"+category.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(category_to_update)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateCategory(category))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la modificación')
		})
		
	}
}

export function fetchFilterCategory(category){
	return function (dispatch,getState) {
		let filter = {
			category_id: category.id
		}	
		const token = getState().main.globalData.token;
		const email = getState().main.userData.email;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"posts/filter",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						"X-Email": email,
                        "X-Token": token,
                        'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(filter)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 401)
					return response.json().then(err => { throw err; });
				if (response.status === 500)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					if(json.posts){
						dispatch(filterCategory(category,json.posts))
					}else{
						dispatch(filterCategory(category,[]))
					}			
				}else{										
                    console.log('No se pudo actualizar el filtro')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo actualizar el filtro')
		      })
	}
}
