import {fetchActions} from "./actionAction"
import {cancelPurchaseOrder,changeOrderStatus,fetchMyPosts} from "./postAction"
import {fetchPopularCategoriesSucceeded} from "./categoriesAction"

export const HANDLE_USER_INPUT_CHANGE = 'HANDLE_USER_INPUT_CHANGE'
export const HANDLE_USER_PROFILE_INPUT_CHANGE = 'HANDLE_USER_PROFILE_INPUT_CHANGE'
export const HANDLE_AUTH_STATE_CHANGE = 'HANDLE_AUTH_STATE_CHANGE'
export const AUTH_USER_SUCCEEDED = 'AUTH_USER_SUCCEEDED'
export const AUTH_USER_FAILED = "AUTH_USER_FAILED"
export const RESET_USER_DATA = 'RESET_USER_DATA'
export const CANCEL_EDIT_PROFILE = 'CANCEL_EDIT_PROFILE'
export const SAVE_USER_PROFILE = 'SAVE_USER_PROFILE'
export const SHOW_ORDER = 'SHOW_ORDER'
export const CANCEL_ORDER = 'CANCEL_ORDER'
export const UPDATE_ORDER_STATUS = 'UPDATE_ORDER_STATUS'
export const UPDATE_USER_PROFILE = 'UPDATE_USER_PROFILE'
export const HANDLE_CANCEL_ORDER_DESCRIPTION = 'HANDLE_CANCEL_ORDER_DESCRIPTION'
export const RESET_MAIN_FETCH = 'RESET_MAIN_FETCH'
export const FOLLOW_CATEGORY = 'FOLLOW_CATEGORY'
export const UNFOLLOW_CATEGORY = 'UNFOLLOW_CATEGORY'
export const FOLLOWED_CATEGORIES_SUCCEEDED = 'FOLLOWED_CATEGORIES_SUCCEEDED'
export const HANDLE_INPUT_FILE_CHANGE = 'HANDLE_INPUT_FILE_CHANGE'
export const DELETE_ORDER_PICTURES = 'DELETE_ORDER_PICTURES'

export const REQUEST_FINISH_PURCHASE_SUCCEDED = 'REQUEST_FINISH_PURCHASE_SUCCEDED'
export const REQUEST_FINISH_PURCHASE_FAILED = 'REQUEST_FINISH_PURCHASE_FAILED'
export const HANDLE_CHANGE_PASSWORD_INPUT_CHANGE = 'HANDLE_CHANGE_PASSWORD_INPUT_CHANGE'
export const RESET_CHANGE_PASSWORD = 'RESET_CHANGE_PASSWORD'
export const FETCH_MONTHLY_GAINS_SUCCEEDED = 'FETCH_MONTHLY_GAINS_SUCCEEDED'
export const SHOW_MONTH_GAIN = 'SHOW_MONTH_GAIN'
export const FETCH_USER_GAINS_SUCCEEDED = 'FETCH_USER_GAINS_SUCCEEDED'

export function fetchUserGainsSucceeded(user_gains){
	return{
		type:FETCH_USER_GAINS_SUCCEEDED,
		user_gains
	}
}

export function fetchMonthlyGainsSucceeded(monthly_gains){
	return{
		type: FETCH_MONTHLY_GAINS_SUCCEEDED,
		monthly_gains
	}
}

export function showMonthGain(month_gain){
	return{
		type: SHOW_MONTH_GAIN,
		month_gain
	}
}

export const REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED = 'REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED'
export const REQUEST_CHANGE_PROFILE_IMAGE_FAILED = 'REQUEST_CHANGE_PROFILE_IMAGE_FAILED'

export const REQUEST_GET_USER_AVERAGE_SCORE_SUCCEDED = 'REQUEST_GET_USER_AVERAGE_SCORE_SUCCEDED'
export const REQUEST_GET_USER_AVERAGE_SCORE_FAILED = 'REQUEST_GET_USER_AVERAGE_SCORE_FAILED'

function requestGetUserAverageScoreSucceded(score){
	return {
		type: REQUEST_GET_USER_AVERAGE_SCORE_SUCCEDED,
		average: score
	}
}

function requestGetUserAverageScoreFailed(){
	return {
		type: REQUEST_GET_USER_AVERAGE_SCORE_FAILED
	}
}

export function resetChancePassword(){
	return{
		type: RESET_CHANGE_PASSWORD
	}
}

export function handleChangePasswordInputChange(event){
	return{
		type: HANDLE_CHANGE_PASSWORD_INPUT_CHANGE,
		event
	}
}

function requestFinishPurchaseSucceded(purchase){
	return {
		type: REQUEST_FINISH_PURCHASE_SUCCEDED,
		purchase
	}
}

function requestFinishPurchaseFailed(){
	return {
		type: REQUEST_FINISH_PURCHASE_FAILED
	}
}

export function deleteOrderImage(index){
	return {
		type: DELETE_ORDER_PICTURES,
		index
	}
}

export function handleInputFileChange(event){
	return {
		type: HANDLE_INPUT_FILE_CHANGE,
		event
	}
}

export function followCategory(category){
	return{
		type: FOLLOW_CATEGORY,
		category
	}
}

export function unFollowCategory(category){
	return{
		type: UNFOLLOW_CATEGORY,
		category
	}
}

export function followedCategoriesSucceeded(followed_categories){
	return{
		type: FOLLOWED_CATEGORIES_SUCCEEDED,
		followed_categories
	}
}

export function resetFetch(){
	return{
		type: RESET_MAIN_FETCH
	}
}

export function cancelOrder(order){
	return {
		type: CANCEL_ORDER,
		order
	}
}
export function updateOrderStatus(type_action){
	return {
		type: UPDATE_ORDER_STATUS,
		type_action
	}
}

export function handleCancelOrderDescription(event){
	return{
		type:HANDLE_CANCEL_ORDER_DESCRIPTION,
		event
	}
}

export function handleUserInputChange(event){
	return{
		type:HANDLE_USER_INPUT_CHANGE,
		event
	}
}

export function handleUserProfileInputChange(event){
	return{
		type:HANDLE_USER_PROFILE_INPUT_CHANGE,
		event
	}
}

export function handleAuthStateChange(text){
	return{
		type: HANDLE_AUTH_STATE_CHANGE,
		text
	}
}

export function authUserSucceeded(user,token){
	return{
		type: AUTH_USER_SUCCEEDED,
		user: user,
		token
	}
}

export function authUserFailed(message){
	return{
		type: AUTH_USER_FAILED,
		message
	}
}

export function resetUserData(){
	return{
		type: RESET_USER_DATA
	}
}

export function cancelEditProfile(){
	return{
		type: CANCEL_EDIT_PROFILE
	}
}

export function saveUserProfile(){
	return{
		type: SAVE_USER_PROFILE
	}
}

export function showOrder(order){
    return {
		type: SHOW_ORDER,
		order
    }
}
export function updateUserProfile(user){
	return{
		type: UPDATE_USER_PROFILE,
		user
	}
}

function requestChangeProfileImageSucceded(image){
	return {
		type: REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED,
		image
	}
}

function requestChangeProfileImageFailed(){
	return {
		type: REQUEST_CHANGE_PROFILE_IMAGE_FAILED
	}
}


export function fetchFinishOrder(order,image){
	return (dispatch, getState) => {

    let purchase = {
    	purchase_id: order.id
	  }

    const dataWithFile = new FormData();

    dataWithFile.append('purchase', JSON.stringify(purchase));

    for (var i=0; i < order.pictures.length; i++) {
      console.log(order.pictures[i])
      dataWithFile.append('image'+i, order.pictures[i])
    }

		const token = getState().main.globalData.token;
    const email = getState().main.userData.email;

		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"purchase/finish",{
				method: "PUT", // *GET, POST, PUT, DELETE, etc.
				mode: "cors", // no-cors, cors, *same-origin
				cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
				//credentials: "with-credentials", // include, same-origin, *omit
				headers: {
					'Authorization': 'Bearer ' + token
				},
				body: dataWithFile
		})
		.then(
			response => {
				
				if (response.ok) {
					return response.json()
				}
				if (response.status === 422)
					return response.json().then(err => { throw err; });
				
			},

			error => console.log('An error occurred.', error)
		)
		.then(json => {
			console.log(json)
				if (json && json.status != 500) {
					dispatch(requestFinishPurchaseSucceded(json.purchase))
					dispatch(updateOrderStatus(5))
				}else{
					dispatch(requestFinishPurchaseFailed())
				}		
		})
		.catch((errors) => {
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchCancelPurchaseOrder(order){
	return function (dispatch,getState) {
		let json = {
            purchase: order.id,
            cancel_description: order.cancel_description
		}	
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"purchase/cancel",{
					method: "PUT",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
                        'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 401)
					return response.json().then(err => { throw err; });
				if (response.status === 500)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json) {
					console.log(json)
                    if(json.status==200){
						dispatch(cancelPurchaseOrder(order))
						dispatch(cancelOrder(order))
                    }        		
				}		
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo cancelar la orden')
		      })
	}
}

export function fetchChangeOrderStatus(order,type){
	//estados 2:aceptada , 3:rechazada , 5:finalizada
	let route = ""
	switch(type){
		case 2: route="purchase/accept"
			break;
		case 3:	route="purchase/reject"
			break;
		case 5:	route="purchase/finish"
			break;
	}
	console.log("esta es la ruta "+route)
	return function (dispatch,getState) {
		let json = {
            purchase: order.id,
		}	
		const token = getState().main.globalData.token;	
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+route,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(json)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 404)
				return response.json().then(err => { throw err; });
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 500)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
				if(json.status==200){
					dispatch(updateOrderStatus(type))
					dispatch(changeOrderStatus(order,type))
				}
				else{
					if(json.status==401)
					console.log(json.message)
				}			
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo modificar el estado de la orden')
		})
	}
}



export function fetchSaveUserProfile(user){
	let update_userProfile = {		
		telephone: user.user_profile_saved_values.phone,
		country: user.user_profile_saved_values.country.id,	
		name: user.user_profile_saved_values.name,
		surname: user.user_profile_saved_values.lastname,
		birthday: user.user_profile_saved_values.birthday,
		gender: user.user_profile_saved_values.sex,
		
	}
	return function (dispatch,getState) {
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"users/"+user.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(update_userProfile)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 400)
			return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {console.log(json)
			if (json) {			
				if(json.status==200){
					dispatch(updateUserProfile(user))
				}
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la modificación')
		})
		
	}
}

export function fetchCreateUser(user){
	let newUser = {
			email : user.email,
			password: user.password,
			passwordConfirm: user.passwordConfirm,
			country: user.country_id      
	}
	return dispatch => {
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"register",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		body: JSON.stringify(newUser)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchAuthUser(user){
	let userJson = {
		email: user.email,
		password: user.password
	}
	
	return function (dispatch){
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"login",{
			method: "POST", // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			//credentials: "with-credentials", // include, same-origin, *omit
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				// "Content-Type": "application/x-www-form-urlencoded",
				//"X-Email": email,
				//"X-Token": token
			},
			body: JSON.stringify(userJson)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			if (response.status === 401)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
				dispatch(authUserSucceeded(json.user,json.token))
				dispatch(fetchActions())
				dispatch(fetchFollowedCategories())
				dispatch(fetchMyPosts())
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))

			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			dispatch(authUserFailed(errors.message))
			console.log('No se pudo completar el inicio de sesion')
		})
	}
}

export function fetchFollowCategory(category){
	return function (dispatch,getState) {	
		let follow_category = {
			user_id: getState().main.userData.id,
			category_id : category.id
		}
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"user_category/follow",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(follow_category)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 401)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					dispatch(followCategory(category))
				}else{										
                    console.log('No se pudo seguir el post ')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo seguir el post')
		      })
	}
}

export function fetchUnFollowCategory(category){
	return function (dispatch,getState) {	
		let unfollow_category = {
			user_id: getState().main.userData.id,
			category_id : category.id
		}
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"user_category/unfollow",{
					method: "PUT",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,*/
                        'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(unfollow_category)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 401)
					return response.json().then(err => { throw err; });
				if (response.status === 404)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					dispatch(unFollowCategory(category))
				}else{										
                    console.log('No se pudo dejar de seguir el post ')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo dejar de seguir el post')
		      })
	}
}

export function fetchFollowedCategories(){
	return function (dispatch,getState) {	
		let json = {
			user_id: getState().main.userData.id,
		}
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"user_category/followed_categories",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					if(json.categories && json.categories.length>0)
						dispatch(followedCategoriesSucceeded(json.categories))
					else 
						dispatch(followedCategoriesSucceeded([]))
				}else{										
                    console.log('No se pudieron cargar las categorias seguidas ')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudieron cargar las categorias seguidas')
		      })
	}
}

export function fetchChangePassword(change_password){
	return function (dispatch,getState) {	
		let json = {
			password: change_password.current_password,
			newPassword: change_password.new_password,
			passwordConfirm: change_password.new_password_confirm,
			user_id: getState().main.userData.id,
		  }
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"users/change_password",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					if(json.status==200){
						console.log('Contraña cambiada con éxito ')
					} else{
						console.log('No se pudo cambiar la contraseña')
					}
				}else{										
                    console.log('No se pudo cambiar la contraseña')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo cambiar la contraseña')
		      })
	}
}

export function fetchMonthlyGains(){
	return function (dispatch,getState) {	
		let json = {
			user_id: getState().main.userData.id,
		  }
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"users/monthly_gains",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					if(json.status==200){
						dispatch(fetchMonthlyGainsSucceeded(json.months))
					} else{
						console.log('No tiene ganancias')
						dispatch(fetchMonthlyGainsSucceeded([]))
					}
				}else{										
                    console.log('No se pudieron cargar las ganancias')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				//console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				//console.log("Message:" + message)
                console.log('Error: No se pudieron cargar las ganancias')
		      })
	}
}

export function fetchUserGains(){
	return function (dispatch,getState) {	
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"users/popular_users",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					if(json.status==200){
						dispatch(fetchUserGainsSucceeded(json.popular_users))
					} else{
						console.log('No se consiguieron los usuarios')
						dispatch(fetchUserGainsSucceeded([]))
					}
				}else{										
                    console.log('No se pudieron cargar las ganancias de los usuarios')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				//console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				//console.log("Message:" + message)
                console.log('Error: No se pudieron cargar las ganancias de los usuarios')
		      })
	}
}

export function fetchPopularCategories(){
	return function (dispatch,getState) {	
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"user_category/popular_categories",{
					method: "GET",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
						'Authorization': 'Bearer ' + token
					},
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {	
				console.log(json)			
				if (json != null) {
					if(json.status==200){
						dispatch(fetchPopularCategoriesSucceeded(json.categories))
					} else{
						console.log('No se consiguieron los usuarios')
						dispatch(fetchPopularCategoriesSucceeded([]))
					}
				}else{										
                    console.log('No se pudo cargar el rankin de categorias')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				//console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				//console.log("Message:" + message)
                console.log('Error: No se pudo cargar el rankin de categorias')
		      })
	}
}

export function downloadImage(path){
	console.log(path)
	return function (dispatch,getState) {	
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"purchase/download_image/"+path,{
					method: "GET",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						/*"X-Email": email,
                        "X-Token": token,
						'Authorization': 'Bearer ' + token*/
					},
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 500)
					return response.json().then(err => { throw err; });
				if (response.status === 400)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json != null) {
					console.log(json)
					if(json.status==200){
						//dispatch(fetchMonthlyGainsSucceeded(json.months))
					} 
				}else{										
                    console.log('No se pudo descargar la imagen')
                }			
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo descargar la imagen')
		      })
	}
}

export function fetchUploadProfileImage(event){
	
	return function(dispatch, getState){
		const token = getState().main.globalData.token;
		const user = getState().main.userData;
		let allowed_extensions = ['jpg', 'jpeg', 'png', 'gif'];
		let file = event.target.files[0];
		let short_name = file.name.split('.')
    let extension = short_name[short_name.length -1]

		if(file){
			if(allowed_extensions.indexOf(extension) >= 0){
				const dataWithFile = new FormData()
				dataWithFile.append('image', file) 

				return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"users/"+user.id+"/uploadProfileImage",{
		        method: "POST",
		        mode: "cors",
		        headers: {
							'Authorization': 'Bearer ' + token
		        },
		        body: dataWithFile
		    })
	      .then(
	        response => {
	    		if (response.ok){
						return response.json()
					}	
					if (response.status === 422)
						return response.json().then(err => { throw err; });
					},

	        error => console.log('An error occurred.', error)
	      )
	      .then(json => {
	      	console.log(json)
		      	if (json && json.status !== 500) {
							dispatch(requestChangeProfileImageSucceded(json.image))
							alert(json.message)
		      	}else{
							dispatch(requestChangeProfileImageFailed())
		      	}
		      }
	      )
	      .catch((errors) => {
					console.log(errors);
				});		  
			}else{
				alert('Formato de archivo no válido, las extensiones permitidas son ' + allowed_extensions.join(', '));
			}
		}else{
			alert('No se ha seleccionado ninguna imágen');
		}
	}
}

export function getUserAverageScore(){

	return function(dispatch, getState){

		let token = getState().main.globalData.token;
		let user = {
			user: getState().main.userData.id
		}

		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"users/averageUserScore",{
        method: "POST",
        mode: "cors",
        headers: {
        	"Content-Type": "application/json; charset=utf-8",
					'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(user)
    })
    .then(
      response => {
  		if (response.ok){
				return response.json()
			}	
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			},

      error => console.log('An error occurred.', error)
    )
    .then(json => {
    	console.log(json.average)
      	if (json && json.status !== 500) {
					dispatch(requestGetUserAverageScoreSucceded(json.average))
      	}else{
					dispatch(requestGetUserAverageScoreFailed())
      	}
      }
    )
    .catch((errors) => {
			console.log(errors);
		});


	}

}
