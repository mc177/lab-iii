export const FETCH_REQUEST_ACTIONS_SUCCEEDED = 'FETCH_REQUEST_ACTIONS_SUCCEEDED'



export function fetchRequestActionsSucceeded(actions){
	return{ 
		type: FETCH_REQUEST_ACTIONS_SUCCEEDED,
		actions: actions
	}
}
export function fetchActions(){
	
	return function (dispatch,getState) {	
		const token = getState().main.globalData.token;
		const email = getState().main.userData.email;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"actions",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
            //"X-Token": token,
            'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(fetchRequestActionsSucceeded(json.actions))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar las acciones')
		})
		
	}
	
}

