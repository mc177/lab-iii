export const HANDLE_ROL_NAME_INPUT_CHANGE = 'HANDLE_NAME_INPUT_CHANGE'
export const HANDLE_ROL_DESCRIPTION_INPUT_CHANGE = 'HANDLE_DESCRIPTION_INPUT_CHANGE'
export const HANDLE_ROL_STATUS_INPUT_CHANGE = 'HANDLE_STATUS_INPUT_CHANGE'
export const ADD_NEW_ROL = 'ADD_NEW_ROL'
export const DELETE_ROL = 'DELETE_ROL'
export const EDIT_ROLE = 'EDIT_ROLE'
export const CANCEL_EDIT_ROL = 'CANCEL_EDIT_ROL'
export const FETCH_REQUEST_ROLES_SUCCEEDED = 'FETCH_REQUEST_ROLES_SUCCEEDED'
export const SAVE_NEW_ROL = 'SAVE_NEW_ROL'
export const UPDATE_ROLE = 'UPDATE_ROLE'

export function handleNameInputChange(event,id){
	return {
		type: HANDLE_ROL_NAME_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}
export function handleDescriptionInputChange(event,id){
	return {
		type: HANDLE_ROL_DESCRIPTION_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}
export function handleStatusInputChange(event,id){
	return {
		type: HANDLE_ROL_STATUS_INPUT_CHANGE,
		text:event.target.value,
		id:id
	}
}
export function addNewRol(){
	return {
		type: ADD_NEW_ROL,
	}
}

export function updateRol(rol){
	return {
		type: UPDATE_ROLE,
		rol: rol
	}
}

export function deleteRol(id){
	return {
		type: DELETE_ROL,
		id: id
	}
}

export function editRol(id){
	return {
		type: EDIT_ROLE,
		id: id
	}
}

export function saveNewRol(id,new_role_id){
	return{
		type: SAVE_NEW_ROL,
		id:id,
		new_role_id:new_role_id
	}
}

export function cancelEditRol(text,id){
	return{
		type: CANCEL_EDIT_ROL,
		text: text,
		id:id
	}
}

export function fetchRequestRoleSucceeded(roles){
	return{ 
		type: FETCH_REQUEST_ROLES_SUCCEEDED,
		roles: roles
	}
}
export function fetchRoles(){
	
	return dispatch => {
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"roles",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(fetchRequestRoleSucceeded(json.roles))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar los roles')
		})
		
	}
	
}

export function fetchCreateRole(role){
	let newRole = {
			name : role.name,
			description: role.description,
			status: role.status      
	}
	return dispatch => {
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"roles",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		body: JSON.stringify(newRole)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(saveNewRol(json.role.id,role.id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

export function fetchDeleteRole(id){
	return dispatch => {
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"roles/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("Rol eliminado exitosamente")
				dispatch(deleteRol(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		})
		
	}
}

export function fetchUpdateRole(role){
	let role_to_update = {
			name : role.name,
			description: role.description,
			status: role.status      
	}
	return dispatch => {
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"roles/"+role.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		body: JSON.stringify(role_to_update)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateRol(role))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la modificación')
		})
		
	}
}