import history from '../history'

export const REQUEST_FOLLOW_CATEGORY_POSTS_SUCCEEDED = 'REQUEST_FOLLOW_CATEGORY_POSTS_SUCCEEDED'
export const REQUEST_POSTS_LIKED_SUCCEEDED = 'REQUEST_POSTS_LIKED_SUCCEEDED'
export const REQUEST_HOME_POSTS_SUCCEEDED = 'REQUEST_HOME_POSTS_SUCCEEDED:'
export const HANDLE_POST_INPUT_CHANGE = 'HANDLE_POST_INPUT_CHANGE'
export const REMOVE_LIKE_POST = 'REMOVE_LIKE_POST'
export const LIKE_POST = 'LIKE_POST'
export const REQUEST_MY_SALES_SUCCEEDED = 'REQUEST_MY_SALES_SUCCEEDED'
export const REQUEST_MY_PURCHASES_SUCCEEDED = 'REQUEST_MY_PURCHASES_SUCCEEDED'
export const SHOW_POST = 'SHOW_POST'
export const HANDLE_POST_TO_PUCHASE_INPUT_CHANGE = 'HANDLE_POST_TO_PUCHASE_INPUT_CHANGE'
export const CANCEL_POST_TO_PURCHASE = 'CANCEL_POST_TO_PURCHASE'
export const RESET_FETCH = 'RESET_FETCH'
export const IS_FETCHING = 'IS_FETCHING'
export const FETCH_SEND_REQUIREMENTS_SUCCEEDED='FETCH_SEND_REQUIREMENTS_SUCCEEDED'
export const CANCEL_PURCHASE_ORDER= 'CANCEL_PURCHASE_ORDER'
export const ACCEPT_SALE_ORDER = 'ACCEPT_PURCHASE_ORDER'
export const REJECT_SALE_ORDER = 'REJECT_PURCHASE_ORDER'
export const FINISH_SALE_ORDER = 'REJECT_PURCHASE_ORDER'
export const DELETE_POST_PICTURES = 'DELETE_POST_PICTURES'
export const CANCEL_POST_POST = "CANCEL_POST_POST"
export const REQUEST_POST_POST_SUCCEDED = 'REQUEST_POST_POST_SUCCEDED'
export const REQUEST_POST_POST_FAILED = 'REQUEST_POST_POST_FAILED'
export const REQUEST_MY_POSTS_SUCCEEDED = 'REQUEST_MY_POSTS_SUCCEEDED'
export const CHANGE_ORDER_STATUS = 'CHANGE_ORDER_STATUS'
export const FETCH_POSTS_SUCCEEDED = 'FETCH_POSTS_SUCCEEDED'

export function fetchPostsSucceeded(posts){
	return{
		type: FETCH_POSTS_SUCCEEDED,
		posts
	}
}


export function cancelPurchaseOrder(order){
    return {
        type: CANCEL_PURCHASE_ORDER,
        order
    }
}

export function changeOrderStatus(order,type_action){
    return{
        type: CHANGE_ORDER_STATUS,
        order,
        type_action
    }
}

export function acceptSaleOrder(order){
    return {
        type: ACCEPT_SALE_ORDER,
        order
    }
}

export function rejectSaleOrder(order){
    return {
        type: REJECT_SALE_ORDER,
        order
    }
}

export function finishSaleOrder(order){
    return {
        type: FINISH_SALE_ORDER,
        order
    }
}

export function resetFetchPost(){
    return{
        type: RESET_FETCH
    }
}

export function isFetching(){
    return{
        type: IS_FETCHING
    }
}

export function requestFollowCategoryPostsSucceeded(posts){
    return{
        type: REQUEST_FOLLOW_CATEGORY_POSTS_SUCCEEDED,
        posts
    }
}

export function handlePostToPurchaseInputChange(event){
    return{
        type: HANDLE_POST_TO_PUCHASE_INPUT_CHANGE,
        event
    }
}

export function cancelPostToPurchase(){
    return{
        type:CANCEL_POST_TO_PURCHASE
    }
}

export function requestPostsLikedSucceeded(posts){
    return{
        type: REQUEST_POSTS_LIKED_SUCCEEDED,
        posts
    }
}

export function requestMySalesSucceeded(sales){
    return{ 
        type: REQUEST_MY_SALES_SUCCEEDED,
        sales
    }
}

export function showPost(post){
    return{
        type: SHOW_POST,
        post
    }
}

export function requestMyPurchasesSucceeded(purchases){
    return {
        type: REQUEST_MY_PURCHASES_SUCCEEDED,
        purchases
    }
}

export function likePost(post){
    return{
        type: LIKE_POST,
        post
    }
}

export function removeLikePost(post){
    return{
        type: REMOVE_LIKE_POST,
        post
    }
}

export function fetchSendRequirementsSucceeded(new_purchase){
    return {
        type: FETCH_SEND_REQUIREMENTS_SUCCEEDED,
        new_purchase
    }
}

export function onPostHandleInputChange(event){
    return {
        type: HANDLE_POST_INPUT_CHANGE,
        event
    }
}

export function deletePostPictures(index){
    return {
        type: DELETE_POST_PICTURES,
        index
    }
}

export function cancelPostPost(){
    return {
        type: CANCEL_POST_POST
    }
}
export function requestPostPostSucceded(post){
    return {
      type: REQUEST_POST_POST_SUCCEDED,
      post
    }
  }
  
export function requestPostPostFailed(){
    return {
      type: REQUEST_POST_POST_FAILED
    }
  }

export function requestMyPostsSucceeded(posts){
	return{
		type: REQUEST_MY_POSTS_SUCCEEDED,
		posts
	}
}


export function fetchSendRequirements(post,requirements,action){
    return function (dispatch,getState) {	
		let new_purchase = {
			buyer: getState().main.userData.id,
            post: post.id,
            action: action.id,
            requirements: requirements

        }
        console.log(new_purchase)
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"purchase",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(new_purchase)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
                return response.json().then(err => { throw err; });
            if (response.status === 500)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
                console.log(json)
                if(json.status == 200){
                    dispatch(fetchSendRequirementsSucceeded(json.purchase[0]))
                }
			}		
		}
		).catch((errors) => {
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			console.log('No se pudo completar la compra')
		})
		
	}
}

export function fetchMyPosts(){
	return function (dispatch,getState) {	
		let json = {
			user: getState().main.userData.id,
		}
        const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"posts/user",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(json)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
            }
            if (response.status === 400)
				return response.json().then(err => { throw err; });
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 500)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
                console.log(json)
                if(json.status==200){
					dispatch(requestMyPostsSucceeded(json.posts))
				}
				else if(json.status==401){
					dispatch(requestMyPostsSucceeded([]))
				}
				
			}		
		}
		).catch((errors) => {
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			console.log('No se pudieron cargar mis posts')
		})
		
	}
}

export function fetchMySales(){
    //estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada
	return function (dispatch,getState) {	
		let json = {
			user: getState().main.userData.id,
		}
        const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"purchase/seller_purchases",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(json)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
            }
            if (response.status === 400)
				return response.json().then(err => { throw err; });
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
                console.log(json)
                if(json.status==200)
                    dispatch(requestMySalesSucceeded(json.purchases))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar mis ventas')
		})
		
	}
}

export function fetchMyPurchases(){
    //estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada
    return function (dispatch,getState) {	
		let buyer_purchases = {
			user: getState().main.userData.id,
		}
		const token = getState().main.globalData.token;
        return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"purchase/buyer_purchases",{
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        //credentials: "with-credentials", // include, same-origin, *omit
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            // "Content-Type": "application/x-www-form-urlencoded",
            //"X-Email": email,
            //"X-Token": token
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(buyer_purchases)
        })
        .then(
            response => {
            if (response.ok) {
                return response.json()
            }
            if (response.status === 422)
                return response.json().then(err => { throw err; });
            if (response.status === 500)
                return response.json().then(err => { throw err; });
            
            },
            error => console.log('An error occurred.', error)
        )
        .then(json => {
            if (json) {
                console.log(json)
                if(json.status==200){
                    dispatch(requestMyPurchasesSucceeded(json.purchases))
                }else
                if(json.status==401){
                    dispatch(requestMyPurchasesSucceeded([]))
                }         
            }		
        }
        ).catch((errors) => {
            //dispatch(requestAddCountryFailed(errors))
            let message=""
            for(var prop in errors) {
            message += prop + ": " + errors[prop] + ", "
            console.log(errors)
            }
            //dispatch(addErrorMessages(message, "danger"))
            console.log('No se pudo completar la carga de mis ordenes de compra')
        })
        
    }
}

export function fetchPostsLiked(){
	return function (dispatch,getState) {	
		let json = {
			user_id: getState().main.userData.id,
		}
        const token = getState().main.globalData.token;
        
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"action_post/liked_posts",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
            //"X-Token": token
            'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(json)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
            }
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 400)
                return response.json().then(err => { throw err; });
            if (response.status === 500)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
                console.log(json)
                if(json.status==200){
                    dispatch(requestPostsLikedSucceeded(json.posts))
                }
                else{
                    if(json.status==401){
                        dispatch(requestPostsLikedSucceeded([]))
                        console.log('No se pudieron cargar los posts gustados')
                    }
 
                }
			}		
		}
		).catch((errors) => {
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			console.log('Error: No se pudieron cargar los posts gustados')
		})
		
	}
}

export function fetchFollowCategoryPosts(){
	console.log("categorias seguidas")
	return function (dispatch,getState) {	
		let json = {
			user_id: getState().main.userData.id,
		}
        const token = getState().main.globalData.token;
        //dispatch(requestFollowCategoryPostsSucceeded(postJson.posts))
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"user_category/timeline",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(json)
		})
		.then(
			response => {
				console.log(response)
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 500)
				return response.json().then(err => { throw err; });
			if (response.status === 400)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			console.log(json)
			if (json) {
                
                if(json.status==200){
                    dispatch(requestFollowCategoryPostsSucceeded(json.posts))
                }
			}		
		}
		).catch((errors) => {
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			console.log('No se pudo completar la carga de post en timeline')
		})
		
	}
}

export function fetchCategoryPosts(category){
    return dispatch => {
        /*
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"/register",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		body: JSON.stringify(newUser)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})*/
		
	}
}

export function fetchSaveNewPost(post){
	return function (dispatch,getState) {
    let newPost= {title: post.title,
        price: post.price,
        user_id: getState().main.userData.id,
        category_id: post.category_id,
        description: post.description,
        conditions: post.conditions
    }
    const dataWithFile = new FormData();

    dataWithFile.append('post', JSON.stringify(newPost));

    for (var i=0; i < post.pictures.length; i++) {
        console.log(post.pictures[i])
        dataWithFile.append('image'+i, post.pictures[i])
    }

		const token = getState().main.globalData.token;
    const email = getState().main.userData.email;

		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"posts",{
				method: "POST",
				mode: "cors",
				headers: {
          'Authorization': 'Bearer ' + token
				},
				body: dataWithFile
			})
  		.then(			
  			response => {
  			if (response.ok){
  				return response.json()
  			}	
  			if (response.status === 422)
  				return response.json().then(err => { throw err; });
    		},
  			error => console.log('An error occurred.', error)
			)
			.then(json => {		
        console.log(json)
				if (json != null) {
          if(json.status && json.status != 500){
            dispatch(requestPostPostSucceded(json.post))
            history.push("/detail-publication")
            console.log(json.message)
            window.location.reload(true)
          }else{
            console.log(json.message)
          }
				}else{										
          dispatch(requestPostPostFailed())
          console.log('No se pudo guardar el post ')
        }			
			}
			).catch((errors) => {
		    console.log("errors:")
		    console.log(errors)
        let message=""
        for(var prop in errors) {
          message += prop + ": " + errors[prop] + ", "               
  			}
		    console.log("Message:" + message)
        console.log('Error: No se pudo guardar el post')
      })
	}
}

export function fetchLikePost(post,action){
	return function (dispatch,getState) {
		let json = {
            post: post.id,
            action: action.id,
            user_id: getState().main.userData.id
		}	
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"action_post/like",{
					method: "POST",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
                        'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
				}	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 401)
					return response.json().then(err => { throw err; });
				if (response.status === 500)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json) {
					console.log(json)
                    if(json.status==200){
                        dispatch(likePost(post))
                    }        		
				}		
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo dar Me gusta al post')
		      })
	}
}

export function fetchRemoveLikePost(post,action){
	return function (dispatch,getState) {
		let json = {
            post: post.id,
            action: action.id,
            user_id: getState().main.userData.id
		}	
		const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"action_post/remove_like",{
					method: "PUT",
					mode: "cors",
					headers: {
						"Content-Type": "application/json; charset=utf-8",
                        'Authorization': 'Bearer ' + token
					},
					body: JSON.stringify(json)
			})
			.then(			
				response => {
				if (response.ok){
					return response.json()
                }
                if (response.status === 404)
				    return response.json().then(err => { throw err; });	
				if (response.status === 422)
                    return response.json().then(err => { throw err; });
                if (response.status === 401)
					return response.json().then(err => { throw err; });
				if (response.status === 500)
				    return response.json().then(err => { throw err; });
			},

				error => console.log('An error occurred.', error)
			)
			.then(json => {				
				if (json) {
					console.log(json)
                    if(json.status==200){
                        dispatch(removeLikePost(post))
                    }        		
				}		
			}
			).catch((errors) => {
				console.log("errors:")
				console.log(errors)
                let message=""
                for(var prop in errors) {
                message += prop + ": " + errors[prop] + ", "               
				}
				console.log("Message:" + message)
                console.log('Error: No se pudo remover el like al post')
		      })
	}
}

