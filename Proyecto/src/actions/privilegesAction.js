export const HANDLE_PRIVILEGE_NAME_INPUT_CHANGE = 'HANDLE_PRIVILEGE_NAME_INPUT_CHANGE'
export const HANDLE_PRIVILEGE_DESCRIPTION_INPUT_CHANGE = 'HANDLE_PRIVILEGE_DESCRIPTION_INPUT_CHANGE'
export const ADD_NEW_PRIVILEGE = 'ADD_NEW_PRIVILEGE'
export const DELETE_PRIVILEGE = 'DELETE_PRIVILEGE'
export const EDIT_PRIVILEGE = 'EDITING_PRIVILEGE'
export const CANCEL_EDIT_PRIVILEGE = 'CANCEL_EDIT_PRIVILEGE'
export const HANDLE_PRIVILEGE_STATUS_INPUT_CHANGE = 'HANDLE_PRIVILEGE_STATUS_INPUT_CHANGE'
export const FETCH_REQUEST_CATEGORIES_SUCCEEDED = 'FETCH_REQUEST_CATEGORIES_SUCCEEDED'
export const SAVE_NEW_PRIVILEGE = 'SAVE_NEW_PRIVILEGE'
export const UPDATE_PRIVILEGE = 'UPDATE_PRIVILEGE'
export const HANDLE_PRIVILEGE_INPUT_CHANGE = 'HANDLE_PRIVILEGE_INPUT_CHANGE'


export function handlePrivilegeInputChange(event,id){
	return{
		type:HANDLE_PRIVILEGE_INPUT_CHANGE,
        event,
        id
	}
}

export function updatePrivilege(privilege){
	return{
		type: UPDATE_PRIVILEGE,
		privilege: privilege
	}
}

export function saveNewPrivilege(privilege){
	return{
		type: SAVE_NEW_PRIVILEGE,
		privilege: privilege
	}
}

export function addNewPrivilege(){
	return {
		type: ADD_NEW_PRIVILEGE,
	}
}

export function deletePrivilege(id){
	return {
		type: DELETE_PRIVILEGE,
		id: id
	}
}

export function editPrivilege(id){
	return {
		type: EDIT_PRIVILEGE,
		id: id
	}
}

export function cancelEditPrivilege(id){
	return{
		type: CANCEL_EDIT_PRIVILEGE,
		id:id
	}
}

export function fetchRequestPrivilegesSucceeded(privileges){
	return{ 
		type: FETCH_REQUEST_CATEGORIES_SUCCEEDED,
		privileges: privileges
	}
}

export function fetchPrivileges(){
let privilegesJSON = {
	privileges: [
		{
			id: 1,
			name: "Ilustración",
			description: "Esta es un dibujo usando técnicas bien definidas", 
			status: 1
		},
		{
			id: 2,
			name: "Dibujo animado",
			description: "Esta es un dibujo usando técnicas bien definidas animadas",
			status: 2
		},
		{
			id: 3,
			name: "Música",
			description: "Audio realizado para cualquier fin", 
			status: 1
		}
	]}
	return dispatch => {
		dispatch(fetchRequestPrivilegesSucceeded(privilegesJSON.privileges))
		/*return fetch(process.env.REACT_VERONICA_URL_WEB+"countries",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			console.log(json)
			if (json) {
				console.log(json)	
			}			
			else{
				console.log("no sirvio")							
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddPrivilegeFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})*/
		
	}
}

export function fetchCreatePrivilege(){

	return dispatch => {

		return fetch("/player.json",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			console.log(json)
			if (json) {
			if(json.email){			
				//dispatch(restoreUserState())
				//dispatch(requestRegisterNewUserSucceded(json))				
			}
			else{
				console.log("no sirvio")							
			}
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddPrivilegeFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}

