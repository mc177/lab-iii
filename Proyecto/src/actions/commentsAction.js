import history from '../history'
import {updateOrderStatus} from './mainAppAction'

export const FETCH_COMMENTS_USER_SUCCEEDED = 'FETCH_COMMENTS_USER_SUCCEEDED'
export const HANDLE_COMMENT_INPUT = 'HANDLE_COMMENT_INPUT'
export const HANDLE_SCORE = 'HANDLE_SCORE'
export const REQUEST_PUBLISH_COMMENT_SUCCEDED = 'REQUEST_PUBLISH_COMMENT_SUCCEDED'
export const REQUEST_PUBLISH_COMMENT_FAILED = 'REQUEST_PUBLISH_COMMENT_FAILED'

export function handleCommentInput(event){
    return{
        type: HANDLE_COMMENT_INPUT,
        event
    }
}

export function fetchCommentsUserSucceeded(comments){
	return{
		type: FETCH_COMMENTS_USER_SUCCEEDED,
		comments
	}
}

export function handleScore(score){
    return{
        type: HANDLE_SCORE,
        score
    }
}

function requestPublishCommentSucceded(comment){
	return {
		type: REQUEST_PUBLISH_COMMENT_SUCCEDED,
		comment
	}
}

function requestPublishCommentFailed(){
	return {
		type: REQUEST_PUBLISH_COMMENT_FAILED
	}
}


export function publishComment(order, comment_param){

	return function(dispatch, getState){

		let comment = {
			purchase: order.id,
			message: comment_param.message,
			score: comment_param.purchase.score
		}

		const token = getState().main.globalData.token

		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"comment/publish",{
				method: "POST", // *GET, POST, PUT, DELETE, etc.
				mode: "cors", // no-cors, cors, *same-origin
				cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
				//credentials: "with-credentials", // include, same-origin, *omit
				headers: {
					"Content-Type": "application/json; charset=utf-8",
					'Authorization': 'Bearer ' + token
				},
				body: JSON.stringify(comment)
			})
			.then(
				response => {
					if (response.ok) {
						return response.json()
					}
					if (response.status === 422)
						return response.json().then(err => { throw err; });
					
				},
				error => console.log('An error occurred.', error)
			)
			.then(json => {
					console.log(json)
					if (json && json.status != 500) {
						dispatch(requestPublishCommentSucceded(comment));
						dispatch(updateOrderStatus(6))
						history.push('/detail-order');
						window.location.reload(true);
					}else{
						dispatch(requestPublishCommentFailed());
					}
			}
			).catch((errors) => {
				console.log('No se pudo completar el registro')
			})
	}

}

export function fetchCommentsUser(){

	return (dispatch, getState) => {	
		let user = {
			user: getState().main.userData.id
		}

		const token = getState().main.globalData.token

		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"comment",{
			method: "POST", // *GET, POST, PUT, DELETE, etc.
			mode: "cors", // no-cors, cors, *same-origin
			cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			//credentials: "with-credentials", // include, same-origin, *omit
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				'Authorization': 'Bearer ' + token
			},
			body: JSON.stringify(user)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
				console.log(json)
			if (json && json.status != 500) {
				dispatch(fetchCommentsUserSucceeded(json.comments))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
	}
}