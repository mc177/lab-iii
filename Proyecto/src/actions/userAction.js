import {fetchPostsSucceeded} from './postAction'

export const HANDLE_EMAIL_INPUT_CHANGE = 'HANDLE_EMAIL_INPUT_CHANGE'
export const FETCH_USERS_SUCCEEDED = 'FETCH_USERS_SUCCEEDED'
export const SHOW_USER = 'SHOW_USER'
export const EDIT_USER = 'EDIT_USER'
export const CANCEL_EDIT_USER = 'CANCEL_EDIT_USER'
export const RESTORE_USER_DATA = 'RESTORE_USER_DATA'
export const RESTORE_USER = 'RESTORE_USER'
export const DELETE_USER = 'DELETE_USER'
export const HANDLE_USER_INPUT_CHANGE = 'HANDLE_USER_INPUT_CHANGE'
export const HANDLE_USER_EMAIL_INPUT_CHANGE = 'HANDLE_USER_EMAIL_INPUT_CHANGE'
export const HANDLE_USER_PASSWORD_INPUT_CHANGE = 'HANDLE_USER_PASSWORD_INPUT_CHANGE'
export const HANDLE_USER_CONFIRM_PASSWORD_INPUT_CHANGE = 'HANDLE_USER_CONFIRM_PASSWORD_INPUT_CHANGE'
export const HANDLE_USER_USERNAME_INPUT_CHANGE = 'HANDLE_USER_USERNAME_INPUT_CHANGE'
export const HANDLE_USER_ROLE_INPUT_CHANGE = 'HANDLE_USER_ROLE_INPUT_CHANGE'
export const FETCH_REQUEST_USERS_SUCCEEDED = 'FETCH_REQUEST_USERS_SUCCEEDED'
export const UPDATE_USER = 'UPDATE_USER'
export const SAVE_NEW_USER= 'SAVE_NEW_USER'
export const HANDLE_USER_ROLE_CHANGE ='HANDLE_USER_ROLE_CHANGE'
export const HANDLE_USER_COUNTRY_CHANGE ='HANDLE_USER_COUNTRY_CHANGE'
export const SHOW_PUBLIC_USER_PROFILE_SUCCEEDED = 'SHOW_PUBLIC_USER_PROFILE_SUCCEEDED'

export function showPublicUserProfileSucceeded(user){
	return{
		type: SHOW_PUBLIC_USER_PROFILE_SUCCEEDED,
		user
	}
}

export function handleUserInputChange(event){
	return{
		type:HANDLE_USER_INPUT_CHANGE,
		event
	}
}
export function handleUserRoleChange(event){
	return{
		type:HANDLE_USER_ROLE_CHANGE,
		event
	}
}
export function handleUserCountryChange(event){
	return{
		type:HANDLE_USER_COUNTRY_CHANGE,
		event
	}
}

export function fetchUsersSucceeded(users){
	return{
		type: FETCH_USERS_SUCCEEDED,
		users:users
	}
}

export function showUser(user){
	return{
		type: SHOW_USER,
		user
	}
}

export function editUser(user){
	return{
		type: EDIT_USER,
		user
	}
}

export function updateUser(user){
	return{
		type: UPDATE_USER,
		user
	}
}

export function saveNewUser(user,id){
	return{
		type: SAVE_NEW_USER,
		user: user,
		new_user_id: id
	}
}

export function restoreUser(){
	return{
		type: RESTORE_USER
	}
}

export function deleteUser(id){
	return{
		type: DELETE_USER,
		id
	}
}


export function restoreUserData(){
	return{
		type:RESTORE_USER_DATA
	}
}

export function fetchRequestUsersSucceeded(users){
	return{ 
		type: FETCH_REQUEST_USERS_SUCCEEDED,
		users: users
	}
}

export function fetchUsers(){
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"users",{
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log(json)
				console.log('Se pudieron cargar los usuarios')
				dispatch(fetchRequestUsersSucceeded(json.users))
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudieron cargar los usuarios')
		})
		
	}
}

export function fetchDeleteUser(id){
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"users/"+id,{
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				console.log("Usuario eliminado exitosamente")
				dispatch(deleteUser(id))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar la eliminación')
		})
		
	}
}

 
export function fetchCreateUser(user){
		
		
	let newUser = {
			email : user.email,      
			password : user.password,
			passwordConfirm: user.confirmPassword,
			//telephone: "555",
			country: user.country_id,
			role: user.role_id,			
			/*username : "123",
			name: "Moises",
			surname: "Cam",
			birthday: "2019-10-12",*/
			//gender:1,
			
		}
		
	return function (dispatch,getState) {
		const token = getState().main.globalData.token		
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"users",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(newUser), // body data type must match "Content-Type" header
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {			
			if (json) {
			if(json.email){			
				dispatch(saveNewUser(json.user,user.id))
								
			}
			else{
				//dispatch(requestRegisterNewUserFailed(json))							
			}
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log('No se pudo completar el registro')
		})
		
	}
}
export function fetchUpdateUser(user){
	let update_User = {
		email : user.email,      
		password : "123456789",
		passwordConfirm: "123456789",
		telephone: "555",
		country: user.user_profile.country.id,
		role: user.role.id,			
		username : "123",
		name: "Moises",
		surname: "Cam",
		birthday: "2019-10-12",
		gender:1,
		
	}
	return function (dispatch,getState) {
		const token = getState().main.globalData.token
		return fetch(process.env.REACT_APP_VERONICA_URL_SERVER+"users/"+user.id,{
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
			'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(update_User)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });

			if (response.status === 500)
				return response.json().then(err => { throw err; });

			if (response.status === 403)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
				dispatch(updateUser(user))
			}		
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			//dispatch(addErrorMessages(message, "danger"))
			console.log(errors)
			console.log('No se pudo completar la modificación')
		})
		
	}
}

export function fetchShowPublicUserProfile(user){
	return function (dispatch,getState) {	
		let json = {
			user: user.id,
		}
        const token = getState().main.globalData.token;
		return fetch(process.env.REACT_APP_VERONICA_URL_WEB+"posts/user",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, cors, *same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
            'Authorization': 'Bearer ' + token
		},
		body: JSON.stringify(json)
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
            }
            if (response.status === 400)
				return response.json().then(err => { throw err; });
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			if (response.status === 500)
				return response.json().then(err => { throw err; });
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {
                console.log(json)
                if(json.status==200){
					dispatch(showPublicUserProfileSucceeded(json.posts[0].user))
					dispatch(fetchPostsSucceeded(json.posts))
				}
				else if(json.status==401){
					console.log("no se pudo carga el perfil")
				}
				
			}		
		}
		).catch((errors) => {
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			console.log(errors)
			}
			console.log('No se pudieron cargar los post del perfil público')
		})
		
	}
}

/*export function fetchAuthenticate(user_data){
	
	email = user_data.email
	password = user_data.password
	user = {
		email : email,      
		password : password,	
	}
	return function (dispatch){
		dispatch(requestInProcess())
		dispatch(emailToLowerCase())
		return fetch("/authenticate.json",{
			method: "POST", // *GET, POST, PUT, DELETE, etc.
			//mode: "cors", // no-cors, cors, *same-origin
			//cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
			//credentials: "with-credentials", // include, same-origin, *omit
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				// "Content-Type": "application/x-www-form-urlencoded",
				"X-Email": 'email',
				"X-Token": 'token'
			},
			body: JSON.stringify(user), // body data type must match "Content-Type" header
			})
			.then(
			response => {
				if (response.ok) {
				return response.json()
				}
				if (response.status === 422)
				return response.json().then(err => { throw err; });
				
			},
			error => console.log('An error occurred.', error)
			)
			.then(json => {
			if (json) {
				console.log(json)
				if(json.token){
					dispatch(requestLoginUserSucceded(json))
				}
				else{
					dispatch(requestLoginUserFailed(json))							
				}	
			}
			
			}
			).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
				message += prop + ": " + errors[prop] + ", "
				
			}
			//dispatch(addErrorMessages(message, "danger"))
			})
	
	}
}


export function fetchRecoverPassword(user_data){
	email = user_data.email
	user = {
		email : email,      
	}
	//return function (dispatch){
	return dispatch => {
		dispatch(requestInProcess())
		dispatch(emailToLowerCase())
		return fetch("/user/recoverPassword.json",{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		//mode: "cors", // no-cors, cors, *same-origin
		//cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		//credentials: "with-credentials", // include, same-origin, *omit
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			// "Content-Type": "application/x-www-form-urlencoded",
			//"X-Email": email,
			//"X-Token": token
		},
		body: JSON.stringify(user), // body data type must match "Content-Type" header
		})
		.then(
			response => {
			if (response.ok) {
				return response.json()
			}
			if (response.status === 422)
				return response.json().then(err => { throw err; });
			
			},
			error => console.log('An error occurred.', error)
		)
		.then(json => {
			if (json) {		
			if(json.status == "error"){
				dispatch(requestRecoverPasswordFailed(json))
			}
				
			else{
				dispatch(restoreUserState())
				dispatch(requestRecoverPasswordSucceded(json))
			}
				
			}
			
		}
		).catch((errors) => {
			//dispatch(requestAddCountryFailed(errors))
			let message=""
			for(var prop in errors) {
			message += prop + ": " + errors[prop] + ", "
			
			}

		})
		
	}
}

export function fetchEditUserProfile(){
	return function (dispatch,getState) {
	    const token = getState().mainApp.userData.token;
	    const email = getState().mainApp.userData.email;

	    return fetch("/user/editProfile.json",{
	      method: "GET", 
	      mode: "cors", 
	        headers: {
	            "Content-Type": "application/json; charset=utf-8",
	            "X-Email": email,
	            "X-Token": token
	        },
	    })
	      .then(
	        response => {
	          if (response.ok) {
	            return response.json()
	          }
	          if (response.status == 422)
	            return response.json().then(err => { throw err; });
	        },
	        error => {
			  console.log("no se pudieron cargar los estados")
	        }
	      )
	      .then(json =>{
	        if (json.errorCode != 401) {			
			  dispatch(requestSportsSucceded(json.sports))
			  dispatch(requestSexesSucceded(json.sexes))		  
			  dispatch(requestCountriesSucceded(json.countries))
			  dispatch(setUserProfile(json.user_profile))
	        }else{
	          if (json.errorCode == 401){
				console.log("Error 401")
	          }
	        }
	      }
	      ).catch((errors) => {
	         let message=""
	        for(var prop in errors) {
	          message += prop + ": " + errors[prop] + ", " 
	        }
			console.log("mensaje: "+message)        
	      })
 	}
}*/