import React from 'react';
import App from './App';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import makeStore from './store/makeStore'
import { PersistGate } from 'redux-persist/integration/react'

const { store, persistor } = makeStore();

const unsuscribe = store.subscribe(() => {
  console.log(store.getState())
})

render(
	<Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);