import { STATE_LOGIN, STATE_SIGNUP, RECOVER_PASSWORD } from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout, MainLayoutAdmin } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
import AuthPage from './containers/main/authFormContainer';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './styles/reduction.scss';
import {connect} from 'react-redux'
import {cancelPostPost} from './actions/postAction'

import Hammer from 'hammerjs'
import * as $ from 'jquery'
const AlertPage = React.lazy(() => import('pages/AlertPage'));
const AuthModalPage = React.lazy(() => import('pages/AuthModalPage'));
const BadgePage = React.lazy(() => import('pages/BadgePage'));
const ButtonGroupPage = React.lazy(() => import('pages/ButtonGroupPage'));
const ButtonPage = React.lazy(() => import('pages/ButtonPage'));
const CardPage = React.lazy(() => import('pages/CardPage'));
const ChartPage = React.lazy(() => import('pages/ChartPage'));
const DashboardPage = React.lazy(() => import('pages/DashboardPage'));
const DropdownPage = React.lazy(() => import('pages/DropdownPage'));
const FormPage = React.lazy(() => import('pages/FormPage'));
const InputGroupPage = React.lazy(() => import('pages/InputGroupPage'));
const ModalPage = React.lazy(() => import('pages/ModalPage'));
const ProgressPage = React.lazy(() => import('pages/ProgressPage'));
const TablePage = React.lazy(() => import('pages/TablePage'));
const TypographyPage = React.lazy(() => import('pages/TypographyPage'));
const WidgetPage = React.lazy(() => import('pages/WidgetPage'));

const MySales = React.lazy(() => import('components/Main/MySales'));
const MyLikes = React.lazy(() => import('pages/MyLikes'));
const MyPublications = React.lazy(() => import('pages/MyPublications'));
const PrivilegeList = React.lazy(() => import('components/Privileges/PrivilegeList'));

const CategoryRanking = React.lazy(() => import('containers/main/categoryRankingContainer'));
const EarningsAdmin = React.lazy(() => import('containers/main/earningsAdminContainer'));
const EarningsUser = React.lazy(() => import('containers/main/earningsUserContainer'));
const DetailEarningUser = React.lazy(() => import('containers/main/detailEarningUserContainer'));
const PublicProfile = React.lazy(() => import('containers/users/publicProfileContainer'));
const Profile = React.lazy(() => import('containers/main/profileContainer'));
const SearchPosts = React.lazy(() => import('containers/posts/searchPostsContainer'));
const NewPost = React.lazy(() => import('containers/posts/newPostContainer'));
const FinishOrder = React.lazy(() => import('containers/main/finishOrderContainer'));
const UserQualification = React.lazy(() => import('containers/comments/userQualificationContainer'));
const UserProfile = React.lazy(() => import('containers/main/userProfileContainer'));
const Comments = React.lazy(() => import('containers/comments/commentsContainer'));
const RequirementSeller = React.lazy(() => import('containers/main/requirementSellerContainer'));
const CancelOrder = React.lazy(() => import('containers/main/cancelOrderContainer'));
const DetailOrder = React.lazy(() => import('containers/main/detailOrderContainer'));
const MyPurchases = React.lazy(() => import('containers/main/myPurchasesContainer'));
const RequirementBuyer = React.lazy(() => import('containers/posts/requirementBuyerContainer'));
const DetailPublication = React.lazy(() => import('containers/posts/detailPostContainer'));
const PurchaseOrders = React.lazy(() => import('containers/main/purchaseOrdersContainer'));
const MyLikedPosts = React.lazy(() => import('containers/posts/myLikedPostsContainer'));
const HomePage = React.lazy(() => import('containers/posts/timeLineContainer'));
const CountryList = React.lazy(() => import('containers/countries/countryContainer'));
const CategoryList = React.lazy(() => import('containers/categories/categoriesContainer'));
const RoleList = React.lazy(() => import('containers/roles/rolContainer'));
const ActionList = React.lazy(() => import('components/Actions/ActionList'));
const UserList = React.lazy(() => import('containers/users/userContainer'));
const AuthForm = React.lazy(() => import('containers/main/authFormContainer'));

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {


  /*componentWillMount(){
    let url = window.location.pathname.toString()
    let admin_routes = ['/countries','/categories', '/roles', '/privileges', '/actions', '/users']

    if(admin_routes.indexOf(url) == -1){
      var element = document.getElementById('root');
   
      var hammermanager = new Hammer(element);
      
      hammermanager.get('pan').set({ direction: Hammer.DIRECTION_ALL });     
       
      hammermanager.on('panright', function(e) {    
        document.getElementById('aside-custom').setAttribute('class','cr-sidebar cr-sidebar--open');
        //$('#bottom-menu').fadeOut()
      });

      hammermanager.on('panleft', function(e) {    
        document.getElementById('aside-custom').setAttribute('class','cr-sidebar');
       // $('#bottom-menu').fadeIn()
      });
    }


  }*/
  detRole = (roles,string) =>{
		let role_ = null
		roles.map((role)=>{
			if(role.name == string)
				role_ = role
    })
    return role_
    
    }

  render() {
    const {globalData,user,roles, onAddNewPost} = this.props
    return (
      <BrowserRouter basename={getBasename()}>
        <GAListener>
          {!globalData.isLogged?
          <Switch>
              <EmptyLayout>
                <React.Suspense fallback={<PageSpinner />}>
                  <Route exact path="/" component={AuthPage} />
                </React.Suspense>
              </EmptyLayout>
      </Switch>
:
      <Switch>
            {user.role.id==this.detRole(roles,"Administrador").id ?
            <MainLayoutAdmin user={user} breakpoint={this.props.breakpoint}>
              <React.Suspense fallback={<PageSpinner />}>
                <Route exact path="/userProfile" component={UserProfile} />
                <Route exact path="/users" render={() => <UserList />} />
                <Route exact path="/roles" render={() => <RoleList />} />
                <Route exact path="/countries" render={() => <CountryList />} />
                <Route exact path="/categories" render={() => <CategoryList />} />
                <Route exact path="/privileges" render={() => <PrivilegeList />} /> 
                <Route exact path="/earnings-admin" component={EarningsAdmin} />
                <Route exact path="/category-ranking" render={() => <CategoryRanking />} />         
              </React.Suspense>
            </MainLayoutAdmin>
            :     
            (
            <Switch>
              <MainLayout user={user} onAddNewPost={onAddNewPost} breakpoint={this.props.breakpoint}>   
                <React.Suspense fallback={<PageSpinner />}>
                  <Route exact path="/" component={HomePage} />
                  <Route exact path="/login-modal" component={AuthModalPage} />
                  <Route  path="/buttons" component={ButtonPage} />
                  <Route exact path="/cards" component={CardPage} />
                  <Route exact path="/widgets" component={WidgetPage} />
                  <Route exact path="/typography" component={TypographyPage} />
                  <Route exact path="/alerts" component={AlertPage} />
                  <Route exact path="/tables" component={TablePage} />
                  <Route exact path="/badges" component={BadgePage} />
                  
                  
                  <Route exact path="/publicUserProfile" component={PublicProfile} />
                  <Route exact path="/userProfile" component={UserProfile} />
                  <Route exact path="/profile" component={Profile} />
                  <Route exact path="/my-likes" component={MyLikes} />
                  <Route exact path="/comments" component={Comments} />
                  <Route exact path="/home" component={HomePage} />
                  <Route exact path="/newPost" component={NewPost} />
                  <Route exact path="/searchPosts" render={() => <SearchPosts />} />
                  <Route exact path="/earnings" component={EarningsUser} />
                  <Route exact path="/requirement-buyer" component={RequirementBuyer} />
                  <Route exact path="/requirement-seller" component={RequirementSeller} /> 
                  <Route exact path="/mySells" render={() => <MySales />} />
                  <Route exact path="/actions" render={() => <ActionList />} />
                  <Route exact path="/my-purchases" render={() => <MyPurchases />} />
                  <Route exact path="/my-liked-posts" render={() => <MyLikedPosts />} />
                  <Route exact path="/newUserQualification" render={() => <UserQualification />} />
                  <Route exact path="/purchase-orders" render={() => <PurchaseOrders />} />
                  <Route exact path="/category-ranking" render={() => <CategoryRanking />} />
                  <Route exact path="/detail-earning-user" render={() => <DetailEarningUser />} />
                  <Route exact path="/finish-order" render={() => <FinishOrder />} />
                  <Route exact path="/cancel-order" render={() => <CancelOrder />} />
                  <Route exact path="/detail-publication" render={() => <DetailPublication />} />
                  <Route exact path="/my-publications" render={() => <MyPublications />} />
                  <Route exact path="/detail-order" render={() => <DetailOrder />} />


                  <Route exact path="/button-groups" component={ButtonGroupPage} />
                  <Route exact path="/dropdowns" component={DropdownPage} />
                  <Route exact path="/progress" component={ProgressPage} />
                  <Route exact path="/modals" component={ModalPage} />
                  <Route exact path="/forms" component={FormPage} />
                  <Route exact path="/input-groups" component={InputGroupPage} />
                  <Route exact path="/charts" component={ChartPage} />
                </React.Suspense>
              </MainLayout>
            </Switch>)
            }
          </Switch>
          }
        </GAListener>
      </BrowserRouter>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

const mapStateToProps = state => {
  return {
    globalData: state.main.globalData,
    user: state.main.userData,
    userLogged: state.main.userLogged,
    roles: state.roles.roles
  }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddNewPost: () => {dispatch(cancelPostPost())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
