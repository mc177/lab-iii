import * as privilegesActions from '../actions/privilegesAction'

const initialState = {
    fetch: {
      is_fetching:"",
      message:"",
      result_succeeded:null
    },
    privileges:[],
    privilege:{
        id:"",
        name:"",
        description:"",
        status:"",
    },
  }
  
  const privilegesReducer = (state = initialState, action) => {
  
    switch(action.type) {

  
      case privilegesActions.HANDLE_PRIVILEGE_INPUT_CHANGE:
        return Object.assign({}, state, {
          privileges: state.privileges.map((privilege) => {
            if(privilege.id == action.id){
              return Object.assign({}, privilege, {
                [action.event.target.name]: action.event.target.value
              })
            }
            return privilege
          }),
        })

      case privilegesActions.UPDATE_PRIVILEGE:
        return Object.assign({},state,{
          privileges: state.privileges.map((privilege) => {
            if(privilege.id == action.privilege.id){
              return Object.assign({}, action.privilege, {
                isEditing: false
              })
            }
            return privilege
          }),
        })

      case privilegesActions.EDIT_PRIVILEGE:
        return Object.assign({}, state, {
          privileges: state.privileges.map((privilege) => {
            if(privilege.id == action.id){
              return Object.assign({}, privilege, {
                originalValues: Object.assign({},{
                    name:privilege.name,
                    description: privilege.description,
                    status: privilege.status
                }),
                isEditing:true
              })
            }
            return privilege
          }),
        })

      case privilegesActions.SAVE_NEW_PRIVILEGE:
        return Object.assign({},state,{
          privileges: state.privileges.map((privilege) => {
            if(privilege.id == action.privilege.id){
              return Object.assign({}, action.privilege, {
                isNew: false
              })
            }
            return privilege
          }),
        })

      case privilegesActions.CANCEL_EDIT_PRIVILEGE:
        return Object.assign({}, state, {
          privileges: state.privileges.map((privilege) => {
            if(privilege.id == action.id){
              return Object.assign({}, privilege, {
                name: privilege.originalValues.name,
                status: privilege.originalValues.status,
                description: privilege.originalValues.description,
                isEditing:false,
                originalValues:null,
              })
            }
            return privilege
          }),
        })

      case privilegesActions.ADD_NEW_PRIVILEGE:
         let newCategory = {
          id: Math.random(),
          name:"",
            description:"",
            status:"",
          status: 1, 
          isNew: true
        }
        return Object.assign({}, state, {
          privileges: [...state.privileges,newCategory],
        })

      case privilegesActions.DELETE_PRIVILEGE:
        return Object.assign({}, state,{
          privileges: state.privileges.filter(privilege => privilege.id !== action.id)
        })
      
      case privilegesActions.FETCH_REQUEST_CATEGORIES_SUCCEEDED:
        return Object.assign({}, state, {
          privileges: action.privileges,
        })
        
      default:
        return state
    }
  }
  
  export default privilegesReducer