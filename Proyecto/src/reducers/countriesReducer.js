import * as countriesActions from '../actions/countriesAction'

const initialState = {
    fetch: {
      is_fetching:"",
      message:"",
      result_succeeded:null
    },
    countries:[],
    country:{
        name:"",
        status:"",
    },
  }
  
  const countriesReducer = (state = initialState, action) => {
  
    switch(action.type) {
  
      case countriesActions.HANDLE_COUNTRY_NAME_INPUT_CHANGE:
        return Object.assign({}, state, {
          countries: state.countries.map((country) => {
            if(country.id == action.id){
              return Object.assign({}, country, {
                name:action.text
              })
            }
            return country
          }),
        })

      case countriesActions.HANDLE_STATUS_INPUT_CHANGE:
        return Object.assign({}, state, {
          countries: state.countries.map((country) => {
            if(country.id == action.id){
              return Object.assign({}, country, {
                status:action.text
              })
            }
            return country
          }),
        })

      case countriesActions.UPDATE_COUNTRY:
        return Object.assign({},state,{
          countries: state.countries.map((country) => {
            if(country.id == action.country.id){
              return Object.assign({}, action.country, {
                isEditing: false
              })
            }
            return country
          }),
        })

      case countriesActions.EDIT_COUNTRY:
        return Object.assign({}, state, {
          countries: state.countries.map((country) => {
            if(country.id == action.id){
              return Object.assign({}, country, {
                originalName:country.name,
                originalStatus: country.status,
                isEditing:true
              })
            }
            return country
          }),
        })

      case countriesActions.SAVE_NEW_COUNTRY:
        return Object.assign({},state,{
          countries: state.countries.map((country) => {
            if(country.id == action.new_country_id){
              return Object.assign({}, action.country, {
                isNew: false
              })
            }
            return country
          }),
        })
      case countriesActions.CANCEL_EDIT_COUNTRY:
        return Object.assign({}, state, {
          countries: state.countries.map((country) => {
            if(country.id == action.id){
              return Object.assign({}, country, {
                name: action.text,
                status: country.originalStatus,
                isEditing:false
              })
            }
            return country
          }),
        })

      case countriesActions.ADD_NEW_COUNTRY:
         let newCountry = {
          id: Math.random(),
          name: "",
          status: 1, 
          isNew: true
        }
        return Object.assign({}, state, {
          countries: [...state.countries,newCountry],
        })

      case countriesActions.DELETE_COUNTRY:
        //state.countries.filter(country => country.id !== action.id)
        return Object.assign({}, state, {
          countries: state.countries.map((country) => {
            if(country.id == action.id){
              return Object.assign({}, country, {
                status: 2
                })
            }
            return country
          }),
        })
      
      case countriesActions.FETCH_REQUEST_COUNTRIES_SUCCEEDED:
        return Object.assign({}, state, {
          countries: action.countries,
        })
        
      default:
        return state
    }
  }
  
  export default countriesReducer