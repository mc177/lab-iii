import * as mainAppActions from '../actions/mainAppAction'
import {dateParse} from '../components/Utils/dateParse'

const initialState = {
  fetch: {
    is_fetching:"",
    message:"",
    result_succeeded:null
  },
  userLogged: false,
  userData: {
    email: "",
    password:"",
    passwordConfirm:"",
    country_id:"",//caso login
    user_profile:{
      country_id:"",
      username:"",
      name:"",
      lastname: "",
      image: "",
      twitter: "",
      instagram: "",
      facebook: "",
      phone: "",
      sex: "",
      role: "",
      status: "",
      description: ""
    } 
  },
  globalData: {
    isLogged:false,
    roleUser:"",
    authState:"STATE_LOGIN", //STATE_LOGIN, STATE_SIGNUP , RECOVER_PASSWORD
    token:""
  },
  order: {
    prictures: [],
    prev_pictures: []
  },
  comments:[],
  followed_categories:[],
  change_password:{
    current_password:"",
    new_password:"",
    new_password_confirm:""
  },
  monthly_gains:[],
  month_gain:{},
  user_gains:[]
}

const mainReducer = (state = initialState, action) => {

  switch(action.type) {

    case mainAppActions.FETCH_USER_GAINS_SUCCEEDED:
      return Object.assign({},state, {
        user_gains: action.user_gains
      })

    case mainAppActions.FETCH_MONTHLY_GAINS_SUCCEEDED:
      return Object.assign({},state, {
        monthly_gains: action.monthly_gains  
      })

    case mainAppActions.RESET_MAIN_FETCH:
      return Object.assign({}, state, {
        fetch: Object.assign({}, state.fetch,{
          is_fetching:"",
          message:"",
          result_succeeded:null
        })
      })

    case mainAppActions.HANDLE_USER_INPUT_CHANGE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          [action.event.target.name]: action.event.target.value,
        })
      })

    case mainAppActions.HANDLE_CHANGE_PASSWORD_INPUT_CHANGE:
      return Object.assign({}, state, {
        change_password: Object.assign({}, state.change_password,{
          [action.event.target.name]: action.event.target.value,
        })
      })

    case mainAppActions.HANDLE_USER_PROFILE_INPUT_CHANGE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          user_profile_saved_values: Object.assign({}, state.userData.user_profile_saved_values,{
            [action.event.target.name]: action.event.target.value
          })
        })
      })
    case mainAppActions.UPDATE_USER_PROFILE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          user_profile: state.userData.user_profile_saved_values
        }),
      })
    
    case mainAppActions.HANDLE_AUTH_STATE_CHANGE:
      return Object.assign({},state, {
        globalData: Object.assign({}, state.globalData,{
          authState: action.text
        })  
      })

    case mainAppActions.AUTH_USER_SUCCEEDED:
      return Object.assign({}, state, {
        userData: Object.assign({}, action.user,{
          user_profile: Object.assign({}, action.user.user_profile, {
            birthday: dateParse(action.user.user_profile.birthday)
          }),
          user_profile_saved_values: Object.assign({},action.user.user_profile,{
            country_id: action.user.user_profile.country.id,
            birthday: dateParse(action.user.user_profile.birthday)
          }) 
        }),
        globalData: Object.assign({}, state.globalData,{
          isLogged:true,
          roleUser:action.user.role_id,
          token: action.token
        })
      })

    case mainAppActions.AUTH_USER_FAILED:
      return Object.assign({}, state, {
        fetch: Object.assign({},state.fetch,{
          is_fetching: false,
          message:action.message,
          result_succeeded: false
        })
      })

    case mainAppActions.RESET_CHANGE_PASSWORD:
      return Object.assign({}, state, {
        change_password:{
          current_password:"",
          new_password:"",
          new_password_confirm:""
        }
      })
    
    case mainAppActions.RESET_USER_DATA:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.user,{
          email: "",
          password:"",
          passwordConfirm:"",
          country_id:"",//caso login
          user_profile:{
            country_id:"",
            username:"",
            name:"",
            lastname: "",
            image: "",
            twitter: "",
            instagram: "",
            facebook: "",
            phone: "",
            sex: "",
            role: "",
            status: "",
            description: ""
          } 
        }),
        globalData: Object.assign({}, state.globalData,{
          isLogged:false,
          roleUser:"",
        }),
        change_password:{
          current_password:"",
          new_password:"",
          new_password_confirm:""
        }
      })
    
    case mainAppActions.SAVE_USER_PROFILE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          user_profile: state.userData.user_profile_saved_values
        }),
      })

    case mainAppActions.CANCEL_EDIT_PROFILE:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData,{
          user_profile_saved_values: Object.assign({},state.userData.user_profile,{
            country_id:state.userData.user_profile.country.id
          })
        }),
      })

    case mainAppActions.SHOW_ORDER:
        return Object.assign({},state,{
          order: Object.assign({}, action.order, {
            prev_pictures: []
          }) 
        })

    case mainAppActions.HANDLE_CANCEL_ORDER_DESCRIPTION:
        return Object.assign({},state,{
          order: Object.assign({},state.order,{
            [action.event.target.name]: action.event.target.value
          })
        })

    case mainAppActions.CANCEL_ORDER:
      return Object.assign({},state,{
        order: Object.assign({},state.order,{
          status: 4,
        })
      })


    case mainAppActions.UPDATE_ORDER_STATUS:
      return Object.assign({},state,{
        order: Object.assign({},state.order,{
          status: action.type_action,
        })
      })

    case mainAppActions.FOLLOW_CATEGORY:
      return Object.assign({},state,{
          followed_categories: [...state.followed_categories,action.category]
      })

    case mainAppActions.UNFOLLOW_CATEGORY:
      return Object.assign({},state,{
          followed_categories: state.followed_categories.filter(category => category.id !== action.category.id)
      })

    case mainAppActions.FOLLOWED_CATEGORIES_SUCCEEDED:
      return Object.assign({},state,{
        followed_categories: action.followed_categories
      })

    case mainAppActions.HANDLE_INPUT_FILE_CHANGE:
      if(action.event.target.name == "pictures"){
        let prev_pictures = state.order.prev_pictures
        let pictures = state.order.pictures

        for (var i in action.event.target.files){
          if(action.event.target.files[i].name != null && action.event.target.files[i].name != undefined && action.event.target.files[i].name != "item"){
            prev_pictures.push(URL.createObjectURL(action.event.target.files[i]))
            pictures.push(action.event.target.files[i])
          }
        }

        if(prev_pictures.length < 6){
          return Object.assign({}, state, {
            order: Object.assign({}, state.order,{
              pictures: pictures,
              prev_pictures: prev_pictures
            })
          }) 
        }else{
          alert("Numero máximo de imagénes excedido")
        }
      }

    case mainAppActions.DELETE_ORDER_PICTURES:
      return Object.assign({}, state, {
        order: Object.assign({}, state.order,{
          pictures: state.order.pictures.filter((image, index) => index != action.index),
          prev_pictures: state.order.prev_pictures.filter((image, index) => index != action.index)
        }) 
      }) 

    case mainAppActions.REQUEST_FINISH_PURCHASE_SUCCEDED:
      return Object.assign({}, state, {
        order: action.purchase
      })

    case mainAppActions.SHOW_MONTH_GAIN:
      return Object.assign({}, state, {
        month_gain: action.month_gain
      })
      
    case mainAppActions.REQUEST_CHANGE_PROFILE_IMAGE_SUCCEDED:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData, {
          user_profile: Object.assign({}, state.userData.user_profile, {
            image: action.image
          })
        })
      })

    case mainAppActions.REQUEST_GET_USER_AVERAGE_SCORE_SUCCEDED:
      return Object.assign({}, state, {
        userData: Object.assign({}, state.userData, {
          averageScore: action.average
        })
      })

    default:
      return state
  }
}

export default mainReducer