import * as actionsActions from '../actions/actionAction'

const initialState = {
    isFetching: false,
    actions:[],
    action_single:{
        name:"",
        status:"",
        description:"",
    },
  }
  
  const actionsReducer = (state = initialState, action) => {
  
    switch(action.type) {
 
      case actionsActions.FETCH_REQUEST_ACTIONS_SUCCEEDED:
        return Object.assign({}, state, {
          actions: action.actions,
        })
        
      default:
        return state
    }
  }
  
  export default actionsReducer