import { combineReducers } from 'redux'
import mainReducer from './mainReducer'
import userReducer from './userReducer'
import countriesReducer from './countriesReducer'
import rolesReducer from './rolesReducer'
import categoriesReducer from './categoriesReducer'
import privilegeReducer from './privilegesReducer'
import postReducer from './postReducer'
import commentReducer from './commentsReducer'
import actionsReducer from './actionReducer'

const rootReducer = combineReducers({
  
	  user: userReducer,
	  countries: countriesReducer,
	  roles: rolesReducer,
	  categories: categoriesReducer,
	  main: mainReducer,
	  privilege: privilegeReducer,
	  post: postReducer,
	  comment: commentReducer,
	  actions: actionsReducer,
  
})

export default rootReducer;