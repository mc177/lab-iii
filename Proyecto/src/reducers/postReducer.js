import * as postsActions from '../actions/postAction'

const initialState = {
    fetch: {
      is_fetching:null,
      message:"",
      result_succeeded:null
    },
    posts:[], // no se ha usado
    post:{
      id: "",
      title: "",
      post_date: "",
      price: null,
      description: "",
      conditions: "",
      pictures: [],
      prev_pictures: [],
      user: {
          id: "",
          email: "",
          user_profile:{
              name:"",
              lastname:""
          },
      },
      category: {
          id: "",
          name: "",
          description: "",
          status: null
      },
      status: null,
    },
    follow_category_posts:[],
    my_liked_posts:{posts:[]},
    post_to_purchase:{
      requirements:""
    },
    my_posts:[], //no se ha usado
    my_sales:[],
    my_purchases:[],
  }
  
  const postsReducer = (state = initialState, action) => {
  
    switch(action.type) {

      case postsActions.REQUEST_MY_POSTS_SUCCEEDED:
        return Object.assign({}, state, {
          my_posts: action.posts,
        })

      case postsActions.HANDLE_POST_INPUT_CHANGE:
        if(action.event.target.name == "pictures"){
          let prev_pictures = state.post.prev_pictures
          let pictures = state.post.pictures

          for (var i in action.event.target.files){
            if(action.event.target.files[i].name != null && action.event.target.files[i].name != undefined && action.event.target.files[i].name != "item"){
              prev_pictures.push(URL.createObjectURL(action.event.target.files[i]))
              pictures.push(action.event.target.files[i])
            }
          }

          if(prev_pictures.length < 6){
            return Object.assign({}, state, {
              post: Object.assign({}, state.post,{
                pictures: pictures,
                prev_pictures: prev_pictures
              })
            }) 
          }else{
            alert("Numero máximo de imagénes excedido")
          }
        }else{
          return Object.assign({}, state, {
            post: Object.assign({}, state.post,{
              [action.event.target.name]: action.event.target.value
            })
          }) 
        }

      case postsActions.CANCEL_POST_POST:
        return Object.assign({}, state, {
          post: Object.assign({}, state.post, {
            id: "",
            title: "",
            post_date: "",
            price: "",
            description: "",
            conditions: "",
            pictures: [],
            prev_pictures: [],
            user: {
                id: "",
                email: "",
                user_profile:{
                    name:"",
                    lastname:""
                },
            },
            category: {
                id: "",
                name: "",
                description: "",
                status: null
            },
            category_id: "",
            status: null
          })
        })

      case postsActions.REQUEST_POST_POST_SUCCEDED: 
        return Object.assign({}, state, {
          post: Object.assign({}, action.post, {
            prev_pictures: []
          }),
          my_posts: [...state.my_posts,action.post],
          fetch: Object.assign({}, state.fetch, {
            is_fetching: false
          })
        })

      case postsActions.REQUEST_POST_POST_FAILED: 
        return Object.assign({}, state, {
          fetch: Object.assign({}, state.fetch, {
            is_fetching: false
          })
        })

      case postsActions.DELETE_POST_PICTURES:
        return Object.assign({}, state, {
          post: Object.assign({}, state.post,{
            pictures: state.post.pictures.filter((image, index) => index != action.index),
            prev_pictures: state.post.prev_pictures.filter((image, index) => index != action.index)
          }) 
        }) 

      case postsActions.HANDLE_POST_TO_PUCHASE_INPUT_CHANGE:
        return Object.assign({}, state, {
          post_to_purchase: Object.assign({}, state.post_to_purchase,{
            [action.event.target.name]: action.event.target.value
          })
        })

      case postsActions.FETCH_SEND_REQUIREMENTS_SUCCEEDED:
        return Object.assign({},state,{
          my_purchases: [...state.my_purchases,action.new_purchase],
          fetch: Object.assign({}, state.fetch,{
            is_fetching:false,
            message:"",
            result_succeeded:true
          }),
          post_to_purchase: Object.assign({}, state.post_to_purchase,{
            requirements:""
          })
        })

      case postsActions.IS_FETCHING:
        return Object.assign({}, state, {
          fetch: Object.assign({}, state.fetch,{
            is_fetching:true,
          })
        })

      case postsActions.RESET_FETCH:
        return Object.assign({}, state, {
          fetch: Object.assign({}, state.fetch,{
            is_fetching:null,
            message:"",
            result_succeeded:null
          })
        })

      case postsActions.CANCEL_POST_TO_PURCHASE:
        return Object.assign({},state,{
          post_to_purchase: Object.assign({}, state.post_to_purchase,{
            requirements:""
          })
        })
    
    case postsActions.CANCEL_PURCHASE_ORDER:
      return Object.assign({}, state, {
        my_purchases: state.my_purchases.map((my_purchase) => {
          if(my_purchase.id == action.order.id){
            return Object.assign({}, my_purchase, {
              status:4,
              cancel_description: action.order.cancel_description
            })
          }
          return my_purchase
        }),
        my_sales: state.my_sales.map((my_sale) => {
          if(my_sale.id == action.order.id){
            return Object.assign({}, my_sale, {
              status:4,
              cancel_description: action.order.cancel_description
            })
          }
          return my_sale
        }),
      })

    case postsActions.CHANGE_ORDER_STATUS:
      return Object.assign({}, state, {
        my_purchases: state.my_purchases.map((my_purchase) => {
          if(my_purchase.id == action.order.id){
            return Object.assign({}, my_purchase, {
              status:action.type_action,
            })
          }
          return my_purchase
        }),
        my_sales: state.my_sales.map((my_sale) => {
          if(my_sale.id == action.order.id){
            return Object.assign({}, my_sale, {
              status:action.type_action,
            })
          }
          return my_sale
        }),
      })

    case postsActions.REQUEST_FOLLOW_CATEGORY_POSTS_SUCCEEDED:
        return Object.assign({}, state, {
            follow_category_posts: action.posts,
        })
    
    case postsActions.SHOW_POST:
        return Object.assign({},state,{
          post: action.post
        })

    case postsActions.REQUEST_POSTS_LIKED_SUCCEEDED:
        return Object.assign({},state,{
          my_liked_posts: action.posts,
        })
    
      case postsActions.REQUEST_MY_SALES_SUCCEEDED:
        return Object.assign({},state,{
          my_sales: action.sales,
        })
    
      case postsActions.REQUEST_MY_PURCHASES_SUCCEEDED:
        return Object.assign({},state,{
          my_purchases: action.purchases,
        })
      
      /*case postsActions.REQUEST_HOME_POSTS_SUCCEEDED:
        return Object.assign({},state,{
          follow_category_posts: action.posts.follow_category_posts.map((post) => {
            console.log(post)
            action.posts.liked_posts.map((liked_post) => {
              if(post.id === liked_post.id){
                return Object.assign({},post,{
                  is_liked:true
                })
              }
              return post
            })
          }),
          my_liked_posts:action.posts.liked_posts
        })*/

      case postsActions.LIKE_POST:
        let like_post = {
          post: action.post
        }
        return Object.assign({},state,{
            my_liked_posts: [...state.my_liked_posts,like_post]
        })

      case postsActions.REMOVE_LIKE_POST:
        return Object.assign({},state,{
            my_liked_posts: state.my_liked_posts.filter(my_liked_post => my_liked_post.post.id !== action.post.id)
        })

      case postsActions.DELETE_POST_PICTURES:
        return Object.assign({},state,{
          
        })
        return Object.assign({},state,{
          posts: action.posts
        })
        
      default:
        return state
    }
  }
  
  export default postsReducer