import * as rolesActions from '../actions/rolesAction'

const initialState = {
    isFetching: false,
    roles:[],
    rol:{
        name:"",
        status:"",
        description:"",
    },
  }
  
  const rolesReducer = (state = initialState, action) => {
  
    switch(action.type) {
  
      case rolesActions.HANDLE_ROL_NAME_INPUT_CHANGE:
        return Object.assign({}, state, {
          roles: state.roles.map((rol) => {
            if(rol.id == action.id){
              return Object.assign({}, rol, {
                name:action.text
              })
            }
            return rol
          }),
        })
      case rolesActions.HANDLE_ROL_STATUS_INPUT_CHANGE:
        return Object.assign({}, state, {
          roles: state.roles.map((role) => {
            if(role.id == action.id){
              return Object.assign({}, role, {
                status:action.text
              })
            }
            return role
          }),
        })
        case rolesActions.HANDLE_ROL_DESCRIPTION_INPUT_CHANGE:
        return Object.assign({}, state, {
          roles: state.roles.map((role) => {
            if(role.id == action.id){
              return Object.assign({}, role, {
                description:action.text
              })
            }
            return role
          }),
        })
      
      case rolesActions.EDIT_ROLE:
        return Object.assign({}, state, {
          roles: state.roles.map((rol) => {
            if(rol.id == action.id){
              return Object.assign({}, rol, {
                originalName:rol.name,
                originalDescription: rol.description,
                originalStatus: rol.status,
                isEditing:true
              })
            }
            return rol
          }),
        })
      
      case rolesActions.SAVE_NEW_ROL:
        return Object.assign({}, state, {
          roles: state.roles.map((rol) => {
            if(rol.id == action.new_role_id){
              return Object.assign({}, rol, {
                id: action.id,
                isNew:false
              })
            }
            return rol
          }),
        })


      case rolesActions.UPDATE_ROLE:
        return Object.assign({}, state, {
          roles: state.roles.map((rol) => {
            if(rol.id == action.rol.id){
              return Object.assign({}, action.rol, {
                isEditing:false
              })
            }
            return rol
          }),
        })

      case rolesActions.CANCEL_EDIT_ROL:
        return Object.assign({}, state, {
          roles: state.roles.map((rol) => {
            if(rol.id == action.id){
              return Object.assign({}, rol, {
                name: action.text,
                description: rol.originalDescription,
                status: rol.originalStatus,
                isEditing:false
              })
            }
            return rol
          }),
        })

      case rolesActions.ADD_NEW_ROL:
         let newRol = {
          id: Math.random(),
          name: "",
          status: 1, 
          isNew: true
        }
        return Object.assign({}, state, {
          roles: [...state.roles,newRol],
        })

      case rolesActions.DELETE_ROL:
        return Object.assign({}, state, {
          roles: state.roles.map((rol) => {
            if(rol.id == action.id){
              return Object.assign({}, rol, {
                status: 2
                })
            }
            return rol
          }),
        }) 
      
      case rolesActions.FETCH_REQUEST_ROLES_SUCCEEDED:
        return Object.assign({}, state, {
          roles: action.roles,
        })
        
      default:
        return state
    }
  }
  
  export default rolesReducer