import * as categoriesActions from '../actions/categoriesAction'

const initialState = {
    fetch: {
      is_fetching:"",
      message:"",
      result_succeeded:null
    },
    categories:[],
    category:{
        id:"",
        name:"",
        description:"",
        status:"",
    },
    filter_category:{
      id:"",
      name:"",
      posts:[]
    },
  popular_categories:[]
  }
  
  const categoriesReducer = (state = initialState, action) => {
  
    switch(action.type) {

      case categoriesActions.FETCH_POPULAR_CATEGORIES_SUCCEEDED:
        return Object.assign({}, state, {
          popular_categories: state.categories
        })
  
      case categoriesActions.HANDLE_CATEGORY_NAME_INPUT_CHANGE:
        return Object.assign({}, state, {
          categories: state.categories.map((category) => {
            if(category.id == action.id){
              return Object.assign({}, category, {
                name:action.text
              })
            }
            return category
          }),
        })

    case categoriesActions.HANDLE_CATEGORY_DESCRIPTION_INPUT_CHANGE:
        return Object.assign({}, state, {
          categories: state.categories.map((category) => {
            if(category.id == action.id){
              return Object.assign({}, category, {
                description:action.text
              })
            }
            return category
          }),
        })

      case categoriesActions.HANDLE_CATEGORY_STATUS_INPUT_CHANGE:
        return Object.assign({}, state, {
          categories: state.categories.map((category) => {
            if(category.id == action.id){
              return Object.assign({}, category, {
                status:action.text
              })
            }
            return category
          }),
        })

      case categoriesActions.UPDATE_CATEGORY:
        return Object.assign({},state,{
          categories: state.categories.map((category) => {
            if(category.id == action.category.id){
              return Object.assign({}, action.category, {
                isEditing: false
              })
            }
            return category
          }),
        })

      case categoriesActions.EDIT_CATEGORY:
        return Object.assign({}, state, {
          categories: state.categories.map((category) => {
            if(category.id == action.id){
              return Object.assign({}, category, {
                originalName:category.name,
                originalStatus: category.status,
                originalDescription: category.description,
                isEditing:true
              })
            }
            return category
          }),
        })

      case categoriesActions.SAVE_NEW_CATEGORY:
        return Object.assign({},state,{
          categories: state.categories.map((category) => {
            if(category.id == action.new_category_id){
              return Object.assign({}, action.category, {
                isNew: false
              })
            }
            return category
          }),
        })
      case categoriesActions.CANCEL_EDIT_CATEGORY:
        return Object.assign({}, state, {
          categories: state.categories.map((category) => {
            if(category.id == action.id){
              return Object.assign({}, category, {
                name: action.text,
                status: category.originalStatus,
                description: category.originalDescription,
                isEditing:false
              })
            }
            return category
          }),
        })

      case categoriesActions.ADD_NEW_CATEGORY:
         let newCategory = {
          id: Math.random(),
          name: "",
          status: 1, 
          isNew: true
        }
        return Object.assign({}, state, {
          categories: [...state.categories,newCategory],
        })

      case categoriesActions.DELETE_CATEGORY:
        return Object.assign({}, state, {
          categories: state.categories.map((category) => {
            if(category.id == action.id){
              return Object.assign({}, category, {
                status: 2
                })
            }
            return category
          }),
        })
        
      case categoriesActions.FILTER_CATEGORY:
        return Object.assign({}, state, {
          filter_category: Object.assign({},state.filter_category,{
            category:action.category,
            posts:action.posts
          })
        })
      
      case categoriesActions.FETCH_REQUEST_CATEGORIES_SUCCEEDED:
        return Object.assign({}, state, {
          categories: action.categories,
        })
        
      default:
        return state
    }
  }
  
  export default categoriesReducer