import * as commentsActions from '../actions/commentsAction'

const initialState = {
    fetch: {
      is_fetching:"",
      message:"",
      result_succeeded:null
    },
    comments:[],
    comment:{
        id: "",
        message: "",
        publish_date: "",
        purchase: {
            purchase_id: "",
            description: "",
            purchase_status: null,
            score: null,
            buyer: {
                id: "",
                email: "",
                buyer_profile: {
                    name: "",
                    lastname: ""
                }
            },
        },
        status: null,
    }
}

const commentsReducer = (state = initialState, action) => {
  
    switch(action.type) {
      case commentsActions.HANDLE_COMMENT_INPUT:
        return Object.assign({}, state, {
            comment: Object.assign({},state.comment,{
                [action.event.target.name]: action.event.target.value
            })       
        })
      
      case commentsActions.HANDLE_SCORE:
        return Object.assign({}, state,{
            comment: Object.assign({},state.comment,{
                purchase: Object.assign({},state.comment.purchase,{
                    score:action.score
                })
            })     
        })

      case commentsActions.FETCH_COMMENTS_USER_SUCCEEDED:
        return Object.assign({}, state,{
          comments: action.comments
        })
        
      case commentsActions.REQUEST_PUBLISH_COMMENT_SUCCEDED:
        return Object.assign({}, state, {
          comment: action.comment
        })

      default:
        return state
    }
  }
  
  export default commentsReducer