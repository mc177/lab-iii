import { connect } from 'react-redux'
import DetailOrder from '../../components/Main/DetailOrder'
import * as mainAppActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_purchases: state.post.my_purchases,
        order: state.main.order
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onDownloadImage: (path) =>{dispatch(mainAppActions.downloadImage(path))}
    }
}
const detailOrderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailOrder)

export default detailOrderContainer