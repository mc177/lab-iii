import { connect } from 'react-redux'
import earningsAdmin from '../../components/Main/EarningsAdmin'
import * as mainAppActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        monthly_gains: state.main.monthly_gains,
        user_gains: state.main.user_gains
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(mainAppActions.fetchUserGains())
    return {
    }
}
const earningsAdminContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(earningsAdmin)

export default earningsAdminContainer