import { connect } from 'react-redux'
import myPurchases from '../../components/Main/MyPurchases'
import * as mainAppAction from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_purchases: state.post.my_purchases
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onShowOrder: (order) => {dispatch(mainAppAction.showOrder(order))}
    }
}
const myPurchasesContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(myPurchases)

export default myPurchasesContainer