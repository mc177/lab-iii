import { connect } from 'react-redux'
import CancelOrder from '../../components/Main/CancelOrder'
import * as mainAppActions from '../../actions/mainAppAction'
import * as postActions from '../../actions/postAction' 

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_purchases: state.post.my_purchases,
        order: state.main.order
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onCancelOrder: (order) => {dispatch(mainAppActions.fetchCancelPurchaseOrder(order))},
        onHandleCancelOrderDescription: (e) => {dispatch(mainAppActions.handleCancelOrderDescription(e))}
    }
}
const detailOrderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CancelOrder)

export default detailOrderContainer