import { connect } from 'react-redux'
import earningUser from '../../components/Main/EarningsUser'
import * as mainAppActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        monthly_gains: state.main.monthly_gains
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(mainAppActions.fetchMonthlyGains())
    return {
        onShowMonthGain: (month_gain) =>{dispatch(mainAppActions.showMonthGain(month_gain))}
    }
}
const earningsUserContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(earningUser)

export default earningsUserContainer