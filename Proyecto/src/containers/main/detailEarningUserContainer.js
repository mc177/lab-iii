import { connect } from 'react-redux'
import detailEarningUser from '../../components/Main/DetailEarningUser'
import * as mainAppActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        monthly_gains: state.main.monthly_gains,
        month_gain: state.main.month_gain
    }
}
const mapDispatchToProps = dispatch => {

}
const detailEarningUserContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(detailEarningUser)

export default detailEarningUserContainer