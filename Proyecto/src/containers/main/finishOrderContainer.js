import { connect } from 'react-redux'
import finishOrder from '../../components/Main/FinishOrder'
import * as mainAppActions from '../../actions/mainAppAction'
import * as postActions from '../../actions/postAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_purchases: state.post.my_purchases,
        order: state.main.order
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onHandleInputFile: (e) => {dispatch(mainAppActions.handleInputFileChange(e))},
        onFinishOrder:(order,image) => {dispatch(mainAppActions.fetchFinishOrder(order,image));dispatch(postActions.changeOrderStatus(order,5))},
        onDeletOrderPicture: (index) => {dispatch(mainAppActions.deleteOrderImage(index))}
    }
}
const finishOrderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(finishOrder)

export default finishOrderContainer