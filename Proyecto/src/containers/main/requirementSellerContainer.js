import { connect } from 'react-redux'
import RequirementSeller from '../../components/Main/RequirementSeller'
import * as mainAppActions from '../../actions/mainAppAction'
import * as postActions from '../../actions/postAction' 

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_purchases: state.post.my_purchases,
        order: state.main.order
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onHandleCancelOrderDescription: (e) => {dispatch(mainAppActions.handleCancelOrderDescription(e))},
        onChangeOrderStatus:(order,type) =>{dispatch(mainAppActions.fetchChangeOrderStatus(order,type))}

    }
}
const requirementSellerContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RequirementSeller)

export default requirementSellerContainer