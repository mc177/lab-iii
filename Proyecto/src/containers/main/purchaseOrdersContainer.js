import { connect } from 'react-redux'
import purchaseOrders from '../../components/Main/PurchaseOrders'
import * as mainAppAction from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_sales: state.post.my_sales
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onShowOrder: (order) => {dispatch(mainAppAction.showOrder(order))}
    }
}
const purchaseOrdersContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(purchaseOrders)

export default purchaseOrdersContainer