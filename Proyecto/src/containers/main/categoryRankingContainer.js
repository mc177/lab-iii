import { connect } from 'react-redux'
import CategoryRanking from '../../components/Main/CategoryRanking'
import * as mainAppActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        monthly_gains: state.main.monthly_gains,
        user_gains: state.main.user_gains,
        popular_categories: state.categories.popular_categories

    }
}
const mapDispatchToProps = dispatch => {
    dispatch(mainAppActions.fetchPopularCategories())
    return {
    }
}
const categoryRankingContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryRanking)

export default categoryRankingContainer