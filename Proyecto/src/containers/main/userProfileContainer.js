import { connect } from 'react-redux'
import userProfile from '../../components/Main/UserProfile'
import * as mainAppActions from '../../actions/mainAppAction'
import * as countriesAction from '../../actions/countriesAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      countries: state.countries.countries,
      change_password: state.main.change_password
    }
}
const mapDispatchToProps = dispatch => {
  dispatch(countriesAction.fetchCountries())
    return {
        onHandleUserInputChange: (event,id) => {dispatch(mainAppActions.handleUserInputChange(event))},
        onHandleUserProfileInputChange: (event,id) => {dispatch(mainAppActions.handleUserProfileInputChange(event))},
        onCancelEditProfile:() => {dispatch(mainAppActions.cancelEditProfile())},
        onSaveUserProfile: (user) => {dispatch(mainAppActions.fetchSaveUserProfile(user))},
        onHandleChangePasswordInputChange: (e) => {dispatch(mainAppActions.handleChangePasswordInputChange(e))},
        resetChancePassword: () => {dispatch(mainAppActions.resetChancePassword())},
        onChangePassword: (change_password) => {dispatch(mainAppActions.fetchChangePassword(change_password))},
        onHandleProfileImageChagne: (e) => {dispatch(mainAppActions.fetchUploadProfileImage(e))}
    }
}

const userProfileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(userProfile)

export default userProfileContainer