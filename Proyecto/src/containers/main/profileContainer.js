import { connect } from 'react-redux'
import profile from '../../components/Main/Profile'
import * as userAction from '../../actions/userAction'
import * as postActions from '../../actions/postAction'
import * as mainActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_posts: state.post.my_posts
    }
}
const mapDispatchToProps = dispatch => {
		dispatch(mainActions.getUserAverageScore());
    return {
        onShowPost:(post) => {dispatch(postActions.showPost(post))}
    }
}
const profileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(profile)

export default profileContainer