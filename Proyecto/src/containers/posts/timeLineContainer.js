import { connect } from 'react-redux'
import homePage from '../../components/Posts/HomePage'
import * as postActions from '../../actions/postAction'
import * as userActions from '../../actions/userAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      follow_category_posts: state.post.follow_category_posts,
      my_liked_posts: state.post.my_liked_posts
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(postActions.fetchFollowCategoryPosts())
    dispatch(postActions.fetchPostsLiked())
    dispatch(postActions.fetchMySales())
    dispatch(postActions.fetchMyPurchases())
    return {
        onLikePost:(id) => {dispatch(postActions.likePost(id))},
        onRemoveLikePost:(id) => {dispatch(postActions.removeLikePost(id))},
        onShowPost:(post) => {dispatch(postActions.showPost(post))},
        onShowPublicUserProfile: (user) => {dispatch(userActions.fetchShowPublicUserProfile(user))}
    }
}

const timeLineContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(homePage)

export default timeLineContainer