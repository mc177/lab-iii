import { connect } from 'react-redux'
import NewPost from '../../components/Posts/NewPost'
import * as postActions from '../../actions/postAction'
import * as categoriesAction from '../../actions/categoriesAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      post: state.post.post,
      my_liked_posts: state.post.my_liked_posts,
      categories: state.categories.categories
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(categoriesAction.fetchCategories())
    return {
      onSaveNewPost: (post) => {dispatch(postActions.fetchSaveNewPost(post))},
      onPostInputChange: (e) => {dispatch(postActions.onPostHandleInputChange(e))},
      onDeletePostPicture: (index) => {dispatch(postActions.deletePostPictures(index))},
      onCancelPostPost: () => {dispatch(postActions.cancelPostPost())}
    }
}

const DetailPostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPost)

export default DetailPostContainer