import { connect } from 'react-redux'
import RequirementBuyer from '../../components/Posts/RequirementBuyer'
import * as postActions from '../../actions/postAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      post: state.post.post,
      post_to_purchase: state.post.post_to_purchase,
      fetch_post: state.post.fetch,
      actions: state.actions.actions,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onHandlePostToPurchaseInputChange: (e) => {dispatch(postActions.handlePostToPurchaseInputChange(e))},
        onCancelPostToPurchase:() => {dispatch(postActions.cancelPostToPurchase())},
        onResetFetchPost: () => {dispatch(postActions.resetFetchPost())},
        onSendRequirements: (post,requirements,action) => {dispatch(postActions.fetchSendRequirements(post,requirements,action))}
    }
}

const requirementBuyerContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RequirementBuyer)

export default requirementBuyerContainer