import { connect } from 'react-redux'
import myLikedPosts from '../../components/Main/MyLikedPosts'
import * as postActions from '../../actions/postAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      follow_category_posts: state.post.follow_category_posts,
      my_liked_posts: state.post.my_liked_posts,
      actions: state.actions.actions
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onLikePost:(post,action) => {dispatch(postActions.fetchLikePost(post,action))},
        onRemoveLikePost:(post,action) => {dispatch(postActions.fetchRemoveLikePost(post,action))},
        onShowPost:(post) => {dispatch(postActions.showPost(post))}
    }
}

const myLikedPostsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(myLikedPosts)

export default myLikedPostsContainer