import { connect } from 'react-redux'
import SearchPosts from '../../components/Posts/SearchPosts'
import * as postActions from '../../actions/postAction'
import * as categoriesAction from '../../actions/categoriesAction'
import * as mainAppActions from '../../actions/mainAppAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      post: state.post.post,
      my_liked_posts: state.post.my_liked_posts,
      categories_: state.categories.categories,
      followed_categories: state.main.followed_categories,
      filter_category: state.categories.filter_category,
      my_liked_posts: state.post.my_liked_posts,
      actions: state.actions.actions
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(categoriesAction.fetchCategories())
    return {
      onSaveNewPost: (post) => {dispatch(postActions.fetchSaveNewPost(post))},
      onFollowCategory: (category) => {dispatch(mainAppActions.fetchFollowCategory(category))},
      onUnFollowCategory: (category) => {dispatch(mainAppActions.fetchUnFollowCategory(category))},
      onSearchPostByCategory: (category) => {dispatch(categoriesAction.fetchFilterCategory(category))},
      onShowPost:(post) => {dispatch(postActions.showPost(post))},
      onLikePost:(post,action) => {dispatch(postActions.fetchLikePost(post,action))},
      onRemoveLikePost:(post,action) => {dispatch(postActions.fetchRemoveLikePost(post,action))},

    }
}

const searchPostsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPosts)

export default searchPostsContainer