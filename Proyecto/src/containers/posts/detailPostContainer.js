import { connect } from 'react-redux'
import DetailPost from '../../components/Posts/DetailPost'
import * as postActions from '../../actions/postAction'
import * as userActions from '../../actions/userAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      post: state.post.post,
      my_liked_posts: state.post.my_liked_posts,
      actions: state.actions.actions
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onLikePost:(post,action) => {dispatch(postActions.fetchLikePost(post,action))},
        onRemoveLikePost:(post,action) => {dispatch(postActions.fetchRemoveLikePost(post,action))},
        onShowPublicUserProfile: (user) => {dispatch(userActions.fetchShowPublicUserProfile(user))}
    }
}

const DetailPostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailPost)

export default DetailPostContainer