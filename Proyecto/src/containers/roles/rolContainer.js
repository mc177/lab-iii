import { connect } from 'react-redux'
import rolesList from '../../components/Roles/RoleList'
import * as rolesAction from '../../actions/rolesAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      roles: state.roles.roles

    }
}
const mapDispatchToProps = dispatch => {
    dispatch(rolesAction.fetchRoles())
    return {
        onHandleNameInputChange: (event,id) => {dispatch(rolesAction.handleNameInputChange(event,id))},
        onHandleDescriptionInputChange: (event,id) => {dispatch(rolesAction.handleDescriptionInputChange(event,id))},
        onHandleStatusInputChange: (event,id) => {dispatch(rolesAction.handleStatusInputChange(event,id))},
        onAddNewRol: () => {dispatch(rolesAction.addNewRol())},
        onCancelNewRol: (id) => {dispatch(rolesAction.deleteRol(id))},
        onDeleteRol: (id) => {dispatch(rolesAction.fetchDeleteRole(id))},
        onEditRol: (id) => {dispatch(rolesAction.editRol(id))},
        onCancelEditRol: (text,id) => {dispatch(rolesAction.cancelEditRol(text,id))},
        onSaveNewRol: (role) => {dispatch(rolesAction.fetchCreateRole(role))},
        onUpdateRol: (role) => {dispatch(rolesAction.fetchUpdateRole(role))}
    }
}
const rolesListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(rolesList)

export default rolesListContainer