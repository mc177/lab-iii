import { connect } from 'react-redux'
import PrivilegeList from '../../components/Privileges/PrivilegeList'
import * as privilegeAction from '../../actions/privilegesAction'

const mapStateToProps = state => {
    return {
        userData: state.main.userData,
        roles: state.roles.roles,
        privileges: state.privilege.privileges
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(privilegeAction.fetchPrivileges())
    return {
        onEditPrivilege:(id) => {dispatch(privilegeAction.editPrivilege(id))},
        onCancelEditPrivilege: (id) => {dispatch(privilegeAction.cancelEditPrivilege(id))},
        onDeletePrivilege: (id) => {dispatch(privilegeAction.deletePrivilege(id))},
        onUpdatePrivilege: (privilege) => {dispatch(privilegeAction.updatePrivilege(privilege))},
        onSaveNewPrivilege: (privilege) => {dispatch(privilegeAction.saveNewPrivilege(privilege))},
        onHandlePrivilegeInputChange: (e,id) => {dispatch(privilegeAction.handlePrivilegeInputChange(e,id))},
        onAddNewPrivilege: () => {dispatch(privilegeAction.addNewPrivilege())}
    }
}
const privilegeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivilegeList)

export default privilegeContainer