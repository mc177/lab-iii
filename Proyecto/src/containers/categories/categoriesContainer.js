import { connect } from 'react-redux'
import categoryList from '../../components/Categories/CategoryList'
import * as categoriesAction from '../../actions/categoriesAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      categories: state.categories.categories
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(categoriesAction.fetchCategories())
    return {
        onHandleNameInputChange: (event,id) => {dispatch(categoriesAction.handleNameInputChange(event,id))},
        onHandleDescriptionInputChange: (event,id) => {dispatch(categoriesAction.handleDescriptionInputChange(event,id))},
        onAddNewCategory: () => {dispatch(categoriesAction.addNewCategory())},
        onDeleteCategory: (id) => {dispatch(categoriesAction.fetchDeleteCategory(id))},
        onEditCategory: (id) => {dispatch(categoriesAction.editCategory(id))},
        onCancelEditCategory: (text,id) => {dispatch(categoriesAction.cancelEditCategory(text,id))},
        onHandleStatusInputChange: (event,id) => {dispatch(categoriesAction.handleStatusInputChange(event,id))},
        onUpdateCategory: (category) => {dispatch(categoriesAction.fetchUpdateCategory(category))},
        onSaveNewCategory: (category) => {dispatch(categoriesAction.fetchCreateCategory(category))},
    }
}


const categoriesListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(categoryList)

export default categoriesListContainer