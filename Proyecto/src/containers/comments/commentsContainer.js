import { connect } from 'react-redux'
import Comments from '../../components/Comments/Comments'
import * as commentsAction from '../../actions/commentsAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        my_purchases: state.post.my_purchases,
        order: state.main.order,
        comments: state.comment.comments,
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(commentsAction.fetchCommentsUser())
    return {
        
    }
}
const detailOrderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Comments)

export default detailOrderContainer