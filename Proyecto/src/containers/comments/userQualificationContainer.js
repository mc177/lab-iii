import { connect } from 'react-redux'
import UserQualification from '../../components/Comments/UserQualification'
import * as commentsAction from '../../actions/commentsAction'

const mapStateToProps = state => {
    return {
        user: state.main.userData,
        order: state.main.order,
        comment: state.comment.comment
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onHandleCommentInput:(e) => {dispatch(commentsAction.handleCommentInput(e))},
        onHandleScore: (score) => {dispatch(commentsAction.handleScore(score))},
        onPublishComment: (order, comment) => {dispatch(commentsAction.publishComment(order, comment))}
    }
}
const userQualificationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserQualification)

export default userQualificationContainer