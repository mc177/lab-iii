import { connect } from 'react-redux'
import countriesList from '../../components/Countries/CountryList'
import * as countriesAction from '../../actions/countriesAction'

const mapStateToProps = state => {
    return {
      user: state.main.userData,
      countries: state.countries.countries
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(countriesAction.fetchCountries())
    return {
        onHandleNameInputChange: (event,id) => {dispatch(countriesAction.handleNameInputChange(event,id))},
        onAddNewCountry: () => {dispatch(countriesAction.addNewCountry())},
        onCancelNewCountry: (id) => {dispatch(countriesAction.deleteCountry(id))},
        onDeleteCountry: (id) => {dispatch(countriesAction.fetchDeleteCountry(id))},
        onEditCountry: (id) => {dispatch(countriesAction.editCountry(id))},
        onCancelEditCountry: (text,id) => {dispatch(countriesAction.cancelEditCountry(text,id))},
        onHandleStatusInputChange: (event,id) => {dispatch(countriesAction.handleStatusInputChange(event,id))},
        onUpdateCountry: (country) => {dispatch(countriesAction.fetchUpdateCountry(country))},
        onSaveNewCountry: (country) => {dispatch(countriesAction.fetchCreateCountry(country))},
    }
}

const countriesListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(countriesList)

export default countriesListContainer