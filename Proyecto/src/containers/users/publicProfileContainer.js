import { connect } from 'react-redux'
import publicProfile from '../../components/Users/PublicProfile'
import * as userAction from '../../actions/userAction'
import * as postActions from '../../actions/postAction'

const mapStateToProps = state => {
    return {
        user: state.user.user,
        user_posts: state.post.posts       
    }
}
const mapDispatchToProps = dispatch => {  
    return {
        onShowPost:(post) => {dispatch(postActions.showPost(post))}
    }
}
const publicProfileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(publicProfile)

export default publicProfileContainer