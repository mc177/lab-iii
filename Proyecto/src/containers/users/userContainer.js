import { connect } from 'react-redux'
import UserListModule from '../../components/Users/UserListModule'
import * as userAction from '../../actions/userAction'
import * as countriesAction from '../../actions/countriesAction'
import * as rolesAction from '../../actions/rolesAction'

const mapStateToProps = state => {
    return {
        userData: state.main.userData,
        roles: state.roles.roles,
        users: state.user.users,
        user: state.user.user,
        countries : state.countries.countries,
        roles: state.roles.roles,        
    }
}
const mapDispatchToProps = dispatch => {
    dispatch(userAction.fetchUsers())
    dispatch(countriesAction.fetchCountries())
    dispatch(rolesAction.fetchRoles())    
    return {
        onHandleUserCountryChange: (e) =>{dispatch(userAction.handleUserCountryChange(e))},
        onHandleUserRoleChange: (e) =>{dispatch(userAction.handleUserRoleChange(e))},
        onShowUser:(user) => {dispatch(userAction.showUser(user))},
        onEditUser:(user) => {dispatch(userAction.editUser(user))},
        onRestoreUser: () => {dispatch(userAction.restoreUser())},
        onDeleteUser: (id) => {dispatch(userAction.fetchDeleteUser(id))},
        onUpdateUser: (user) => {dispatch(userAction.fetchUpdateUser(user))},
        onSaveNewUser: (user) => {dispatch(userAction.fetchCreateUser(user))},
        onHandleUserInputChange: (e) => {dispatch(userAction.handleUserInputChange(e))},
    }
}
const userContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserListModule)

export default userContainer